#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <unsupported/Eigen/IterativeSolvers>
#include <Eigen/SparseLU>
#include "IDRStab.h"
#include "BiCGSTABL.h"
#include <vector>
#include <string>
#include <iostream>
#include <chrono>
#include <type_traits>
#include <cmath>
#include <limits>
#include "discretizer.hpp"
#include "input_struct.hpp"
#include <fstream>
#include "mf.hpp"
#include "result.hpp"
#include <iomanip>

#include <amgcl/adapter/eigen.hpp>
#include <amgcl/backend/builtin.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/solver/bicgstabl.hpp>
#include <amgcl/solver/idrs.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/relaxation/damped_jacobi.hpp>
#include <amgcl/relaxation/as_preconditioner.hpp>
#include <amgcl/profiler.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/adapter/eigen.hpp>
#include <amgcl/backend/eigen.hpp>
#include "cxxopts.hpp"
#include <omp.h>

typedef std::chrono::high_resolution_clock Clock;
typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SparseMatrixType;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorType;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> DenseMatrixTypeCol;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DenseMatrixTypeRow;

double exact_solution1(double a, double b, double c, double x, double y, double z)
{
	//TODO: Test synthesized solution
	//u=u
	//e=e
	//sl=sl
	//s constructed such that the solution fits
	return a * std::pow(x + y + z, b) + c;
}
double exact_solution1_s(double a, double b, double c, double u, double e, double sl, double x,
	double y, double z)
{
	//TODO: Test synthesized solution
	//phi(x)=a * std::pow(x + y + z, b) + c
	//u=u
	//e=e
	//sl=sl
	//s constructed such that the solution fits
	// return	-3 * a * (-1 + b) * b * e * std::pow(x + y + z,
	// 		(-2 + b)) + 3 * a * b * u * std::pow(x + y + z,
	// 		(-1 + b)) + sl * (c + a * std::pow(x + y + z, b));
	return 3 * u * a * b * std::pow(x + y + z, b - 1)
		- 3 * b * (b - 1) * a * e * std::pow(x + y + z, b - 2)
		+ sl * (a * std::pow(x + y + z, b) + c);
}
double compare_solutions(VectorType value, VectorType expected)
{
	double deviation = (value - expected).norm();
	return deviation / value.size();
}
double solve_case(int N, std::string scheme)
{
	Input3D input;
	input.x_w = 0;
	input.x_e = 1;
	input.y_s = 0;
	input.y_n = 1;
	input.z_d = 0;
	input.z_u = 1;
	input.N_x = N;
	input.N_y = N;
	input.N_z = N;
	double a = 1;
	double b = 2;
	double c = 1;
	double u = 1;
	double e = 1;
	double sl = 0;

	input.phi_w = [a, b, c, input](double y, double z)
	{
		return exact_solution1(a, b, c, input.x_w, y, z);
	};
	input.phi_e = [a, b, c, input](double y, double z)
	{
		return exact_solution1(a, b, c, input.x_e, y, z);
	};
	input.phi_n = [a, b, c, input](double x, double z)
	{
		return exact_solution1(a, b, c,  x, input.y_n, z);
	};
	input.phi_s = [a, b, c, input](double x, double z)
	{
		return exact_solution1(a, b, c,  x, input.y_s, z);
	};
	input.phi_d = [a, b, c, input](double x, double y)
	{
		return exact_solution1(a, b, c, x, y, input.z_d);
	};
	input.phi_u = [a, b, c, input](double x, double y)
	{
		return exact_solution1(a, b, c, x, y, input.z_u);
	};

	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	double dz = (input.z_u - input.z_d) / (input.N_z - 1);
	input.u_x = [u](double x, double y, double z)
	{
		return u;
	};
	input.u_y = [u](double x, double y, double z)
	{
		return u;
	};
	input.u_z = [u](double x, double y, double z)
	{
		return u;
	};
	input.e = [e](double x, double y, double z)
	{
		return e;
	};
	input.s = [a, b, c, u, e, sl](double x, double y, double z)
	{
		return exact_solution1_s(a, b, c, u, e, sl, x, y, z);
	};
	input.sl = [sl](double x, double y, double z)
	{
		return sl;
	};

	if (scheme == "CF" || scheme == "CFCF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CF(u, e, dx);
		};
	}
	else if (scheme == "HF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::HF(u, e, dx);
		};
	}
	else if (scheme == "CD")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CD(u, e, dx);
		};
	}
	else if (scheme == "UW")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::UW(u, e, dx);
		};
	}

	SparseMatrixType A;
	VectorType rhs;

	if (scheme == "CFCF")
	{
		discretizer::discretizecf(input, A, rhs);
	}
	else
	{
		discretizer::discretize(input, A, rhs);
	}

	//Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
	//solver.analyzePattern(A);
	//solver.factorize(A);
	// Eigen::BiCGSTAB<SparseMatrixType, Eigen::DiagonalPreconditioner<double>> solver;
	// // Eigen::IDRStab<SparseMatrixType, Eigen::DiagonalPreconditioner<double>> solver;
	// solver.compute(A);
	// solver.setTolerance(1e-12);
	// solver.setMaxIterations(2000);
	// VectorType phi = solver.solve(rhs);

	typedef amgcl::backend::builtin<double> backend;
	//Use BiCGStab with AMG as a preconditioner
	// typedef amgcl::make_solver <
	// amgcl::amg <
	// backend,
	// amgcl::coarsening::smoothed_aggregation,
	// amgcl::relaxation::spai0 >,
	// amgcl::solver::bicgstab<backend >>
	// amgcl_solver;

	//Use BiCGStab from the amgcl library with Jacobian preconditioner:
	typedef amgcl::make_solver <
	amgcl::relaxation::as_preconditioner <
	amgcl::backend::builtin<double>,
	      amgcl::relaxation::damped_jacobi
	      >,
	      amgcl::solver::bicgstab <
	      amgcl::backend::builtin<double>
	      >>
	      amgcl_solver;
	//This calls apply in damped_jacobi.hpp, which never uses damping but applies
	//diag(A) as a preconditioner.
	//solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
	//Set up the solver parameters
	typename amgcl_solver::params prm;
	prm.solver.tol = 1e-12;
	prm.solver.maxiter = 2000;
	//prm.precond.damping = 1.0;
	int iterations;
	double true_residual;
	double estimated_residual;
	//Initial guess & result vector
	Eigen::VectorXd x_eigen = Eigen::VectorXd::Zero(A.rows());
	std::vector<double> x_amg = std::vector<double>(x_eigen.data(), x_eigen.data() + x_eigen.size());
	//Convert Eigen's matrix format to one compatible with amgcl's backend
	size_t n = A.rows();
	const int* ptr = A.outerIndexPtr();
	const int* col = A.innerIndexPtr();
	const double* val = A.valuePtr();
	amgcl::backend::crs<double> A_amgcl(std::make_tuple(n,
			amgcl::make_iterator_range(ptr, ptr + n + 1),
			amgcl::make_iterator_range(col, col + ptr[n]),
			amgcl::make_iterator_range(val, val + ptr[n])));

	//Set up the solver
	amgcl_solver solve(A_amgcl, prm);

	//Solve the system for the given RHS:
	std::tie(iterations, estimated_residual) = solve(A_amgcl, rhs, x_amg);

	//Convert the result back into Eigen's format to compute the true residual.
	Eigen::VectorXd phi = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(x_amg.data(),
			x_amg.size());
	//std::cout << "iterations: " << iterations << std::endl;

	VectorType phi_exact(input.N_x * input.N_y * input.N_z);

	for (int i = 0; i < input.N_x; i++)
	{
		for (int j = 0; j < input.N_y; j++)
		{
			for (int k = 0; k < input.N_z; k++)
			{

				//Index of the cell
				int ix_c = i + input.N_x * j + input.N_x * input.N_y * k;
				//Position of the nodal point
				double x = input.x_w + i * dx;
				double y = input.y_s + j * dy;
				double z = input.z_d + k * dz;
				phi_exact(ix_c) = exact_solution1(a, b, c, x, y, z);
			}
		}


	}

	return compare_solutions(phi, phi_exact);
}
int main()
{



	std::vector<int> N_vector(5);
	std::generate(N_vector.begin(), N_vector.end(), [n = 1]()mutable
	{
		n *= 2;
		return n + 1;});

	std::cout << std::setw(5) << "N" << std::setw(15) << "CFCF" << std::setw(15) << "CF" << std::setw(15) << "HF" << std::setw(15) << "CD" << std::setw(15) << "UW" << std::endl;

	for (auto N : N_vector)
	{
		std::cout << std::setw(5) << N
			<< std::setw(15) << solve_case(N, "CFCF")
			<< std::setw(15) << solve_case(N, "CF")
			<< std::setw(15) << solve_case(N, "HF")
			<< std::setw(15) << solve_case(N, "CD")
			<< std::setw(15) << solve_case(N, "UW")
			<< std::endl;
	}

	// assert(solve_case(257, "CFCF") < 7e-7);
	// assert(solve_case(257, "CF") < 8e-4);
	// assert(solve_case(257, "HF") < 8e-4);
	// assert(solve_case(257, "CD") < 8e-4);
	// assert(solve_case(257, "UW") < 8e-4);
}