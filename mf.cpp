#include <cmath>
#include <iostream>
#include "mf.hpp"
#include <vector>
#include<algorithm>
#include <cassert>
double mf::B(const double z)
{
	/*
	        Generating function for the Bernoulli numbers
	        B(z)=z/(exp(z)-1)
	*/
	if (std::abs(z) > 1.0e-3)
	{
		return z / std::expm1(z);
	}
	else
	{
		//Use series expansion to avoid division by 0
		constexpr double B0 = +1.0;
		constexpr double B1 = -1.0 / 2;
		constexpr double B2 = +1.0 / 6;
		constexpr double B4 = -1.0 / 30;
		constexpr double B6 = +1.0 / 42;

		constexpr double C0 = B0 / 1;
		constexpr double C1 = B1 / 1;
		constexpr double C2 = B2 / 2;
		constexpr double C4 = B4 / 24;
		constexpr double C6 = B6 / 720;

		const double zz = z * z;
		// note: the z-term is handled separately
		return C0 + C1 * z + (C2 + (C4 + C6 * zz) * zz) * zz;

	}
}
double mf::C(const double z)
{
	/*
	        Function C(z)
	        C(Z)=(exp(z/2)-1-z/2)/(z(exp(z)-1))
		z->inf, C~1/(2z sinh(z/2))
		z->-inf,C~ 0.5+1/z
	*/
	double expm1z = std::expm1(z);

	if (std::isinf(expm1z))
	{
		//For double precision exp(z) is inf for z>709.
		//C(Z) tends to 1/(2z sinh(z/2))
		//This is exponentially close to 1/(2z exp(z/2))
		//This can probably be set to 0.0 for any practical application
		return 1 / (2 * z * std::sinh(z / 2));
	}
	else
	{
		if (std::abs(z) > 1.0e-3)
		{
			return (std::expm1(z / 2.0) - z / 2.0) / (z * expm1z);
		}
		else
		{
			//Use series expansion to avoid division by 0
			//C(Z)~1/8-z/24+z^2/384+z^3/1440
			//The cubic term is well below machine epsilon if
			//|z|<1e-7
			//Horner form of 1/8-z/24+z^2/384+z^3/1440
			//return 1 / 8.0 + z * (-(1 / 24.0) + (1 / 384.0 + z / 1440.0) * z);
			constexpr double C0 = 1 / 8.0;
			constexpr double C1 = -1 / 24.0;
			constexpr double C2 = 1 / 384.0;
			constexpr double C3 = 1 / 1440.0;
			constexpr double C4 = -1 / 15360.0;
			double confuse = C0 + z * (C1 + z * (C2 + z * (C3 - z * C4)));
			return confuse;
			//return 1/8.0+z*(-(1/24.0)+z*(1/384.0+(1/1440.0-z/15360.0)*z));
		}
	}

}
double mf::W(const double z)
{
	/*
	        W function
	        W(z)=(exp(z)-1-z)/(z*(exp(z)-1))
	*/
	if ( std::abs(z) > 1e-7)
	{
		double exmp1z = std::expm1(z);

		if (std::isinf(exmp1z))
		{
			//Use asymptotic value for W to avoid
			//inf/inf
			//W(z)=1/z-1/std::expm1(z)
			if (z > 0)
			{
				return 0.0 + 1.0 / z;
			}
			else
			{
				return 1.0 + 1.0 / z;
			}
		}
		else
		{
			//No weird exceptions occured, just apply formula
			//for W(z)
			return (exmp1z - z) / (z * exmp1z);
		}
	}
	else
	{
		return z * (z * z / 720.0 - 1.0 / 12.0) + 0.5;
	}

}
double mf::sgn(const double z)
{
	if (z >= 0.0)
	{
		return 1.0;
	}
	else
	{
		return -1.0;
	}
}
double mf::A(const double u, const double e)
{
	return u / e;
}
double mf::P(const double A, const double dx)
{
	/*
	        Grid Peclet number
	*/
	return dx * A;
}
double mf::P(const double u, const double e, const double dx)
{
	/*
	        Grid Peclet number
	*/
	return dx * A(u, e);
}
std::vector<double> mf::logspace(const double a, const double b, const int N,
	const double base)
{
	/*
	        TODO: Write test (has been tested on Coliru only thus far)
	        Works similar to Matlab's logspace:
	        https://nl.mathworks.com/help/matlab/ref/logspace.html
	        Generates logarithmically spaced points between [base^a,base^b]
	*/
	assert(N > 1);
	std::vector<double> output(N);
	//base^a*multiplier^(N-1)=base^b
	double multiplier = std::pow(base, (b - a) / (N - 1));
	double value = std::pow(base, a);
	output[0] = value;

	for (int i = 1; i < N; ++i)
	{
		output[i] = output[i - 1] * multiplier;
	}

	return output;
}