#ifndef INPUT_STRUCT_H
#define INPUT_STRUCT_H
#include <functional>
/*
        +X direction: west -> east
        +Y direction: south-> north
        +Z direction: down -> up
        See Fig. 1 in "Discretization and Parallel Iterative Schemes for Advection-Diffusion-Reaction Problems"
*/
//TODO: Work out dx,dy,dz from the available info (dx is needed to define the flux function)
struct Input1D
{
	//Dirichlet boundary conditions
	double phi_w;	//West
	double phi_e;	//East

	//Coordinates of the boundaries
	double x_w;
	double x_e;

	//Number of gridpoints in the x-direction
	int N_x;

	//Flux approximation
	std::function<std::array<double, 4>(double, double, double)> flux; //flux(u,e,dx)

	//x-Component advection(x):
	std::function<double(double)> u; //u(x)

	//Diffusion (x)
	std::function<double(double)> e; //e(x)

	//Source (x)
	std::function<double(double)> s; //s(x)

	//Linear source(x)
	std::function<double(double)> sl; //sl(x)
};
struct Input2D
{
	//Dirichlet boundary conditions
	std::function<double(double)> phi_w; //phi_w(y)
	std::function<double(double)> phi_e; //phi_e(y)
	std::function<double(double)> phi_n; //phi_n(x)
	std::function<double(double)> phi_s; //phi_s(x)

	//Coordinates of the boundaries
	double x_w;
	double x_e;
	double y_s;
	double y_n;

	//Cross-flux coefficient, 0=not included, 1=fully included
	double CFbeta = 0.0;
	double CFalpha = 1.0;
	void set_CFbeta(double beta)
	{
		CFbeta = beta;
		CFalpha = (1 + CFbeta) / 2;
		//TODO: Check this
	}
	/*
	        "Adding the three
	        one-dimensional problems in x, y and z-direction, we find that we need to choose
	        CFalpha=(1+2*CFbeta)/3 for consistency"
	        TODO:
	        wat in 2d?
	        kan dit handig via een enum?, dat gewoon altijd cross-flux scenario wordt doorgerekend
	        maar dat het geen effect heeft als CFbeta=0.0. In plaats van met if-statement gerommel?

	        Idee:
	        -na uitschrijven lijkt het dat er allerlei producten ontstaan van de flux-coefficienten
	        alles zou in principe te verkrijgen moeten zijn uit flux::CF
	        -hebben indices nodig van de 4 extra punten in het stencil
	        -2e sectie maken onder huidige ding met if statement: if(CFbeta>0){//neem de cross-flux mee}
	        -in huidige sectie die alpha erin gooien, als het goed is veranderd niks als beta=0.0!
		-Dit verifieren
	        -checken wat alpha/beta moet zijn, originele complete flux paper heeft het niet over CFbeta en CFalpha
	        -checken dat scheme 2e orde is in algemene geval
	        -checken dat scheme 2e orde is voor CFCF,CF,HF,CD als er geen advectie is
	*/

	//Number of gridpoints
	int N_x;
	int N_y;

	//Flux approximation
	std::function<std::array<double, 4>(double, double, double)> flux; //flux(u,e,d?)

	//x,y-Components advection(x,y):
	std::function<double(double, double)> u_x; //u_x(x,y)
	std::function<double(double, double)> u_y; //u_y(x,y)

	//Diffusion (x,y)
	std::function<double(double, double)> e; //e(x,y)

	//Constant source (x,y)
	std::function<double(double, double)> s; //s(x,y)

	//Linear source(x,y)
	std::function<double(double, double)> sl; //sl(x,y)
};
struct Input3D
{
	//Dirichlet boundary conditions
	std::function<double(double, double)> phi_w; //phi_w(y,z)
	std::function<double(double, double)> phi_e; //phi_e(y,z)
	std::function<double(double, double)> phi_n; //phi_n(x,z)
	std::function<double(double, double)> phi_s; //phi_s(x,z)
	std::function<double(double, double)> phi_d; //phi_d(x,y)
	std::function<double(double, double)> phi_u; //phi_u(x,y)

	//Coordinates of the boundaries
	double x_w;
	double x_e;
	double y_s;
	double y_n;
	double z_d;
	double z_u;

	//Number of gridpoints
	int N_x;
	int N_y;
	int N_z;

	//Cross-flux coefficient, 0=not included, 1=fully included
	double CFbeta = 0.0;
	double CFalpha = 1.0;
	void set_CFbeta(double beta)
	{
		CFbeta = beta;
		CFalpha = (1 + 2 * CFbeta) / 3;
		//TODO: Check this
	}
	/*
	        "Adding the three
	        one-dimensional problems in x, y and z-direction, we find that we need to choose
	        CFalpha=(1+2*CFbeta)/3 for consistency"
	        TODO:
	        wat in 2d?
	        kan dit handig via een enum?, dat gewoon altijd cross-flux scenario wordt doorgerekend
	        maar dat het geen effect heeft als CFbeta=0.0. In plaats van met if-statement gerommel?
	*/

	//Flux approximation
	std::function<std::array<double, 4>(double, double, double)> flux; //flux(u,e,d?)

	//x,y,z-Components advection(x,y,z):
	std::function<double(double, double, double)> u_x; //u_x(x,y,z)
	std::function<double(double, double, double)> u_y; //u_y(x,y,z)
	std::function<double(double, double, double)> u_z; //u_z(x,y,z)

	//Diffusion (x,y,z)
	std::function<double(double, double, double)> e; //e(x,y,z)

	//Constant source (x,y,z)
	std::function<double(double, double, double)> s; //s(x,y,z)

	//Linear source(x,y,z)
	std::function<double(double, double, double)> sl; //sl(x,y,z)
};

#endif //INPUT_STRUCT_H