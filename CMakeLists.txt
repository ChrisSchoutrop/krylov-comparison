#In build directory to configure:
#cmake ../
#To build use any of the following in the build directory:
#reset;cmake --build .; make test
#reset;cmake --build .;ctest --verbose
#reset;(cd ..; ./format.sh);cmake –G”Unix Makefiles” ../src; make -j;make test
#reset;(cd ..; ./format.sh);cmake –G”Unix Makefiles” ../src; make -j

#Backup method  in case cmake somehow fails (run in code directory):
#g++ -o acos_2020 acos_2020.cpp mf.cpp flux.cpp discretizer.cpp input_struct.cpp result.cpp  -I eigen-eigen-323c052e1731/ -I /usr/include/suitesparse/ -I /usr/include/superlu -I amgcl -std=c++1z -lsuperlu -lumfpack -O3 -march=native -fopenmp


cmake_minimum_required(VERSION 3.10)
#https://bastian.rieck.me/blog/posts/2017/simple_unit_tests/
#https://stackoverflow.com/questions/7724569/debug-vs-release-in-cmake
#If the test throws a runtime error, it failed; throw std::runtime_error("test failed")

#To make SuperLU compile one can also use:
#https://ubuntuforums.org/showthread.php?t=360888
#reset
#g++ -c superlu_test.cpp -I eigen-eigen-323c052e1731/ -I /usr/include/suitesparse
#g++ -o confuse superlu_test.o -lumfpack

#g++ -c superlu_test.cpp -I eigen-eigen-323c052e1731/ -I /usr/include/superlu -I /usr/include/suitesparse
#g++ -o super_confuse superlu_test.o -lsuperlu -lumfpack

include_directories(amgcl)
include_directories(eigen-eigen-323c052e1731)
include_directories(/usr/include/superlu)
include_directories(/usr/include/suitesparse)
ENABLE_TESTING()
#target_link_libraries(superlu)
#target_link_libraries(umfpack)
#look for header in user given directories
#https://gitlab.mpi-magdeburg.mpg.de/software/flexiblas-release/blob/00d14fdde71ebbb094acd1acdefd051f71976231/CMakeModules/FindSUPERLU.cmake

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1z -O3 -march=native -fopenmp")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1z -O0 -g")
#TODO: Figure out why including -fopenmp makes this acos2020 extremely slow
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1z -O3")

#Simple program to see if superlu and suitesparse work properly
#Add the flags needed to make SuperLU and UMFPack work
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lsuperlu -lumfpack -I /usr/include/superlu -I /usr/include/suitesparse")
ADD_EXECUTABLE(superlu_test superlu_test.cpp)

#Link openblas (Only needed for Avron's implementation of condest)
#https://stackoverflow.com/questions/59548308/use-cmake-findblas-to-link-openblas
#include_directories(eigen-eigen-323c052e1731)
ADD_EXECUTABLE(condest_benchmark condest_benchmark.cpp mf.cpp flux.cpp discretizer.cpp)
#https://stackoverflow.com/questions/14500830/error-in-compiling-a-sample-file-using-umfpack
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lm -lrt")
set(BLA_VENDER OpenBLAS)
find_package(BLAS REQUIRED)
if(BLAS_FOUND)
    message("OpenBLAS found.")
    include_directories(${BLAS_INCLUDE_DIRS})
    target_link_libraries(condest_benchmark ${BLAS_LIBRARIES})
endif(BLAS_FOUND)

#Test for the Matrix Functions (mf)
ADD_EXECUTABLE( mftest mftest.cpp mf.cpp)
ADD_TEST(mftest mftest)

#Test comparing 1D scheme with exact solution of difference scheme
ADD_EXECUTABLE( oned_test oned_test.cpp mf.cpp flux.cpp)
ADD_TEST(oned_test oned_test)

#Test for the 1D discretization scheme with synthetic solution
ADD_EXECUTABLE( discretizer_test1d discretizer_test1d.cpp mf.cpp flux.cpp discretizer.cpp input_struct.cpp)
ADD_TEST(discretizer_test1d discretizer_test1d)

#Test for the 2D discretization scheme with synthetic solution
ADD_EXECUTABLE( discretizer_test2d discretizer_test2d.cpp mf.cpp flux.cpp discretizer.cpp input_struct.cpp)
ADD_TEST(discretizer_test2d discretizer_test2d)

#Test for the 3D discretization scheme with synthetic solution
ADD_EXECUTABLE( discretizer_test3d discretizer_test3d.cpp mf.cpp flux.cpp discretizer.cpp input_struct.cpp)
ADD_TEST(discretizer_test3d discretizer_test3d)

#Convergence rate program
ADD_EXECUTABLE(convergence_rate convergence_rate.cpp mf.cpp flux.cpp discretizer.cpp input_struct.cpp result.cpp)

#Main program
ADD_EXECUTABLE(acos_2020 acos_2020.cpp mf.cpp flux.cpp discretizer.cpp input_struct.cpp result.cpp)
