#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <random>
#include <chrono>
int main()
{
	{
	boost::mt19937 rng(time(0));

	boost::normal_distribution<> nd(0.0, 1.0);

	boost::variate_generator<boost::mt19937&,
	      boost::normal_distribution<> > var_nor(rng, nd);

	for (int i = 0; i < 10; ++i)
	{
		double d = var_nor();
		std::cout << d << std::endl;
	}

	int counter = 0;
	int N = 1e5;

	for (int i = 0; i < N; ++i)
	{
		double d = var_nor();

		if (d > -1 && d < 1)
		{
			counter++;
		}
	}

	std::cout << "Fraction in 1 stdev:" << (double)counter / (double)N << std::endl;
	}
	{
		boost::mt19937 m_randomGenerator;
		boost::normal_distribution<> m_distribution;
		boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > m_variateGenerator(m_randomGenerator,m_distribution);
		std::random_device randomDevice;

		if (randomDevice.entropy() == 0)
		{
			size_t m_seed = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch()).count();
			m_randomGenerator = boost::mt19937(m_seed);
		}
		else
		{
			m_randomGenerator = boost::mt19937(randomDevice());
		}

		m_distribution = boost::normal_distribution<>(0.0, 1.0);
		std::cout<<m_variateGenerator();
	}
}