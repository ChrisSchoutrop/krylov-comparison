/*
        TODO: Robert scheme to take the linear source into account properly
*/
#include "discretizer.hpp"
#include <iostream>
void discretizer::discretizecf(const Input2D& input, SparseMatrixType& A, VectorType& b)
{
	//TODO: Assert that only CF flux is used?, other schemes do not take crossflux into account
	/*
		        TODO:
		        -crossflux
			For this we need:
				For F_{i+1/2,j}, we also need:
				i,j+1/2
				i,j-1/2
				i+1,j+1/2
				i+1,j-1/2
				Basically if we're considering the cell at x_{i,j}
				we're going to need the HF alpha,beta for all 12 edges.
				Notation: Using the compass stencil, for example a flux going from
				a to b would be Fab.
				Therefore we need the fluxes:
				F_NWN
				F_NNE
				F_WNW
				F_CN
				F_ENE
				F_WC
				F_CE
				F_SWW
				F_SC
				F_SEE
				F_SWS
				F_SSE
				And the nodal points:
				x_NW
				x_N
				x_NE
				x_W
				x_C
				x_E
				x_SW
				x_S
				x_SE

		        This fills A and B such that by solving A*phi=b, we obtain an approximation to:

		        div(u(x,y) phi(x,y)-e(x,y) grad(phi(x,y))=s(x,y)-sl(x,y)*phi(x,y)
		        It is assumed that u and e are piecewise constant between nodes.
	*/
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	//TODO: Work out alpha_hat and beta_hat, 1.0 and 1.0 should be ok
	double alpha_hat = 1.0;
	double beta_hat = 1.0;

	//Prepare A and b
	//Reserve 5 non-zeros per row (5-point stencil)
	//TODO: Check if this size is ok
	A.resize(input.N_x * input.N_y, input.N_x * input.N_y);
	A.setZero();
	A.reserve(Eigen::VectorXi::Constant(input.N_x * input.N_y, 9));
	b.resize(input.N_x * input.N_y, 1);
	b.setZero();

	//Define variables needed to construct the discretization
	//Allocate arrays for the flux coefficients
	enum coefficient {alpha, beta, gamma, delta};
	std::array<double, 4> F_NWN;
	std::array<double, 4> F_NNE;
	std::array<double, 4> F_WNW;
	std::array<double, 4> F_CN;
	std::array<double, 4> F_ENE;
	std::array<double, 4> F_WC;
	std::array<double, 4> F_CE;
	std::array<double, 4> F_SWW;
	std::array<double, 4> F_SC;
	std::array<double, 4> F_SEE;
	std::array<double, 4> F_SWS;
	std::array<double, 4> F_SSE;

	//Indices of neighbouring cells
	int ix_NW;
	int ix_N;
	int ix_NE;
	int ix_W;
	int ix_C;
	int ix_E;
	int ix_SW;
	int ix_S;
	int ix_SE;
	double x, y; //Position of the nodal point

	for (int i = 0; i < input.N_x; i++)
	{


		for (int j = 0; j < input.N_y; j++)
		{
			//Indices of neighbouring cells
			ix_NW = (i - 1) + input.N_x * (j + 1);
			ix_N =  (i + 0) + input.N_x * (j + 1);
			ix_NE = (i + 1) + input.N_x * (j + 1);
			ix_W =  (i - 1) + input.N_x * (j + 0);
			ix_C =  (i + 0) + input.N_x * (j + 0);
			ix_E =  (i + 1) + input.N_x * (j + 0);
			ix_SW = (i - 1) + input.N_x * (j - 1);
			ix_S =  (i + 0) + input.N_x * (j - 1);
			ix_SE = (i + 1) + input.N_x * (j - 1);

			//Position of the nodal point
			x = input.x_w + i * dx;
			y = input.y_s + j * dy;

			//Work out the Dirichlet boundary conditions
			if (i == 0)
			{
				//West boundary
				A.coeffRef(ix_C, ix_C) += 1;
				b(ix_C) = input.phi_w(y);
			}
			else if (i == input.N_x - 1)
			{
				//East boundary
				A.coeffRef(ix_C, ix_C) += 1;
				b(ix_C) = input.phi_e(y);
			}
			else if (j == 0)
			{
				//South boundary
				A.coeffRef(ix_C, ix_C) += 1;
				b(ix_C) = input.phi_s(x);
			}
			else if (j == input.N_y - 1)
			{
				//North boundary
				A.coeffRef(ix_C, ix_C) += 1;
				b(ix_C) = input.phi_n(x);
			}
			else
			{
				//NOTE: For efficient assembly it would probably be faster to first compute all fluxes in the entire domain
				//Instead of considering each cell individually, currently many flux calculations are not recycled.
				F_NWN = input.flux(input.u_x(x - dx / 2, y + dy), input.e(x - dx / 2, y + dy), dx);
				F_NNE = input.flux(input.u_x(x + dx / 2, y + dy), input.e(x + dx / 2, y + dy), dx);
				F_WNW = input.flux(input.u_y(x - dx, y + dy / 2), input.e(x - dx, y + dy / 2), dy);
				F_CN  = input.flux(input.u_y(x, y + dy / 2), input.e(x, y + dy / 2), dy);
				F_ENE = input.flux(input.u_y(x + dx, y + dy / 2), input.e(x + dx, y + dy / 2), dy);
				F_WC  = input.flux(input.u_x(x - dx / 2, y), input.e(x - dx / 2, y), dx);
				F_CE  = input.flux(input.u_x(x + dx / 2, y), input.e(x + dx / 2, y), dx);
				F_SWW = input.flux(input.u_y(x - dx, y - dy / 2), input.e(x - dx, y - dy / 2), dy);
				F_SC  = input.flux(input.u_y(x, y - dy / 2), input.e(x, y - dy / 2), dy);
				F_SEE = input.flux(input.u_y(x + dx, y - dy / 2), input.e(x + dx, y - dy / 2), dy);
				F_SWS = input.flux(input.u_x(x - dx / 2, y - dy), input.e(x - dx / 2, y - dy), dx);
				F_SSE = input.flux(input.u_x(x + dx / 2, y - dy), input.e(x + dx / 2, y - dy), dx);

				//Not at a boundary
				//CF Scheme without the effects of cross flux:

				//North flux
				A.coeffRef(ix_C, ix_S) += 0;
				A.coeffRef(ix_C, ix_C) += dx * F_CN[alpha];
				A.coeffRef(ix_C, ix_N) += dx * F_CN[beta];
				b(ix_C) -= dy * (F_CN[gamma] * alpha_hat * input.s(x, y) + F_CN[delta] * alpha_hat * input.s(x, y + dy));
				//East flux
				A.coeffRef(ix_C, ix_W) += 0;
				A.coeffRef(ix_C, ix_C) += dy * F_CE[alpha];
				A.coeffRef(ix_C, ix_E) += dy * F_CE[beta];
				b(ix_C) -= dx * (F_CE[gamma] * alpha_hat * input.s(x, y) + F_CE[delta] * alpha_hat * input.s(x + dx, y));
				//South flux
				A.coeffRef(ix_C, ix_S) -= dx * F_SC[alpha];
				A.coeffRef(ix_C, ix_C) -= dx * F_SC[beta];
				A.coeffRef(ix_C, ix_N) -= 0;
				b(ix_C) += dy * (F_SC[gamma] * alpha_hat * input.s(x, y - dy) + F_SC[delta] * alpha_hat * input.s(x, y));
				//West flux
				A.coeffRef(ix_C, ix_W) -= dy * F_WC[alpha];
				A.coeffRef(ix_C, ix_C) -= dy * F_WC[beta];
				A.coeffRef(ix_C, ix_E) -= 0;
				b(ix_C) += dx * (F_WC[gamma] * alpha_hat * input.s(x - dx, y) + F_WC[delta] * alpha_hat * input.s(x, y));
				//Linear source
				A.coeffRef(ix_C, ix_C) += dx * dy * input.sl(x, y);
				//Other source
				b(ix_C) += dx * dy * input.s(x, y);

				//Cross flux in F_CE
				//x_{i,j} part
				A.coeffRef(ix_C, ix_C) += (-beta_hat * dx * F_CE[gamma]) * F_CN[alpha] / dy;
				A.coeffRef(ix_C, ix_N) += (-beta_hat * dx * F_CE[gamma]) * F_CN[beta] / dy;
				A.coeffRef(ix_C, ix_S) -= (-beta_hat * dx * F_CE[gamma]) * F_SC[alpha] / dy;
				A.coeffRef(ix_C, ix_C) -= (-beta_hat * dx * F_CE[gamma]) * F_SC[beta] / dy;
				//x_{i+1,j} part
				A.coeffRef(ix_C, ix_E) += (-beta_hat * dx * F_CE[delta]) * F_ENE[alpha] / dy;
				A.coeffRef(ix_C, ix_NE) += (-beta_hat * dx * F_CE[delta]) * F_ENE[beta] / dy;
				A.coeffRef(ix_C, ix_SE) -= (-beta_hat * dx * F_CE[delta]) * F_SEE[alpha] / dy;
				A.coeffRef(ix_C, ix_E) -= (-beta_hat * dx * F_CE[delta]) * F_SEE[beta] / dy;

				//Cross flux in F_WC
				//x_{i-1,j} part
				A.coeffRef(ix_C, ix_W) += (beta_hat * dx * F_WC[gamma]) * F_WNW[alpha] / dy;
				A.coeffRef(ix_C, ix_NW) += (beta_hat * dx * F_WC[gamma]) * F_WNW[beta] / dy;
				A.coeffRef(ix_C, ix_SW) -= (beta_hat * dx * F_WC[gamma]) * F_SWW[alpha] / dy;
				A.coeffRef(ix_C, ix_W) -= (beta_hat * dx * F_WC[gamma]) * F_SWW[beta] / dy;
				//x_{i,j} part
				A.coeffRef(ix_C, ix_C) += (beta_hat * dx * F_WC[delta]) * F_CN[alpha] / dy;
				A.coeffRef(ix_C, ix_N) += (beta_hat * dx * F_WC[delta]) * F_CN[beta] / dy;
				A.coeffRef(ix_C, ix_S) -= (beta_hat * dx * F_WC[delta]) * F_SC[alpha] / dy;
				A.coeffRef(ix_C, ix_C) -= (beta_hat * dx * F_WC[delta]) * F_SC[beta] / dy;

				//Cross flux in F_CN
				//TODO: Double check all cross flux parts after completing
				//TODO: Make convergence test to check if all schemes are implemented properly
				//TODO: Documentation in code
				//TODO: Documentation in paper (especially the notation)
				//TODO: Merge 2d and 2d cfcf functions?
				//x_{i,j} part
				A.coeffRef(ix_C, ix_C) += (-beta_hat * dy * F_CN[gamma]) * F_CE[alpha] / dx;
				A.coeffRef(ix_C, ix_E) += (-beta_hat * dy * F_CN[gamma]) * F_CE[beta] / dx;
				A.coeffRef(ix_C, ix_W) -= (-beta_hat * dy * F_CN[gamma]) * F_WC[alpha] / dx;
				A.coeffRef(ix_C, ix_C) -= (-beta_hat * dy * F_CN[gamma]) * F_WC[beta] / dx;
				//x_{i,j+1} part
				A.coeffRef(ix_C, ix_N)  += (-beta_hat * dy * F_CN[delta]) * F_NNE[alpha] / dx;
				A.coeffRef(ix_C, ix_NE) += (-beta_hat * dy * F_CN[delta]) * F_NNE[beta] / dx;
				A.coeffRef(ix_C, ix_NW) -= (-beta_hat * dy * F_CN[delta]) * F_NWN[alpha] / dx;
				A.coeffRef(ix_C, ix_N)  -= (-beta_hat * dy * F_CN[delta]) * F_NWN[beta] / dx;

				//Cross flux in F_SC
				//x_{i,j-1} part
				A.coeffRef(ix_C, ix_S)  += (beta_hat * dy * F_SC[gamma]) * F_SSE[alpha] / dx;
				A.coeffRef(ix_C, ix_SE) += (beta_hat * dy * F_SC[gamma]) * F_SSE[beta] / dx;
				A.coeffRef(ix_C, ix_SW) -= (beta_hat * dy * F_SC[gamma]) * F_SWS[alpha] / dx;
				A.coeffRef(ix_C, ix_S)  -= (beta_hat * dy * F_SC[gamma]) * F_SWS[beta] / dx;
				//x_{i,j} part
				A.coeffRef(ix_C, ix_C) += (beta_hat * dy * F_SC[delta]) * F_CE[alpha] / dx;
				A.coeffRef(ix_C, ix_E) += (beta_hat * dy * F_SC[delta]) * F_CE[beta] / dx;
				A.coeffRef(ix_C, ix_W) -= (beta_hat * dy * F_SC[delta]) * F_WC[alpha] / dx;
				A.coeffRef(ix_C, ix_C) -= (beta_hat * dy * F_SC[delta]) * F_WC[beta] / dx;
			}
		}

	}

	A.makeCompressed();
}
void discretizer::discretizecf(const Input3D& input, SparseMatrixType& A, VectorType& b)
{
	/*
	        TODO:
	        -crossflux
	        -Set expected number of equations/row
	        -Check result
	        Input:
	        Input3D struct
	        Reference to discretization matrix A
	        Reference to rhs vector A

	        This fills A and B such that by solving A*phi=b, we obtain an approximation to:

	        div(u(x,y,z) phi(x,y,z)-e(x,y,z) grad(phi(x,y,z))=s(x,y,z)-sl(x,y,z)*phi(x,y,z)
	        It is assumed that u and e are piecewise constant between nodes.
	*/
	/*
		        TODO:
		        -crossflux
		        -Set expected number of equations/row
		        -Check result
		        Input:
		        Input2D struct
		        Reference to discretization matrix A
		        Reference to rhs vector A

		        This fills A and B such that by solving A*phi=b, we obtain an approximation to:

		        div(u(x,y) phi(x,y)-e(x,y) grad(phi(x,y))=s(x,y)-sl(x,y)*phi(x,y)
		        It is assumed that u and e are piecewise constant between nodes.
	*/
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	double dz = (input.z_u - input.z_d) / (input.N_z - 1);
	//TODO: Work out alpha_hat and beta_hat, 1.0 and 1.0 should be ok
	double alpha_hat = 1.0;
	double beta_hat = 1.0;

	//Prepare A and b
	//Reserve 5 non-zeros per row (5-point stencil)
	//TODO: Check if this size is ok
	A.resize(input.N_x * input.N_y * input.N_z, input.N_x * input.N_y * input.N_z);
	A.setZero();
	A.reserve(Eigen::VectorXi::Constant(input.N_x * input.N_y * input.N_z, 19));
	b.resize(input.N_x * input.N_y * input.N_z, 1);
	b.setZero();

	//Define variables needed to construct the discretization
	//Allocate arrays for the flux coefficients
	enum coefficient {alpha, beta, gamma, delta};
	// std::array<double, 4> F_e;	//Flux through eastern edge
	// std::array<double, 4> F_w;	//Flux through western edge
	// std::array<double, 4> F_n;	//Flux through northern edge
	// std::array<double, 4> F_s;	//Flux through southern edge
	// std::array<double, 4> F_u;	//Flux through up edge
	// std::array<double, 4> F_d;	//Flux through down edge
	// int ix_c, ix_e, ix_w, ix_n, ix_s, ix_u, ix_d; //Indices of neighbouring cells

	//Allocate arrays for the flux coefficients
	//Each vertex connecting two edges nodes in the compass stencil
	//Fluxes oriented in +x+y+z direction
	std::array<double, 4> F_UWUNW, F_UCUN, F_UEUNE, F_USWUW, F_USUC, F_USEUE, F_UNWUN, F_UNUNE, F_UWUC, F_UCUE, F_USWUS, F_USUSE, F_WNW, F_CN, F_ENE, F_SWW, F_SC, F_SEE, F_NWN, F_NNE, F_WC, F_CE, F_SWS,
	    F_SSE, F_DWDNW, F_DCDN, F_DEDNE, F_DSWDW, F_DSDC, F_DSEDE, F_DNWDN, F_DNDNE, F_DWDC, F_DCDE, F_DSWDS, F_DSDSE, F_DNWNW, F_DNN, F_DNENE, F_DWW, F_DCC, F_DEE, F_DSWSW, F_DSS, F_DSESE, F_NWUNW, F_NUN,
	    F_NEUNE, F_WUW, F_CUC, F_EUE, F_SWUSW, F_SUS, F_SEUSE;	//Indices of neighbouring cells
	//Each edge in the compass stencil
	int ix_UNW, ix_UN, ix_UNE, ix_UW, ix_UC, ix_UE, ix_USW, ix_US, ix_USE, ix_NW, ix_N, ix_NE, ix_W,
	    ix_C, ix_E, ix_SW, ix_S, ix_SE, ix_DNW, ix_DN, ix_DNE, ix_DW, ix_DC, ix_DE, ix_DSW, ix_DS, ix_DSE;
	double x, y, z; //Position of the nodal point

	for (int i = 0; i < input.N_x; i++)
	{
		for (int j = 0; j < input.N_y; j++)
		{
			for (int k = 0; k < input.N_z; k++)
			{
				//Indices of neighbouring cells
				ix_UNW = (i - 1) + input.N_x * (j + 1) + input.N_x * input.N_y * (k + 1);
				ix_UN  = (i + 0) + input.N_x * (j + 1) + input.N_x * input.N_y * (k + 1);
				ix_UNE = (i + 1) + input.N_x * (j + 1) + input.N_x * input.N_y * (k + 1);
				ix_UW  = (i - 1) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 1);
				ix_UC  = (i + 0) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 1);
				ix_UE  = (i + 1) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 1);
				ix_USW = (i - 1) + input.N_x * (j - 1) + input.N_x * input.N_y * (k + 1);
				ix_US  = (i + 0) + input.N_x * (j - 1) + input.N_x * input.N_y * (k + 1);
				ix_USE = (i + 1) + input.N_x * (j - 1) + input.N_x * input.N_y * (k + 1);
				ix_NW  = (i - 1) + input.N_x * (j + 1) + input.N_x * input.N_y * (k + 0);
				ix_N   = (i + 0) + input.N_x * (j + 1) + input.N_x * input.N_y * (k + 0);
				ix_NE  = (i + 1) + input.N_x * (j + 1) + input.N_x * input.N_y * (k + 0);
				ix_W   = (i - 1) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 0);
				ix_C   = (i + 0) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 0);
				ix_E   = (i + 1) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 0);
				ix_SW  = (i - 1) + input.N_x * (j - 1) + input.N_x * input.N_y * (k + 0);
				ix_S   = (i + 0) + input.N_x * (j - 1) + input.N_x * input.N_y * (k + 0);
				ix_SE  = (i + 1) + input.N_x * (j - 1) + input.N_x * input.N_y * (k + 0);
				ix_DNW = (i - 1) + input.N_x * (j + 1) + input.N_x * input.N_y * (k - 1);
				ix_DN  = (i + 0) + input.N_x * (j + 1) + input.N_x * input.N_y * (k - 1);
				ix_DNE = (i + 1) + input.N_x * (j + 1) + input.N_x * input.N_y * (k - 1);
				ix_DW  = (i - 1) + input.N_x * (j + 0) + input.N_x * input.N_y * (k - 1);
				ix_DC  = (i + 0) + input.N_x * (j + 0) + input.N_x * input.N_y * (k - 1);
				ix_DE  = (i + 1) + input.N_x * (j + 0) + input.N_x * input.N_y * (k - 1);
				ix_DSW = (i - 1) + input.N_x * (j - 1) + input.N_x * input.N_y * (k - 1);
				ix_DS  = (i + 0) + input.N_x * (j - 1) + input.N_x * input.N_y * (k - 1);
				ix_DSE = (i + 1) + input.N_x * (j - 1) + input.N_x * input.N_y * (k - 1);

				//Position of the nodal point
				x = input.x_w + i * dx;
				y = input.y_s + j * dy;
				z = input.z_d + k * dz;

				//Work out the Dirichlet boundary conditions
				if (i == 0)
				{
					//West boundary
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_w(y, z);
				}
				else if (i == input.N_x - 1)
				{
					//East boundary
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_e(y, z);
				}
				else if (j == 0)
				{
					//South boundary
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_s(x, z);
				}
				else if (j == input.N_y - 1)
				{
					//North boundary
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_n(x, z);
				}
				else if (k == 0)
				{
					//Down boundary
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_d(x, y);
				}
				else if (k == input.N_z - 1)
				{
					//Up boundary
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_u(x, y);
				}
				else
				{
					//Up layer,  going to +y (Expected 6)
					F_UWUNW = input.flux(input.u_y(x - dx, y + dy / 2, z + dz), input.e(x - dx, y + dy / 2, z + dz), dy);
					F_UCUN  = input.flux(input.u_y(x + 00, y + dy / 2, z + dz), input.e(x + 00, y + dy / 2, z + dz), dy);
					F_UEUNE = input.flux(input.u_y(x + dx, y + dy / 2, z + dz), input.e(x + dx, y + dy / 2, z + dz), dy);
					F_USWUW = input.flux(input.u_y(x - dx, y + dy / 2, z + dz), input.e(x - dx, y + dy / 2, z + dz), dy);
					F_USUC  = input.flux(input.u_y(x + 00, y + dy / 2, z + dz), input.e(x + 00, y + dy / 2, z + dz), dy);
					F_USEUE = input.flux(input.u_y(x + dx, y + dy / 2, z + dz), input.e(x + dx, y + dy / 2, z + dz), dy);

					//Up layer,  going to +x (Expected 6)
					F_UNWUN = input.flux(input.u_x(x - dx / 2, y + dy, z + dz), input.e(x - dx / 2, y + dy, z + dz), dx);
					F_UNUNE = input.flux(input.u_x(x + dx / 2, y + dy, z + dz), input.e(x + dx / 2, y + dy, z + dz), dx);
					F_UWUC  = input.flux(input.u_x(x - dx / 2, y + 00, z + dz), input.e(x - dx / 2, y + 00, z + dz), dx);
					F_UCUE  = input.flux(input.u_x(x + dx / 2, y + 00, z + dz), input.e(x + dx / 2, y + 00, z + dz), dx);
					F_USWUS = input.flux(input.u_x(x - dx / 2, y - dy, z + dz), input.e(x - dx / 2, y - dy, z + dz), dx);
					F_USUSE = input.flux(input.u_x(x + dx / 2, y - dy, z + dz), input.e(x + dx / 2, y - dy, z + dz), dx);

					//Mid layer, going to +y (Expected 6)
					F_WNW = input.flux(input.u_y(x - dx, y + dy / 2, z + 00 ), input.e(x - dx, y + dy / 2, z + 00 ), dy);
					F_CN  = input.flux(input.u_y(x + 00, y + dy / 2, z + 00 ), input.e(x + 00, y + dy / 2, z + 00 ), dy);
					F_ENE = input.flux(input.u_y(x + dx, y + dy / 2, z + 00 ), input.e(x + dx, y + dy / 2, z + 00 ), dy);
					F_SWW = input.flux(input.u_y(x - dx, y + dy / 2, z + 00 ), input.e(x - dx, y + dy / 2, z + 00 ), dy);
					F_SC  = input.flux(input.u_y(x + 00, y + dy / 2, z + 00 ), input.e(x + 00, y + dy / 2, z + 00 ), dy);
					F_SEE = input.flux(input.u_y(x + dx, y + dy / 2, z + 00 ), input.e(x + dx, y + dy / 2, z + 00 ), dy);

					//Mid layer, going to +x (Expected 6)
					F_NWN = input.flux(input.u_x(x - dx / 2, y + dy, z + 00 ), input.e(x - dx / 2, y + dy, z + 00 ), dx);
					F_NNE = input.flux(input.u_x(x + dx / 2, y + dy, z + 00 ), input.e(x + dx / 2, y + dy, z + 00 ), dx);
					F_WC  = input.flux(input.u_x(x - dx / 2, y + 00, z + 00 ), input.e(x - dx / 2, y + 00, z + 00 ), dx);
					F_CE  = input.flux(input.u_x(x + dx / 2, y + 00, z + 00 ), input.e(x + dx / 2, y + 00, z + 00 ), dx);
					F_SWS = input.flux(input.u_x(x - dx / 2, y - dy, z + 00 ), input.e(x - dx / 2, y - dy, z + 00 ), dx);
					F_SSE = input.flux(input.u_x(x + dx / 2, y - dy, z + 00 ), input.e(x + dx / 2, y - dy, z + 00 ), dx);

					//Bot layer, going to +y (Expected 6)
					F_DWDNW = input.flux(input.u_y(x - dx, y + dy / 2, z - dz), input.e(x - dx, y + dy / 2, z - dz), dy);
					F_DCDN  = input.flux(input.u_y(x + 00, y + dy / 2, z - dz), input.e(x + 00, y + dy / 2, z - dz), dy);
					F_DEDNE = input.flux(input.u_y(x + dx, y + dy / 2, z - dz), input.e(x + dx, y + dy / 2, z - dz), dy);
					F_DSWDW = input.flux(input.u_y(x - dx, y + dy / 2, z - dz), input.e(x - dx, y + dy / 2, z - dz), dy);
					F_DSDC  = input.flux(input.u_y(x + 00, y + dy / 2, z - dz), input.e(x + 00, y + dy / 2, z - dz), dy);
					F_DSEDE = input.flux(input.u_y(x + dx, y + dy / 2, z - dz), input.e(x + dx, y + dy / 2, z - dz), dy);

					//Bot layer, going to +x (Expected 6)
					F_DNWDN = input.flux(input.u_x(x - dx / 2, y + dy, z - dz), input.e(x - dx / 2, y + dy, z - dz), dx);
					F_DNDNE = input.flux(input.u_x(x + dx / 2, y + dy, z - dz), input.e(x + dx / 2, y + dy, z - dz), dx);
					F_DWDC  = input.flux(input.u_x(x - dx / 2, y + 00, z - dz), input.e(x - dx / 2, y + 00, z - dz), dx);
					F_DCDE  = input.flux(input.u_x(x + dx / 2, y + 00, z - dz), input.e(x + dx / 2, y + 00, z - dz), dx);
					F_DSWDS = input.flux(input.u_x(x - dx / 2, y - dy, z - dz), input.e(x - dx / 2, y - dy, z - dz), dx);
					F_DSDSE = input.flux(input.u_x(x + dx / 2, y - dy, z - dz), input.e(x + dx / 2, y - dy, z - dz), dx);

					//From bot, going +z (Expected 9)
					F_DNWNW = input.flux(input.u_z(x - dx, y + dy, z - dz / 2), input.e(x - dx, y + dy, z - dz / 2), dz);
					F_DNN   = input.flux(input.u_z(x + 00, y + dy, z - dz / 2), input.e(x + 00, y + dy, z - dz / 2), dz);
					F_DNENE = input.flux(input.u_z(x + dx, y + dy, z - dz / 2), input.e(x + dx, y + dy, z - dz / 2), dz);
					F_DWW   = input.flux(input.u_z(x - dx, y + 00, z - dz / 2), input.e(x - dx, y + 00, z - dz / 2), dz);
					F_DCC   = input.flux(input.u_z(x + 00, y + 00, z - dz / 2), input.e(x + 00, y + 00, z - dz / 2), dz);
					F_DEE   = input.flux(input.u_z(x + dx, y + 00, z - dz / 2), input.e(x + dx, y + 00, z - dz / 2), dz);
					F_DSWSW = input.flux(input.u_z(x - dx, y - dy, z - dz / 2), input.e(x - dx, y - dy, z - dz / 2), dz);
					F_DSS   = input.flux(input.u_z(x + 00, y - dy, z - dz / 2), input.e(x + 00, y - dy, z - dz / 2), dz);
					F_DSESE = input.flux(input.u_z(x + dx, y - dy, z - dz / 2), input.e(x + dx, y - dy, z - dz / 2), dz);

					//From mid, going +z (Expected 9)
					F_NWUNW = input.flux(input.u_z(x - dx, y + dy, z + dz / 2), input.e(x - dx, y + dy, z + dz / 2), dz);
					F_NUN   = input.flux(input.u_z(x + 00, y + dy, z + dz / 2), input.e(x + 00, y + dy, z + dz / 2), dz);
					F_NEUNE = input.flux(input.u_z(x + dx, y + dy, z + dz / 2), input.e(x + dx, y + dy, z + dz / 2), dz);
					F_WUW   = input.flux(input.u_z(x - dx, y + 00, z + dz / 2), input.e(x - dx, y + 00, z + dz / 2), dz);
					F_CUC   = input.flux(input.u_z(x + 00, y + 00, z + dz / 2), input.e(x + 00, y + 00, z + dz / 2), dz);
					F_EUE   = input.flux(input.u_z(x + dx, y + 00, z + dz / 2), input.e(x + dx, y + 00, z + dz / 2), dz);
					F_SWUSW = input.flux(input.u_z(x - dx, y - dy, z + dz / 2), input.e(x - dx, y - dy, z + dz / 2), dz);
					F_SUS   = input.flux(input.u_z(x + 00, y - dy, z + dz / 2), input.e(x + 00, y - dy, z + dz / 2), dz);
					F_SEUSE = input.flux(input.u_z(x + dx, y - dy, z + dz / 2), input.e(x + dx, y - dy, z + dz / 2), dz);

					//Non-cross fluxes:
					//North flux
					A.coeffRef(ix_C, ix_S) += 0;
					A.coeffRef(ix_C, ix_C) += dx * dz * F_CN[alpha];
					A.coeffRef(ix_C, ix_N) += dx * dz * F_CN[beta];
					b(ix_C) -= dy * (F_CN[gamma] * alpha_hat * input.s(x, y, z) + F_CN[delta] * alpha_hat * input.s(x, y + dy, z));
					//East flux
					A.coeffRef(ix_C, ix_W) += 0;
					A.coeffRef(ix_C, ix_C) += dy * dz * F_CE[alpha];
					A.coeffRef(ix_C, ix_E) += dy * dz * F_CE[beta];
					b(ix_C) -= dx * (F_CE[gamma] * alpha_hat * input.s(x, y, z) + F_CE[delta] * alpha_hat * input.s(x + dx, y, z));
					//South flux
					A.coeffRef(ix_C, ix_S) -= dx * dz * F_SC[alpha];
					A.coeffRef(ix_C, ix_C) -= dx * dz * F_SC[beta];
					A.coeffRef(ix_C, ix_N) -= 0;
					b(ix_C) += dy * (F_SC[gamma] * alpha_hat * input.s(x, y - dy, z) + F_SC[delta] * alpha_hat * input.s(x, y, z));
					//West flux
					A.coeffRef(ix_C, ix_W) -= dy * dz * F_WC[alpha];
					A.coeffRef(ix_C, ix_C) -= dy * dz * F_WC[beta];
					A.coeffRef(ix_C, ix_E) -= 0;
					b(ix_C) += dx * (F_WC[gamma] * alpha_hat * input.s(x - dx, y, z) + F_WC[delta] * alpha_hat * input.s(x, y, z));
					//Up flux
					A.coeffRef(ix_C, ix_UC) += 0;
					A.coeffRef(ix_C, ix_C) += dx * dy * F_CUC[alpha];
					A.coeffRef(ix_C, ix_DC) += dx * dy * F_CUC[beta];
					b(ix_C) -= dz * (F_CUC[gamma] * alpha_hat * input.s(x, y, z) + F_CUC[delta] * alpha_hat * input.s(x, y, z + dz));
					//Down flux
					A.coeffRef(ix_C, ix_UC) -= dx * dy * F_DCC[alpha];
					A.coeffRef(ix_C, ix_C) -= dx * dy * F_DCC[beta];
					A.coeffRef(ix_C, ix_DC) -= 0;
					b(ix_C) += dz * (F_DCC[gamma] * alpha_hat * input.s(x, y, z - dz) + F_DCC[delta] * alpha_hat * input.s(x, y, z));
					//Linear source
					A.coeffRef(ix_C, ix_C) += dx * dy * dz * input.sl(x, y, z);
					//Other source
					b(ix_C) += dx * dy * dz * input.s(x, y, z);

					//Cross fluxes:
					//F_CN (+y)
					//x_{i,j,k}=x_C, GAMMA
					//df/dx=(F_CE-F_WC)/dx=(alpha_CE*phi_C+beta_CE*phi_E-alpha_WC*phi_W-beta_WC*phi_C)
					A.coeffRef(ix_C, ix_C) += (-beta_hat * dy * F_CN[gamma]) * F_CE[alpha] / dx;
					A.coeffRef(ix_C, ix_E) += (-beta_hat * dy * F_CN[gamma]) * F_CE[beta] / dx;
					A.coeffRef(ix_C, ix_W) -= (-beta_hat * dy * F_CN[gamma]) * F_WC[alpha] / dx;
					A.coeffRef(ix_C, ix_C) -= (-beta_hat * dy * F_CN[gamma]) * F_WC[beta] / dx;
					//df/dz=(F_CUC-F_DCC)/dz=(alpha_CUC*phi_C+beta_CUC*phi_UC-alpha_DCC*phi_DC-beta_DCC*phi_C)
					A.coeffRef(ix_C, ix_C)  += (-beta_hat * dy * F_CN[gamma]) * F_CUC[alpha] / dz;
					A.coeffRef(ix_C, ix_UC) += (-beta_hat * dy * F_CN[gamma]) * F_CUC[beta] / dz;
					A.coeffRef(ix_C, ix_DC) -= (-beta_hat * dy * F_CN[gamma]) * F_DCC[alpha] / dz;
					A.coeffRef(ix_C, ix_C)  -= (-beta_hat * dy * F_CN[gamma]) * F_DCC[beta] / dz;
					//x_{i,j+1,k}=x_N, DELTA
					//df/dx=(F_NNE-F_NWN)/dx
					A.coeffRef(ix_C, ix_N)  += (-beta_hat * dy * F_CN[delta]) * F_NNE[alpha] / dx;
					A.coeffRef(ix_C, ix_NE) += (-beta_hat * dy * F_CN[delta]) * F_NNE[beta] / dx;
					A.coeffRef(ix_C, ix_NW) -= (-beta_hat * dy * F_CN[delta]) * F_NWN[alpha] / dx;
					A.coeffRef(ix_C, ix_N)  -= (-beta_hat * dy * F_CN[delta]) * F_NWN[beta] / dx;
					//df/dz=(F_NUN-F_DNN)/dz
					A.coeffRef(ix_C, ix_N)  += (-beta_hat * dy * F_CN[delta]) * F_NUN[alpha] / dz;
					A.coeffRef(ix_C, ix_UN) += (-beta_hat * dy * F_CN[delta]) * F_NUN[beta] / dz;
					A.coeffRef(ix_C, ix_DN) -= (-beta_hat * dy * F_CN[delta]) * F_DNN[alpha] / dz;
					A.coeffRef(ix_C, ix_N)  -= (-beta_hat * dy * F_CN[delta]) * F_DNN[beta] / dz;

					//F_CE (+x)
					//x_{i,j,k}=x_C
					//df/dy=(F_CN-F_SC)/dy
					A.coeffRef(ix_C, ix_C) += (-beta_hat * dx * F_CE[gamma]) * F_CN[alpha] / dy;
					A.coeffRef(ix_C, ix_N) += (-beta_hat * dx * F_CE[gamma]) * F_CN[beta] / dy;
					A.coeffRef(ix_C, ix_S) -= (-beta_hat * dx * F_CE[gamma]) * F_SC[alpha] / dy;
					A.coeffRef(ix_C, ix_C) -= (-beta_hat * dx * F_CE[gamma]) * F_SC[beta] / dy;
					//df/dz=(F_CUC-F_DCC)/dz
					A.coeffRef(ix_C, ix_C)  += (-beta_hat * dx * F_CE[gamma]) * F_CUC[alpha] / dz;
					A.coeffRef(ix_C, ix_UC) += (-beta_hat * dx * F_CE[gamma]) * F_CUC[beta] / dz;
					A.coeffRef(ix_C, ix_DC) -= (-beta_hat * dx * F_CE[gamma]) * F_DCC[alpha] / dz;
					A.coeffRef(ix_C, ix_C)  -= (-beta_hat * dx * F_CE[gamma]) * F_DCC[beta] / dz;
					//x_{i+1,j,k}=x_E
					//df/dy=(F_ENE-F_SEE)/dy
					A.coeffRef(ix_C, ix_E)  += (-beta_hat * dx * F_CE[delta]) * F_ENE[alpha] / dy;
					A.coeffRef(ix_C, ix_NE) += (-beta_hat * dx * F_CE[delta]) * F_ENE[beta] / dy;
					A.coeffRef(ix_C, ix_SE) -= (-beta_hat * dx * F_CE[delta]) * F_SEE[alpha] / dy;
					A.coeffRef(ix_C, ix_E)  -= (-beta_hat * dx * F_CE[delta]) * F_SEE[beta] / dy;
					//df/dz=(F_EUE-F_DEE)/dz
					A.coeffRef(ix_C, ix_E)  += (-beta_hat * dx * F_CE[delta]) * F_EUE[alpha] / dz;
					A.coeffRef(ix_C, ix_UE) += (-beta_hat * dx * F_CE[delta]) * F_EUE[beta] / dz;
					A.coeffRef(ix_C, ix_DE) -= (-beta_hat * dx * F_CE[delta]) * F_DEE[alpha] / dz;
					A.coeffRef(ix_C, ix_E)  -= (-beta_hat * dx * F_CE[delta]) * F_DEE[beta] / dz;

					//F_SC (-y)
					//x_{i,j-1,k}=x_S
					//df/dx=(F_SSE-F_SWS)/dx
					A.coeffRef(ix_C, ix_S)  += (+beta_hat * dy * F_SC[gamma]) * F_SSE[alpha] / dx;
					A.coeffRef(ix_C, ix_SE) += (+beta_hat * dy * F_SC[gamma]) * F_SSE[beta] / dx;
					A.coeffRef(ix_C, ix_SW) -= (+beta_hat * dy * F_SC[gamma]) * F_SWS[alpha] / dx;
					A.coeffRef(ix_C, ix_S)  -= (+beta_hat * dy * F_SC[gamma]) * F_SWS[beta] / dx;
					//df/dz=(F_SUS-F_DSS)/dz
					A.coeffRef(ix_C, ix_S)  += (+beta_hat * dy * F_SC[gamma]) * F_SUS[alpha] / dz;
					A.coeffRef(ix_C, ix_US) += (+beta_hat * dy * F_SC[gamma]) * F_SUS[beta] / dz;
					A.coeffRef(ix_C, ix_DS) -= (+beta_hat * dy * F_SC[gamma]) * F_DSS[alpha] / dz;
					A.coeffRef(ix_C, ix_S)  -= (+beta_hat * dy * F_SC[gamma]) * F_DSS[beta] / dz;
					//x_{i,j,k}=x_C
					//df/dx=(F_CE-F_WC)/dx
					A.coeffRef(ix_C, ix_C) += (+beta_hat * dy * F_SC[delta]) * F_CE[alpha] / dx;
					A.coeffRef(ix_C, ix_E) += (+beta_hat * dy * F_SC[delta]) * F_CE[beta] / dx;
					A.coeffRef(ix_C, ix_W) -= (+beta_hat * dy * F_SC[delta]) * F_WC[alpha] / dx;
					A.coeffRef(ix_C, ix_C) -= (+beta_hat * dy * F_SC[delta]) * F_WC[beta] / dx;
					//df/dz=(F_CUC-F_DCC)/dz
					A.coeffRef(ix_C, ix_C)  += (+beta_hat * dy * F_SC[delta]) * F_CUC[alpha] / dz;
					A.coeffRef(ix_C, ix_UC) += (+beta_hat * dy * F_SC[delta]) * F_CUC[beta] / dz;
					A.coeffRef(ix_C, ix_DC) -= (+beta_hat * dy * F_SC[delta]) * F_DCC[alpha] / dz;
					A.coeffRef(ix_C, ix_C)  -= (+beta_hat * dy * F_SC[delta]) * F_DCC[beta] / dz;

					//F_WC (-x)
					//x_{i-1,j,k}=x_W
					//df/dy=(F_WNW-F_SWW)/dy
					A.coeffRef(ix_C, ix_W)  += (+beta_hat * dx * F_WC[gamma]) * F_WNW[alpha] / dy;
					A.coeffRef(ix_C, ix_NW) += (+beta_hat * dx * F_WC[gamma]) * F_WNW[beta] / dy;
					A.coeffRef(ix_C, ix_SW) -= (+beta_hat * dx * F_WC[gamma]) * F_SWW[alpha] / dy;
					A.coeffRef(ix_C, ix_W)  -= (+beta_hat * dx * F_WC[gamma]) * F_SWW[beta] / dy;
					//df/dz=(F_WUW-F_DWW)/dz
					A.coeffRef(ix_C, ix_W)  += (+beta_hat * dx * F_WC[gamma]) * F_WUW[alpha] / dz;
					A.coeffRef(ix_C, ix_UW) += (+beta_hat * dx * F_WC[gamma]) * F_WUW[beta] / dz;
					A.coeffRef(ix_C, ix_DW) -= (+beta_hat * dx * F_WC[gamma]) * F_DWW[alpha] / dz;
					A.coeffRef(ix_C, ix_W)  -= (+beta_hat * dx * F_WC[gamma]) * F_DWW[beta] / dz;
					//x_{i,j,k}=x_C
					//df/dy=(F_CN-F_SC)/dy
					A.coeffRef(ix_C, ix_C) += (+beta_hat * dx * F_WC[delta]) * F_CN[alpha] / dy;
					A.coeffRef(ix_C, ix_N) += (+beta_hat * dx * F_WC[delta]) * F_CN[beta] / dy;
					A.coeffRef(ix_C, ix_S) -= (+beta_hat * dx * F_WC[delta]) * F_SC[alpha] / dy;
					A.coeffRef(ix_C, ix_C) -= (+beta_hat * dx * F_WC[delta]) * F_SC[beta] / dy;
					//df/dz=(F_CUC-F_DCC)/dz
					A.coeffRef(ix_C, ix_C)  += (+beta_hat * dx * F_WC[delta]) * F_CUC[alpha] / dz;
					A.coeffRef(ix_C, ix_UC) += (+beta_hat * dx * F_WC[delta]) * F_CUC[beta] / dz;
					A.coeffRef(ix_C, ix_DC) -= (+beta_hat * dx * F_WC[delta]) * F_DCC[alpha] / dz;
					A.coeffRef(ix_C, ix_C)  -= (+beta_hat * dx * F_WC[delta]) * F_DCC[beta] / dz;

					//F_CUC (+z)
					//x_{i,j,k}=x_C
					//df/dx=(F_CE-F_WC)/dx
					A.coeffRef(ix_C, ix_C) += (-beta_hat * dx * F_CUC[gamma]) * F_CE[alpha] / dx;
					A.coeffRef(ix_C, ix_E) += (-beta_hat * dx * F_CUC[gamma]) * F_CE[beta] / dx;
					A.coeffRef(ix_C, ix_W) -= (-beta_hat * dx * F_CUC[gamma]) * F_WC[alpha] / dx;
					A.coeffRef(ix_C, ix_C) -= (-beta_hat * dx * F_CUC[gamma]) * F_WC[beta] / dx;
					//df/dy=(F_CN-F_SC)/dy
					A.coeffRef(ix_C, ix_C) += (-beta_hat * dx * F_CUC[gamma]) * F_CN[alpha] / dy;
					A.coeffRef(ix_C, ix_N) += (-beta_hat * dx * F_CUC[gamma]) * F_CN[beta] / dy;
					A.coeffRef(ix_C, ix_S) -= (-beta_hat * dx * F_CUC[gamma]) * F_SC[alpha] / dy;
					A.coeffRef(ix_C, ix_C) -= (-beta_hat * dx * F_CUC[gamma]) * F_SC[beta] / dy;
					//x_{i,j,k+1}=x_UC
					//df/dx=(F_UCUE-F_UWUC)/dx
					A.coeffRef(ix_C, ix_UC) += (-beta_hat * dx * F_CUC[delta]) * F_UCUE[alpha] / dx;
					A.coeffRef(ix_C, ix_UE) += (-beta_hat * dx * F_CUC[delta]) * F_UCUE[beta] / dx;
					A.coeffRef(ix_C, ix_UW) -= (-beta_hat * dx * F_CUC[delta]) * F_UWUC[alpha] / dx;
					A.coeffRef(ix_C, ix_UC) -= (-beta_hat * dx * F_CUC[delta]) * F_UWUC[beta] / dx;
					//df/dy=(F_UCUN-F_USUC)/dy
					A.coeffRef(ix_C, ix_UC) += (-beta_hat * dx * F_CUC[delta]) * F_UCUN[alpha] / dy;
					A.coeffRef(ix_C, ix_UN) += (-beta_hat * dx * F_CUC[delta]) * F_UCUN[beta] / dy;
					A.coeffRef(ix_C, ix_US) -= (-beta_hat * dx * F_CUC[delta]) * F_USUC[alpha] / dy;
					A.coeffRef(ix_C, ix_UC) -= (-beta_hat * dx * F_CUC[delta]) * F_USUC[beta] / dy;

					//F_DCC (-z)
					//x_{i,j,k-1}=x_DC
					//df/dx=(F_DCDE-F_DWDC)/dx
					A.coeffRef(ix_C, ix_DC) += (+beta_hat * dx * F_DCC[gamma]) * F_DCDE[alpha] / dx;
					A.coeffRef(ix_C, ix_DE) += (+beta_hat * dx * F_DCC[gamma]) * F_DCDE[beta] / dx;
					A.coeffRef(ix_C, ix_DW) -= (+beta_hat * dx * F_DCC[gamma]) * F_DWDC[alpha] / dx;
					A.coeffRef(ix_C, ix_DC) -= (+beta_hat * dx * F_DCC[gamma]) * F_DWDC[beta] / dx;
					//df/dy=(F_DCDN-F_DSDC)/dy
					A.coeffRef(ix_C, ix_DC) += (+beta_hat * dx * F_DCC[gamma]) * F_DCDN[alpha] / dy;
					A.coeffRef(ix_C, ix_DN) += (+beta_hat * dx * F_DCC[gamma]) * F_DCDN[beta] / dy;
					A.coeffRef(ix_C, ix_DS) -= (+beta_hat * dx * F_DCC[gamma]) * F_DSDC[alpha] / dy;
					A.coeffRef(ix_C, ix_DC) -= (+beta_hat * dx * F_DCC[gamma]) * F_DSDC[beta] / dy;
					//x_{i,j,k}=x_C
					//df/dx=(F_CE-F_WC)/dx
					A.coeffRef(ix_C, ix_C) += (+beta_hat * dx * F_DCC[delta]) * F_CE[alpha] / dx;
					A.coeffRef(ix_C, ix_E) += (+beta_hat * dx * F_DCC[delta]) * F_CE[beta] / dx;
					A.coeffRef(ix_C, ix_W) -= (+beta_hat * dx * F_DCC[delta]) * F_WC[alpha] / dx;
					A.coeffRef(ix_C, ix_C) -= (+beta_hat * dx * F_DCC[delta]) * F_WC[beta] / dx;
					//df/dy=(F_CN-F_SC)/dy
					A.coeffRef(ix_C, ix_C) += (+beta_hat * dx * F_DCC[delta]) * F_CN[alpha] / dy;
					A.coeffRef(ix_C, ix_N) += (+beta_hat * dx * F_DCC[delta]) * F_CN[beta] / dy;
					A.coeffRef(ix_C, ix_S) -= (+beta_hat * dx * F_DCC[delta]) * F_SC[alpha] / dy;
					A.coeffRef(ix_C, ix_C) -= (+beta_hat * dx * F_DCC[delta]) * F_SC[beta] / dy;

					/*
					        //TODO: The corners USW,UNW,UNE,USE,DSW,DNW,DNE,DSE seem unused, is this correct?
					        The above part is only a 19 point stencil, what is missing?
					*/

				}
			}
		}

	}

	A.makeCompressed();
}
void discretizer::discretize(const Input1D& input, SparseMatrixType& A, VectorType& b)
{
	/*
	        TODO:
	        -Set expected number of equations/row
	        -Check result
	        Input:
	        Input1D struct
	        Reference to discretization matrix A
	        Reference to rhs vector A

	        This fills A and B such that by solving A*phi=b, we obtain an approximation to:

	        d/dx(u(x) phi(x)-e(x) dphi(x)/dx)=s(x)-sl(x)*phi(x)
	        It is assumed that u and e are piecewise constant between nodes.
	*/
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);

	//Prepare A and b
	//Reserve 3 non-zeros per row (3-point stencil)
	A.resize(input.N_x, input.N_x);
	A.setZero();
	A.reserve(Eigen::VectorXi::Constant(input.N_x, 3));
	b.resize(input.N_x, 1);
	b.setZero();

	//Dirichlet boundary conditions:
	A.coeffRef(0, 0) = 1;
	A.coeffRef(input.N_x - 1, input.N_x - 1) = 1;
	b(0) = input.phi_w; //phi(x=0)
	b(input.N_x - 1) = input.phi_e; //phi(x=1)

	//Allocate arrays for the flux coefficients
	enum coefficient {alpha, beta, gamma, delta};
	std::array<double, 4> F_right;
	std::array<double, 4> F_left;

	//Construct A and b
	for (size_t i = 1; i < input.N_x - 1; ++i)
	{
		//Loop over all non-boundary cells
		double x = input.x_w + i * dx;

		//Evaluate fluxes at the edges of the cell
		F_right = input.flux(input.u(x + 0.5 * dx), input.e(x + 0.5 * dx), dx);
		F_left = input.flux(input.u(x - 0.5 * dx), input.e(x - 0.5 * dx), dx);

		//Loop over all non-boundary cells
		//Conservation law: F_right-F_left=dx*(s-sl)
		//F_left
		A.coeffRef(i, i - 1) -= F_left[alpha];
		A.coeffRef(i, i) -= F_left[beta];
		A.coeffRef(i, i + 1) -= 0;

		//F_right
		A.coeffRef(i, i - 1) += 0;
		A.coeffRef(i, i) += F_right[alpha];
		A.coeffRef(i, i + 1) += F_right[beta];

		//Linear source
		A.coeffRef(i, i) += dx * input.sl(x);

		//F_left
		b(i) -= dx * (-F_left[gamma] * input.s(x - dx) + -F_left[delta] * input.s(x));
		//F_right
		b(i) -= dx * (F_right[gamma] * input.s(x) + F_right[delta] * input.s(x + dx));
		//Source
		b(i) += dx * input.s(x);
	}

	A.makeCompressed();

}
void discretizer::discretize(const Input2D& input, SparseMatrixType& A, VectorType& b)
{
	/*
	        TODO:
	        -crossflux
	        -Check result
	        Input:
	        Input2D struct
	        Reference to discretization matrix A
	        Reference to rhs vector A

	        This fills A and B such that by solving A*phi=b, we obtain an approximation to:

	        div(u(x,y) phi(x,y)-e(x,y) grad(phi(x,y))=s(x,y)-sl(x,y)*phi(x,y)
	        It is assumed that u and e are piecewise constant between nodes.
	*/
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);

	//Prepare A and b
	//Reserve 5 non-zeros per row (5-point stencil)
	//TODO: Check if this size is ok
	A.resize(input.N_x * input.N_y, input.N_x * input.N_y);
	A.setZero();
	A.reserve(Eigen::VectorXi::Constant(input.N_x * input.N_y, 5));
	b.resize(input.N_x * input.N_y, 1);
	b.setZero();

	//Define variables needed to construct the discretization
	//Allocate arrays for the flux coefficients
	enum coefficient {alpha, beta, gamma, delta};
	std::array<double, 4> F_e;	//Flux through eastern edge
	std::array<double, 4> F_w;	//Flux through western edge
	std::array<double, 4> F_n;	//Flux through northern edge
	std::array<double, 4> F_s;	//Flux through southern edge
	int ix_c, ix_e, ix_w, ix_n, ix_s; //Indices of neighbouring cells
	double x, y; //Position of the nodal point

	for (int i = 0; i < input.N_x; i++)
	{


		for (int j = 0; j < input.N_y; j++)
		{
			//Indices of neighbouring cells
			ix_c = i + input.N_x * j;
			ix_e = (i + 1) + input.N_x * j;
			ix_w = (i - 1) + input.N_x * j;
			ix_n = i + input.N_x * (j - 1);
			ix_s = i + input.N_x * (j + 1);

			//Position of the nodal point
			x = input.x_w + i * dx;
			y = input.y_s + j * dy;

			//Work out the Dirichlet boundary conditions
			if (i == 0)
			{
				//West boundary
				A.coeffRef(ix_c, ix_c) += 1;
				b(ix_c) = input.phi_w(y);
			}
			else if (i == input.N_x - 1)
			{
				//East boundary
				A.coeffRef(ix_c, ix_c) += 1;
				b(ix_c) = input.phi_e(y);
			}
			else if (j == 0)
			{
				//South boundary
				A.coeffRef(ix_c, ix_c) += 1;
				b(ix_c) = input.phi_s(x);
			}
			else if (j == input.N_y - 1)
			{
				//North boundary
				A.coeffRef(ix_c, ix_c) += 1;
				b(ix_c) = input.phi_n(x);
			}
			else
			{
				//Not at a boundary
				F_e = input.flux(input.u_x(x + dx / 2, y), input.e(x + dx / 2, y), dx);
				F_w = input.flux(input.u_x(x - dx / 2, y), input.e(x - dx / 2, y), dx);
				F_n = input.flux(input.u_y(x, y + dy / 2), input.e(x, y + dy / 2), dy);
				F_s = input.flux(input.u_y(x, y - dy / 2), input.e(x, y - dy / 2), dy);

				//North flux
				A.coeffRef(ix_c, ix_s) += 0;
				A.coeffRef(ix_c, ix_c) += dx * F_n[alpha];
				A.coeffRef(ix_c, ix_n) += dx * F_n[beta];
				b(ix_c) -= dy * (F_n[gamma] * input.s(x, y) + F_n[delta] * input.s(x, y + dy));
				//East flux
				A.coeffRef(ix_c, ix_w) += 0;
				A.coeffRef(ix_c, ix_c) += dy * F_e[alpha];
				A.coeffRef(ix_c, ix_e) += dy * F_e[beta];
				b(ix_c) -= dx * (F_e[gamma] * input.s(x, y) + F_e[delta] * input.s(x + dx, y));
				//South flux
				A.coeffRef(ix_c, ix_s) -= dx * F_s[alpha];
				A.coeffRef(ix_c, ix_c) -= dx * F_s[beta];
				A.coeffRef(ix_c, ix_n) -= 0;
				b(ix_c) += dy * (F_s[gamma] * input.s(x, y - dy) + F_s[delta] * input.s(x, y));
				//West flux
				A.coeffRef(ix_c, ix_w) -= dy * F_w[alpha];
				A.coeffRef(ix_c, ix_c) -= dy * F_w[beta];
				A.coeffRef(ix_c, ix_e) -= 0;
				b(ix_c) += dx * (F_w[gamma] * input.s(x - dx, y) + F_w[delta] * input.s(x, y));
				//Linear source
				A.coeffRef(ix_c, ix_c) += dx * dy * input.sl(x, y);
				//Other source
				b(ix_c) += dx * dy * input.s(x, y);
			}
		}

	}

	A.makeCompressed();

}
void discretizer::discretize(const Input3D& input, SparseMatrixType& A, VectorType& b)
{
	/*
	        TODO:
	        -crossflux
	        -Set expected number of equations/row
	        -Check result
	        Input:
	        Input3D struct
	        Reference to discretization matrix A
	        Reference to rhs vector A

	        This fills A and B such that by solving A*phi=b, we obtain an approximation to:

	        div(u(x,y,z) phi(x,y,z)-e(x,y,z) grad(phi(x,y,z))=s(x,y,z)-sl(x,y,z)*phi(x,y,z)
	        It is assumed that u and e are piecewise constant between nodes.
	*/
	/*
		        TODO:
		        -crossflux
		        -Set expected number of equations/row
		        -Check result
		        Input:
		        Input2D struct
		        Reference to discretization matrix A
		        Reference to rhs vector A

		        This fills A and B such that by solving A*phi=b, we obtain an approximation to:

		        div(u(x,y) phi(x,y)-e(x,y) grad(phi(x,y))=s(x,y)-sl(x,y)*phi(x,y)
		        It is assumed that u and e are piecewise constant between nodes.
	*/
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	double dz = (input.z_u - input.z_d) / (input.N_z - 1);

	//Prepare A and b
	//Reserve 5 non-zeros per row (5-point stencil)
	//TODO: Check if this size is ok
	A.resize(input.N_x * input.N_y * input.N_z, input.N_x * input.N_y * input.N_z);
	A.setZero();
	A.reserve(Eigen::VectorXi::Constant(input.N_x * input.N_y * input.N_z, 7));
	b.resize(input.N_x * input.N_y * input.N_z, 1);
	b.setZero();

	//Define variables needed to construct the discretization
	//Allocate arrays for the flux coefficients
	enum coefficient {alpha, beta, gamma, delta};
	std::array<double, 4> F_e;	//Flux through eastern edge
	std::array<double, 4> F_w;	//Flux through western edge
	std::array<double, 4> F_n;	//Flux through northern edge
	std::array<double, 4> F_s;	//Flux through southern edge
	std::array<double, 4> F_u;	//Flux through up edge
	std::array<double, 4> F_d;	//Flux through down edge
	int ix_c, ix_e, ix_w, ix_n, ix_s, ix_u, ix_d; //Indices of neighbouring cells
	double x, y, z; //Position of the nodal point

	for (int i = 0; i < input.N_x; i++)
	{
		for (int j = 0; j < input.N_y; j++)
		{
			for (int k = 0; k < input.N_z; k++)
			{
				//Indices of neighbouring cells
				ix_c = i + input.N_x * j + input.N_x * input.N_y * k;
				ix_e = (i + 1) + input.N_x * j + input.N_x * input.N_y * k;
				ix_w = (i - 1) + input.N_x * j + input.N_x * input.N_y * k;
				ix_n = i + input.N_x * (j + 1) + input.N_x * input.N_y * k;
				ix_s = i + input.N_x * (j - 1) + input.N_x * input.N_y * k;
				ix_u = i + input.N_x * j + input.N_x * input.N_y * (k + 1);
				ix_d = i + input.N_x * j + input.N_x * input.N_y * (k - 1);

				//Position of the nodal point
				x = input.x_w + i * dx;
				y = input.y_s + j * dy;
				z = input.z_d + k * dz;

				//Work out the Dirichlet boundary conditions
				if (i == 0)
				{
					//West boundary
					A.coeffRef(ix_c, ix_c) += 1;
					b(ix_c) = input.phi_w(y, z);
				}
				else if (i == input.N_x - 1)
				{
					//East boundary
					A.coeffRef(ix_c, ix_c) += 1;
					b(ix_c) = input.phi_e(y, z);
				}
				else if (j == 0)
				{
					//South boundary
					A.coeffRef(ix_c, ix_c) += 1;
					b(ix_c) = input.phi_s(x, z);
				}
				else if (j == input.N_y - 1)
				{
					//North boundary
					A.coeffRef(ix_c, ix_c) += 1;
					b(ix_c) = input.phi_n(x, z);
				}
				else if (k == 0)
				{
					//Down boundary
					A.coeffRef(ix_c, ix_c) += 1;
					b(ix_c) = input.phi_d(x, y);
				}
				else if (k == input.N_z - 1)
				{
					//Up boundary
					A.coeffRef(ix_c, ix_c) += 1;
					b(ix_c) = input.phi_u(x, y);
				}
				else
				{
					//Not at a boundary
					F_e = input.flux(input.u_x(x + dx / 2, y, z), input.e(x + dx / 2, y, z), dx);
					F_w = input.flux(input.u_x(x - dx / 2, y, z), input.e(x - dx / 2, y, z), dx);
					F_n = input.flux(input.u_y(x, y + dy / 2, z), input.e(x, y + dy / 2, z), dy);
					F_s = input.flux(input.u_y(x, y - dy / 2, z), input.e(x, y - dy / 2, z), dy);
					F_u = input.flux(input.u_z(x, y, z + dz / 2), input.e(x, y, z + dz / 2), dz);
					F_d = input.flux(input.u_z(x, y, z - dz / 2), input.e(x, y, z + dz / 2), dz);

					//North flux
					A.coeffRef(ix_c, ix_s) += 0;
					A.coeffRef(ix_c, ix_c) += dx * dz * F_n[alpha];
					A.coeffRef(ix_c, ix_n) += dx * dz * F_n[beta];
					b(ix_c) -= dy * (F_n[gamma] * input.s(x, y, z) + F_n[delta] * input.s(x, y + dy, z));
					//East flux
					A.coeffRef(ix_c, ix_w) += 0;
					A.coeffRef(ix_c, ix_c) += dy * dz * F_e[alpha];
					A.coeffRef(ix_c, ix_e) += dy * dz * F_e[beta];
					b(ix_c) -= dx * (F_e[gamma] * input.s(x, y, z) + F_e[delta] * input.s(x + dx, y, z));
					//South flux
					A.coeffRef(ix_c, ix_s) -= dx * dz * F_s[alpha];
					A.coeffRef(ix_c, ix_c) -= dx * dz * F_s[beta];
					A.coeffRef(ix_c, ix_n) -= 0;
					b(ix_c) += dy * (F_s[gamma] * input.s(x, y - dy, z) + F_s[delta] * input.s(x, y, z));
					//West flux
					A.coeffRef(ix_c, ix_w) -= dy * dz * F_w[alpha];
					A.coeffRef(ix_c, ix_c) -= dy * dz * F_w[beta];
					A.coeffRef(ix_c, ix_e) -= 0;
					b(ix_c) += dx * (F_w[gamma] * input.s(x - dx, y, z) + F_w[delta] * input.s(x, y, z));
					//Up flux
					A.coeffRef(ix_c, ix_u) += 0;
					A.coeffRef(ix_c, ix_c) += dx * dy * F_u[alpha];
					A.coeffRef(ix_c, ix_d) += dx * dy * F_u[beta];
					b(ix_c) -= dz * (F_u[gamma] * input.s(x, y, z) + F_u[delta] * input.s(x, y, z + dz));
					//Down flux
					A.coeffRef(ix_c, ix_u) -= dx * dy * F_d[alpha];
					A.coeffRef(ix_c, ix_c) -= dx * dy * F_d[beta];
					A.coeffRef(ix_c, ix_d) -= 0;
					b(ix_c) += dz * (F_d[gamma] * input.s(x, y, z - dz) + F_d[delta] * input.s(x, y, z));
					//Linear source
					A.coeffRef(ix_c, ix_c) += dx * dy * dz * input.sl(x, y, z);
					//Other source
					b(ix_c) += dx * dy * dz * input.s(x, y, z);
				}
			}
		}

	}

	A.makeCompressed();
}