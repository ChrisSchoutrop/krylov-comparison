# CMake generated Testfile for 
# Source directory: /home/chris/Documents/repos/eigen/unsupported
# Build directory: /home/chris/Documents/repos/eigen/build/unsupported
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Eigen")
subdirs("doc")
subdirs("test")
