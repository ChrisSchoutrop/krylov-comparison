'''
Plotscript for the acos_2019 results
00 result.algorithm << "\t" <<
01 result.true_residual << "\t" <<
02 result.estimated_residual << "\t" <<
03 result.iterations << "\t" <<
04 result.return_flag << "\t" <<
05 result.u << "\t" <<
06 result.beta << "\t" <<
07 result.e << "\t" <<
08 result.time << "\t" <<
09 result.N << "\t" <<
10 result.max_iterations << "\t" <<
11 result.tolerance << "\t" <<
12 result.const_source << "\t" <<
13 result.boundary_value << "\t" <<
14 result.length << "\t" <<
15 result.S << "\t" <<
16 result.L << "\t" <<
17 result.timestamp << "\t" <<
'''
#TODO: Clean this in some nice way

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import math
import scipy.interpolate
import plotter
from multiprocessing import Pool, cpu_count

plt.close()
font = {'family': 'normal',
        'weight': 'normal',
        'size': 15}
matplotlib.rc('font', **font)
'''
Available options:
	Algorithms (string):
		-amgcl
		-amgcl_eigen_bicgstab_solver
		-amgcl_bicgstabl_solver
		-amgcl_idrs_solver
		-BiCGSTAB
		-BiCGSTABL
		-IDRStab
	N (int):
		-5
		-11
		-21
		-41
		-81
		-161
	x,y (string)
		-u
		-e
		-beta
		-Advective Damkohler
		-Diffusive Damkohler
		-Harmonic Damkohler
		-Peclet
		-Grid Advective Damkohler
		-Grid Diffusive Damkohler
		-Grid Harmonic Damkohler
		-Grid Peclet
	z (string)
		-true_residual
		-estimated_residual
		-iterations
		-time
	scheme (string)
		-UW
		-CD
		-HF
		-CF
		-CFCF
'''
plot_jobs = []
job = {
	"algorithm": ["amgcl","amgcl_eigen_bicgstab_solver","amgcl_bicgstabl_solver","amgcl_idrs_solver","BiCGSTAB","BiCGSTABL","IDRStab"],
	"scheme": "CD",
	"x": "N",
	"y": "time",
	"u": 1,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	"x": "N",
	"y": "time",
	"u": 1,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "IDRStab",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	"x": "N",
	"y": "time",
	"u": 1,
	"e": 1,
	"L":2,
	"S":4,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "IDRStab",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	"x": "N",
	"y": "time",
	"u": 1,
	"e": 1,
	"L":1,
	"S":1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": ["amgcl","amgcl_eigen_bicgstab_solver","amgcl_bicgstabl_solver","amgcl_idrs_solver","BiCGSTAB","BiCGSTABL","IDRStab"],
	"scheme": "CD",
	"x": "N",
	"y": "iterations",
	"u": 1,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	"x": "N",
	"y": "iterations",
	"u": 1,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "IDRStab",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	"x": "N",
	"y": "iterations",
	"u": 1,
	"e": 1,
	"L":2,
	"S":4,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "IDRStab",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"u",
	"y": "iterations",
	#"u": 1,
	"N":161,
	"e": 1,
	"L":2,
	"S":4,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"u",
	"y": "iterations",
	#"u": 1,
	"N":161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "amgcl",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"u",
	"y": "iterations",
	#"u": 1,
	"N":161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "IDRStab",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"u",
	"y": "time",
	#"u": 1,
	"N":161,
	"e": 1,
	"L":2,
	"S":4,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"u",
	"y": "time",
	#"u": 1,
	"N":161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "amgcl",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"u",
	"y": "time",
	#"u": 1,
	"N":161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "IDRStab",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"beta",
	"y": "iterations",
	#"u": 1,
	"N":161,
	"e": 1,
	"L":2,
	"S":4,
	"u": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"beta",
	"y": "iterations",
	#"u": 1,
	"N":161,
	"e": 1,
	"u": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "amgcl",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"beta",
	"y": "iterations",
	#"u": 1,
	"N":161,
	"e": 1,
	"u": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "IDRStab",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"beta",
	"y": "time",
	#"u": 1,
	"N":161,
	"e": 1,
	"L":2,
	"S":4,
	"u": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"beta",
	"y": "time",
	#"u": 1,
	"N":161,
	"e": 1,
	"u": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "amgcl",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"beta",
	"y": "time",
	#"u": 1,
	"N":161,
	"e": 1,
	"u": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": ["amgcl","amgcl_eigen_bicgstab_solver","amgcl_bicgstabl_solver","amgcl_idrs_solver","BiCGSTAB","BiCGSTABL","IDRStab"],
	"scheme": "CD",
	"x": "N",
	"y": "true_residual",
	"u": 1,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	"x": "N",
	"y": "true_residual",
	"u": 1,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "IDRStab",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	"x": "N",
	"y": "true_residual",
	"u": 1,
	"e": 1,
	"L":2,
	"S":4,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "IDRStab",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"u",
	"y": "true_residual",
	#"u": 1,
	"N":161,
	"e": 1,
	"L":2,
	"S":4,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"u",
	"y": "true_residual",
	#"u": 1,
	"N":161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "amgcl",
	"scheme": ["UW","CD","HF","CF","CFCF"],
	#"x": "N",
	"x":"u",
	"y": "true_residual",
	#"u": 1,
	"N":161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
plot_jobs=[]
job = {
	"algorithm": ["amgcl","amgcl_eigen_bicgstab_solver","amgcl_bicgstabl_solver","amgcl_idrs_solver","BiCGSTAB","BiCGSTABL","IDRStab"],
	"scheme": "UW",
	"x": "u",
	"y": "time",
	"N": 161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": ["amgcl","amgcl_eigen_bicgstab_solver","amgcl_bicgstabl_solver","amgcl_idrs_solver","BiCGSTAB","BiCGSTABL","IDRStab"],
	"scheme": "CD",
	"x": "u",
	"y": "time",
	"N": 161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": ["amgcl","amgcl_eigen_bicgstab_solver","amgcl_bicgstabl_solver","amgcl_idrs_solver","BiCGSTAB","BiCGSTABL","IDRStab"],
	"scheme": "HF",
	"x": "u",
	"y": "time",
	"N": 161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": ["amgcl","amgcl_eigen_bicgstab_solver","amgcl_bicgstabl_solver","amgcl_idrs_solver","BiCGSTAB","BiCGSTABL","IDRStab"],
	"scheme": "CF",
	"x": "u",
	"y": "time",
	"N": 161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": ["amgcl","amgcl_eigen_bicgstab_solver","amgcl_bicgstabl_solver","amgcl_idrs_solver","BiCGSTAB","BiCGSTABL","IDRStab"],
	"scheme": "CFCF",
	"x": "u",
	"y": "time",
	"N": 161,
	"e": 1,
	"beta": 1,
}
plot_jobs.append(job)
# job = {
# 	"algorithm": ["amgcl","amgcl_eigen_bicgstab_solver","amgcl_bicgstabl_solver","amgcl_idrs_solver","BiCGSTAB","BiCGSTABL","IDRStab"],
# 	"scheme": "CFCF",
# 	"x": "u",
# 	"y": "time",
# 	"N": 161,
# 	"e": 1,
# 	"beta": 1,
# }
# plot_jobs.append(job)

plot_jobs=[]
job = {
	"algorithm": "BiCGSTAB",
	"scheme": "UW",
	"x": "u",
	"y": "beta",
	"z":"true_residual",
	"N": 161,
	"e": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "amgcl",
	"scheme": "UW",
	"x": "u",
	"y": "beta",
	"z":"true_residual",
	"N": 161,
	"e": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": "UW",
	"x": "u",
	"y": "beta",
	"z":"estimated_residual",
	"N": 161,
	"e": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "amgcl",
	"scheme": "UW",
	"x": "u",
	"y": "beta",
	"z":"estimated_residual",
	"N": 161,
	"e": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "BiCGSTAB",
	"scheme": "UW",
	"x": "u",
	"y": "beta",
	"z":"time",
	"N": 161,
	"e": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "amgcl",
	"scheme": "UW",
	"x": "u",
	"y": "beta",
	"z":"time",
	"N": 161,
	"e": 1,
}
plot_jobs.append(job)

#Load in data
data = np.loadtxt('results_2D_vakantie.txt', delimiter='\t', dtype=object)
data[:, 1:-2] = data[:, 1:-2].astype(float)
print(data)

#Execute plot jobs
for job in plot_jobs:
	print(job)
	filename=""
	#Convert to a useful format
	if "N" in job:
		N = job["N"]
	else:
		N = None
	if isinstance(job["algorithm"],list):
		filename = filename+'_'+str('-'.join(job["algorithm"]))
	else:
		filename = filename+job["algorithm"]
	if isinstance(job["scheme"],list):
		filename = filename+'_'+str('-'.join(job["scheme"]))
	else:
		filename=filename+'_'+str(job["scheme"])
	filename = filename+'_'+str(N)
	filename = filename+'_x'+str(job["x"])
	filename = filename+'_y'+str(job["y"])
	if "z" in job:
		filename = filename+'_z'+str(job["z"])
		z_type = job["z"]
	else:
		z_type = None
	if "u" in job:
		u = job["u"]
	else:
		u = None
	if "e" in job:
		e = job["e"]
	else:
		u = None
	if "beta" in job:
		beta = job["beta"]
	else:
		beta = None
	if job["algorithm"] == "BiCGSTAB":
		L = 1
		S = 1
	elif job["algorithm"] == "BiCGSTABL":
		L = job["L"]
		S = 1
	elif job["algorithm"] == "IDRStab":
		L = job["L"]
		S = job["S"]
	else:
		L = -1
		S = -1
	filename = filename+'_S'+str(S)
	filename = filename+'_L'+str(L)
	plotter.plot(filename, data, z_type, job["scheme"],
	             job["algorithm"], job["x"], job["y"], L, S, N, u, e, beta)
