import numpy as np

output_file=open("x_test_random.mtx",'w')
q=5

#print header
print("%%MatrixMarket matrix array real general",file=output_file)
print(str(q**6)+' '+str(1),file=output_file)

#q**6 guarantees that the result can be seen as a 3D cube or 2D square
for i in range(0,q**6):
	print(np.random.rand(),file=output_file)

