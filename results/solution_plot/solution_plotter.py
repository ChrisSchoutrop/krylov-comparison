import scipy.interpolate
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.io
import math
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
font = {'family': 'normal',
        'weight': 'normal',
        'size': 15}
matplotlib.rc('font', **font)

def read_x():
	'''
	Read in solution from file
	Nice to have: Check the comment field for plot info
	'''
	#Read in vector using
	#https://docs.scipy.org/doc/scipy-1.5.4/reference/generated/scipy.io.mmread.html
	#scipy.io.mmread()
	pass


def plot_2d_sol(x_input,input_filename):
	'''
	Plot 2D solution
	Makes a color plot of z(x,y)

	Assumptions:
	-A square grid is used
	-The length of the grid in x=length in y.
	-The x and y points are at the same positions (square cells)
	-The grid runs from 0 to 1
	-Lexicographic ordering
	'''
	#Add vector of x,y to the vector z
	#Plot using the same code from plotter.py

	#Number of unknowns
	N=len(x_input)

	#Assuming a square grid
	#Number of gridpoints in each direction
	m=round(math.sqrt(N))
	print(m)

	#Assume the grid runs from 0 to 1
	xmin=0
	xmax=1
	ymin=0
	ymax=1

	#Set up the plot grid
	xi = np.linspace(xmin, xmax, m)
	yi = np.linspace(ymin, ymax, m)

	z=np.array(x_input).flatten()
	x=np.empty(N)
	y=np.empty(N)
	for i in range(0,m):
		for j in range(0,m):
			x[i+m*j]=xi[i]
			y[i+m*j]=yi[j]
			print("x[i+m*j]: "+str(x[i+m*j])+" y[i+m*j]: "+str(y[i+m*j]))

	#"Interpolate" the x,y data to the grid
	#This is needed to convert x,y,z data into something that can be plotted
	xi, yi = np.meshgrid(xi, yi)
	zi = scipy.interpolate.griddata(
		(x, y), z, (xi, yi), method='nearest', rescale=True)

	#Set up the plot
	f = plt.figure()
	ax = plt.gca()
	plt.pcolormesh(xi, yi, zi, cmap='jet', edgecolors='none')
	ax.tick_params(axis='both', direction='in', bottom=True,
		top=True, left=True, right=True)
        #ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
        #ax.ticklabel_format(axis='y', style='plain')
	#plt.xscale('log')
	#plt.yscale('log')
	#plt.legend(labels, loc='best')
	ax.autoscale(enable=True, axis='both', tight=True)
	ax.spines['top'].set_linewidth(1.5)
	ax.spines['bottom'].set_linewidth(1.5)
	ax.spines['left'].set_linewidth(1.5)
	ax.spines['right'].set_linewidth(1.5)
	ax.tick_params(axis='both',width=1.5,length=5)
	cbar = plt.colorbar(extend='both')
	cbar.ax.set_ylabel('$\\varphi$ [ ]', usetex=False)
	cbar
	plt.xlabel('x [ ]', usetex=False)
	plt.ylabel('y [ ]', usetex=False)
	f.savefig(input_filename+".pdf", bbox_inches='tight')
	f.savefig(input_filename+".png", bbox_inches='tight')
	plt.close()


def plot_3d_sol(x_input,input_filename):
	'''
	Plot 3D solution
	Makes a color plot of a slice of the function w(x,y,z) at some z-value

	Assumptions:
	-A cubic grid is used
	-The length of the grid in x=length in y=length in z.
	-The x,y and z points are at the same positions (cubic cells)
	-The grid runs from 0 to 1
	-Lexicographic ordering
	'''
	#Reduce solution to only one z-plane in 3D
	#Add vector x,y to w
	#Plot using the same code from plotter.py

	#Number of unknowns
	N=len(x_input)

	#Assuming a square grid
	#Number of gridpoints in each direction
	m=round(N**(1.0/3.0))
	print(m)

	#Assume the grid runs from 0 to 1
	xmin=0
	xmax=1
	ymin=0
	ymax=1

	zmin=0
	zmax=1
	zplot=0.5

	#Find the number of the gridpoint that matches as close as possible the coordinate of zplot
	#This is done by linearly interpolating number(z)
	z_plot_ix=round(m*(zplot-zmin)/(zmax-zmin))

	#Set up the plot grid
	xi = np.linspace(xmin, xmax, m)
	yi = np.linspace(ymin, ymax, m)

	z=np.empty(m*m)
	x=np.empty(m*m)
	y=np.empty(m*m)
	#Loop over the 2D plane
	for i in range(0,m):
		for j in range(0,m):
			x[i+m*j]=xi[i]
			y[i+m*j]=yi[j]
			z[i+m*j]=x_input[i+m*j+z_plot_ix*m*m]
			print("x[i+m*j]: "+str(x[i+m*j])+" y[i+m*j]: "+str(y[i+m*j])+" z[i+m*j]: "+str(z[i+m*j]))

	#"Interpolate" the x,y data to the grid
	#This is needed to convert x,y,z data into something that can be plotted
	xi, yi = np.meshgrid(xi, yi)
	zi = scipy.interpolate.griddata(
		(x, y), z, (xi, yi), method='nearest', rescale=True)

	#Set up the plot
	f = plt.figure()
	ax = plt.gca()
	plt.pcolormesh(xi, yi, zi, cmap='jet', edgecolors='none')
	ax.tick_params(axis='both', direction='in', bottom=True,
		top=True, left=True, right=True)
        #ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
        #ax.ticklabel_format(axis='y', style='plain')
	#plt.xscale('log')
	#plt.yscale('log')
	#plt.legend(labels, loc='best')
	ax.autoscale(enable=True, axis='both', tight=True)
	ax.spines['top'].set_linewidth(1.5)
	ax.spines['bottom'].set_linewidth(1.5)
	ax.spines['left'].set_linewidth(1.5)
	ax.spines['right'].set_linewidth(1.5)
	ax.tick_params(axis='both',width=1.5,length=5)
	#ax.xaxis.set_minor_locator(AutoMinorLocator())
	cbar = plt.colorbar(extend='both')
	cbar.ax.set_ylabel('$\\varphi$ [ ]', usetex=False)
	cbar
	plt.xlabel('x [ ]', usetex=False)
	plt.ylabel('y [ ]', usetex=False)
	f.savefig(input_filename+".pdf", bbox_inches='tight')
	f.savefig(input_filename+".png", bbox_inches='tight')
	plt.close()

input_filename='x_test_random.mtx'
x=scipy.io.mmread(input_filename)
#plot_2d_sol(x,input_filename)
plot_3d_sol(x,input_filename)