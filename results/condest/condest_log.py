import matplotlib.pyplot as plt
import copy

with open('condest_log_superLU.log') as f:
    lines = f.read().splitlines()

dense_list    =[]
sparse_1d_list=[]
sparse_2d_list=[]
sparse_3d_list=[]

append_dense=False
append_sparse_1d=True
append_sparse_2d=False
append_sparse_3d=False

for line in lines:
	#Figure out which part of the file based on the header
	if line=='Sparse 1D':
		append_sparse_1d=True
	if line=='Dense':
		append_dense=True
		append_sparse_1d=False
	if line=='Sparse 2D':
		append_dense=False
		append_sparse_2d=True
	if line=='Sparse 3D':
		append_sparse_2d=False
		append_sparse_3d=True

	#Append to the correct list (split in 3)
	if append_dense:
		dense_list.append(line)
	if append_sparse_2d:
		sparse_2d_list.append(line)
	if append_sparse_1d:
		sparse_1d_list.append(line)
	if append_sparse_3d:
		sparse_3d_list.append(line)

#Remove the header for each section
dense_list.pop(0)
sparse_1d_list.pop(0)
sparse_2d_list.pop(0)
sparse_3d_list.pop(0)

#print(dense_list)
#dense_dict={}
def list_to_dict(input_list):
	'''
	N:[list of N's]
	algo:{time:[],cond:[]}
	'''
	output_dict={}
	output_dict["N"]=[]
	for index,item in enumerate(input_list):
		if "N: " in item:
			#"N: integer"
			#obtain that integer from this string
			output_dict["N"].append(int(item.split("N: ")[1]))
		if "\t Time: " in item:
			#"Algorithm\t Time: integer ms"
			splitted=item.split("\t Time: ")
			algorithm=splitted[0]
			time=float(splitted[1].split(' ms')[0])
			cond=float(input_list[index-1])
			if algorithm in output_dict:
				output_dict[algorithm]["time"].append(time)
				output_dict[algorithm]["cond"].append(cond)
			else:
				output_dict[algorithm]={"time":[],"cond":[]}
	return output_dict

dense_dict    =copy.deepcopy(list_to_dict(dense_list))
sparse_1d_dict=copy.deepcopy(list_to_dict(sparse_1d_list))
sparse_2d_dict=copy.deepcopy(list_to_dict(sparse_2d_list))
sparse_3d_dict=copy.deepcopy(list_to_dict(sparse_3d_list))

#print(lines)
x=[1,2,3,4]
y=[10,100,1000,10000]



def make_plots(input_dict,filename):
	#Time plot
	f = plt.figure()
	ax = plt.gca()
	for key in input_dict.keys():
		if key=="N":
			continue
		x=input_dict["N"]
		y=input_dict[key]["time"]
		while len(x)>len(y):
			y.append(float("NaN"))
		len(x)
		len(y)
		plt.plot(x,y,'o-',label=key)
		plt.xlabel('N')
		plt.ylabel('Time (ms)')
		plt.xscale('log')
		plt.yscale('log')
		plt.legend()
	plt.autoscale()
	ax.tick_params(which='both',axis='both', direction='in', bottom=True,top=True, left=True, right=True)
	#ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
	#ax.ticklabel_format(axis='y', style='plain')
	f.savefig(filename+"_time.pdf", bbox_inches='tight')
	f.savefig(filename+"_time.png", bbox_inches='tight')

	#Condition number plot
	f = plt.figure()
	ax = plt.gca()
	for key in input_dict.keys():
		if key=="N":
			continue
		x=input_dict["N"]
		y=input_dict[key]["cond"]
		while len(x)>len(y):
			y.append(float("NaN"))
		len(x)
		len(y)
		plt.plot(x,y,'o-',label=key)
		plt.xlabel('N')
		plt.ylabel('K')
		plt.xscale('log')
		plt.yscale('log')
		plt.legend()
	plt.autoscale()
	ax.tick_params(which='both',axis='both', direction='in', bottom=True,top=True, left=True, right=True)
	#ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0),useMathText=True)
	#ax.ticklabel_format(axis='y', style='plain')
	f.savefig(filename+"_K.pdf", bbox_inches='tight')
	f.savefig(filename+"_K.png", bbox_inches='tight')

make_plots(dense_dict    ,"dense")
make_plots(sparse_2d_dict,"sparse_2d")
make_plots(sparse_3d_dict,"sparse_3d")
make_plots(sparse_1d_dict,"sparse_1d")