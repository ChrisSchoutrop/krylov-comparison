import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import math
import scipy.interpolate
import copy

plot_index = {
	"algorithm": 0,
	"true_residual": 1,
	"estimated_residual": 2,
	"iterations": 3,
	"return_flag": 4,
	"u": 5,
	"beta": 6,
	"e": 7,
	"time": 8,
	"N": 9,
	"max_iterations": 10,
	"tolerance": 11,
	"S": 12,
	"L": 13,
	"timestamp": 14,
	"scheme": 15,
}


def filter_data(data, scheme=None, algorithm=None, L=None, S=None, N=None, u=None, e=None, beta=None):
	filtered_data = data
	if scheme is not None:
		filtered_data = copy.copy(
			filtered_data[data[:, plot_index["scheme"]] == scheme, :])
	if algorithm is not None:
		filtered_data = copy.copy(
			filtered_data[filtered_data[:, plot_index["algorithm"]] == algorithm, :])
	if L is not None:
		filtered_data = copy.copy(
			filtered_data[filtered_data[:, plot_index["L"]] == L, :])
	if S is not None:
		filtered_data = copy.copy(
			filtered_data[filtered_data[:, plot_index["S"]] == S, :])
	if N is not None:
		filtered_data = copy.copy(
			filtered_data[filtered_data[:, plot_index["N"]] == N, :])
	if u is not None:
		filtered_data = copy.copy(
			filtered_data[filtered_data[:, plot_index["u"]] == u, :])
	if e is not None:
		filtered_data = copy.copy(
			filtered_data[filtered_data[:, plot_index["e"]] == e, :])
	if beta is not None:
		filtered_data = copy.copy(
			filtered_data[filtered_data[:, plot_index["beta"]] == beta, :])

	return filtered_data


def plot_quantity(data, x_type, y_type, z_type=None):
	'''
	Assumptions:
		Length of the grid is 1
		All rows in data are for the same N
		Grid spacing is equal in all directions
		u is the same in all directions
		u,e,beta independent of the spatial coordinates
	Input:
		data: Table containing the quantities as lined out in plot_index
		Plot types: quantities on the x,y,z axis
	Output:
		arrays of x,y,z data
		if z_type is None, the z array will be None
	'''
	def parse_type(input_type):
		'''
		Magic string that prevents autoformat from breaking this function
		'''
		if input_type is None:
			return None

		#Quantities that are directly stored in the table
		if input_type in plot_index:
			return data[:, plot_index[input_type]]

		#Quantities that have to be computed from the table
		if "Grid" in input_type:
			N = data[1, plot_index["N"]]
			L = 1/(N-1)
		else:
			L = 1
		if "Damkohler" in input_type:
			if "Advective" in input_type:
				# beta*L/u
				u = np.array(data[:, plot_index["u"]])
				beta = np.array(data[:, plot_index["beta"]])
				return beta*L/u
			elif "Diffusive" in input_type:
				# beta*L^2/e
				beta = np.array(data[:, plot_index["beta"]])
				e = np.array(data[:, plot_index["e"]])
				return beta*L*L/e
			elif "Harmonic" in input_type:
				# Harmonic mean of Advective and Diffusive;
				# L^2*beta/(u*L+e)
				u = np.array(data[:, plot_index["u"]])
				beta = np.array(data[:, plot_index["beta"]])
				e = np.array(data[:, plot_index["e"]])
				return L*L*beta/(u*L+e)

		elif "Peclet" in input_type:
			#Pe=L*u/e
			u = np.array(data[:, plot_index["u"]])
			e = np.array(data[:, plot_index["e"]])
			return L*u/e

		#Somehow we got to this point
		raise Exception("Parsing of plot variable"+str(input_type)+" failed")

	#Parse the input
	x = parse_type(x_type)
	y = parse_type(y_type)
	z = parse_type(z_type)

	#Convert to arrays of x,y,[z] values
	x = np.array(x, dtype=float)
	y = np.array(y, dtype=float)
	if z_type is None:
		z = None
	else:
		z = data[:, plot_index[z_type]]
		z = np.array(z, dtype=float)
	return x, y, z


def plot_1d_results(filename, x, y, x_type, y_type,legend_type=None):
	'''
	Examples of what this should be able to provide:
	time as function of N for all schemes with a specific solver
	time as function of N for all solvers for a certain scheme
	number of iterations as function of condition number for each solver
	condition number as function of N (for several beta for example)
	-> make the inputs a list, make the output a list of np arrays
	'''
	if len(x) == 0 or len(y) == 0:
		raise Exception("No data for this plot")

	#Set up the plot
	f = plt.figure()
	ax = plt.gca()
	if legend_type is None:
		plt.plot(x,y)
		plt.xlabel(x_type, usetex=True)
		plt.ylabel(y_type, usetex=True)
	else:
		for idx,legend_item in enumerate(legend_type):
			plt.plot(x[idx],y[idx],label=legend_item)
			plt.xlabel(x_type.replace('_',' '), usetex=True)
			plt.ylabel(y_type.replace('_',' '), usetex=True)
	plt.xscale('log')
	plt.yscale('log')
	#ax.autoscale(enable=True, axis='x', tight=True)
	plt.autoscale(enable=True, axis='x', tight=True)
	ax.legend(fontsize='small',frameon=False)
	f.savefig(filename+".pdf", bbox_inches='tight')
	f.savefig(filename+".png", bbox_inches='tight')
	plt.close()
	pass


def plot_2d_results(filename, x, y, z, x_type, y_type, z_type):
	if len(x) == 0 or len(y) == 0 or len(z) == 0:
		raise Exception("No data for this plot")

	#Set up the plot
	f = plt.figure()
	ax = plt.gca()

	#Number of unique x-quantities
	Nx_interpolate = len(set(x))
	Ny_interpolate = len(set(y))

	#Work out the plot ranges
	xmin = min(x)
	xmax = max(y)
	ymin = min(y[y[:] > 0])
	ymax = max(y)
	zmin = min(z)
	zmax = max(z)
	print('xmin: '+str(xmin)+' xmax: '+str(xmax))
	print('ymin: '+str(ymin)+' ymax: '+str(ymax))
	print('zmin: '+str(zmin)+' zmax: '+str(zmax))

	#Set up the plot grid
	xi = np.logspace(np.log10(xmin), np.log10(xmax), Nx_interpolate)
	yi = np.logspace(np.log10(ymin), np.log10(ymax), Ny_interpolate)
	xi, yi = np.meshgrid(xi, yi)

	#"Interpolate" the x,y data to the grid
	zi = scipy.interpolate.griddata(
		(x, y), z, (xi, yi), method='linear', rescale=True)
	if z_type == "iterations":
		pcm = plt.pcolormesh(xi, yi, zi, cmap='jet', norm=matplotlib.colors.LogNorm(
			vmin=1, vmax=1e4), edgecolors='none')
		cbar = plt.colorbar(extend='both')
		cbar.ax.set_ylabel('Iterations', usetex=False)
	elif z_type == "true_residual" or z_type == "estimated_residual":
		pcm = plt.pcolormesh(xi, yi, zi, cmap='jet', norm=matplotlib.colors.LogNorm(
			vmin=1e-12, vmax=1), edgecolors='none')
		cbar = plt.colorbar(extend='both')
		cbar.ax.set_ylabel('Residual', usetex=False)
	elif z_type == "time":
		pcm = plt.pcolormesh(xi, yi, zi, cmap='jet', norm=matplotlib.colors.LogNorm(
			vmin=zmin, vmax=zmax), edgecolors='none')
		cbar = plt.colorbar(extend='both')
		cbar.ax.set_ylabel('Time [s]', usetex=False)
	else:
		pcm = plt.pcolormesh(xi, yi, zi, cmap='jet', norm=matplotlib.colors.LogNorm(
			vmin=1e-12, vmax=1), edgecolors='none')
		cbar = plt.colorbar(extend='both')
		cbar.ax.set_ylabel('???????????????', usetex=False)
	plt.xlabel(x_type, usetex=True)
	plt.ylabel(y_type, usetex=True)
	plt.xscale('log')
	plt.yscale('log')
	plt.xlim(min(x), max(x))
	plt.ylim(min(y[y > 0]), max(y))
	f.savefig(filename+".pdf", bbox_inches='tight')
	f.savefig(filename+".png", bbox_inches='tight')
	plt.close()


def plot(filename, data, z_type, scheme, algorithm, x_type, y_type, L=None, S=None, N=None, u=None, e=None, beta=None):
	if z_type is None:
		#Filter the input data for this specific scheme,algorithm,L,S,N
		if isinstance(scheme, list):
			x=[]
			y=[]
			for idx in range(len(scheme)):
				filtered_data=filter_data(data, scheme[idx], algorithm, L, S, N, u, e, beta)
				x_temp,y_temp,_=plot_quantity(filtered_data, x_type, y_type, z_type)
				x.append(x_temp)
				y.append(y_temp)
			plot_1d_results(filename, x, y, x_type, y_type,legend_type=scheme)
			return
		if isinstance(algorithm, list):
			x=[]
			y=[]
			for idx in range(len(algorithm)):
				if algorithm[idx]=="IDRStab":
					L=2
					S=4
				elif algorithm[idx]=="BiCGSTAB":
					L=1
					S=1
				elif algorithm[idx]=="BiCGSTABL":
					L=2
					S=1
				else:
					L=-1
					S=-1
				filtered_data=filter_data(data, scheme, algorithm[idx], L, S, N, u, e, beta)
				x_temp,y_temp,_=plot_quantity(filtered_data, x_type, y_type, z_type)
				x.append(x_temp)
				y.append(y_temp)
			plot_1d_results(filename, x, y, x_type, y_type,legend_type=algorithm)
			return
		x, y, _ = plot_quantity(filtered_data, x_type, y_type, z_type)
		plot_1d_results(filename, x, y, x_type, y_type)
	else:
		#Filter the input data for this specific scheme,algorithm,L,S,N
		filtered_data = filter_data(data, scheme, algorithm, L, S, N, u, e, beta)
		x, y, z = plot_quantity(filtered_data, x_type, y_type, z_type)
		plot_2d_results(filename, x, y, z, x_type, y_type, z_type)


def plot_1d(filename, N, phi, x_w, x_e):
	'''
	Plot the solution vector phi as a 1D function
	'''
	xi = np.linspace(x_w, x_e, N)
	f = plt.figure()
	ax = plt.gca()
	plt.plot(xi, phi)
	plt.xlabel('x')
	plt.ylabel('phi')

	pass


def plot_2d(filename, N, phi, x_w, x_e, y_s, y_n):
	x = np.linspace(x_w, x_e, N)
	y = np.linspace(y_s, y_n, N)

	#Convert data into x,y,z
	xi = []
	yi = []
	z = []
	for i in range(0, N):
		for j in range(0, N):
			#Index of the cell
			ix_c = i+N*j
			#Position of the nodal point
			xi.append(x[i])
			yi.append(y[j])
			z.append(phi[ix_c])

	xii, yii = np.meshgrid(x, y)
	zi = scipy.interpolate.griddata(
		(xi, yi), z, (xii, yii), method='nearest', rescale=True)
	#pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=2e-3, vmax=5),edgecolors='none')
	#pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=6e-3, vmax=9),edgecolors='none')
	pcm = plt.pcolormesh(xi, yi, zi, cmap='jet', norm=matplotlib.colors.LogNorm(
		vmin=1e-12, vmax=1), edgecolors='none')
	cbar = plt.colorbar(extend='both')
	cbar.ax.set_ylabel(
		'$Wall time\\times\\|\\bf{b}-\\bf{A}\\bf{x}\\|/\\|\\bf{b}\\|$', usetex=True)
	plt.xlabel('$u/\\mathcal{E}$', usetex=True)
	plt.ylabel('$\\beta/\\mathcal{E}$', usetex=True)
	plt.xscale('log')
	plt.yscale('log')
	plt.xlim(min(x), max(x))
	plt.ylim(min(y[y > 0]), max(y))
	f.savefig(filename+".pdf", bbox_inches='tight')
	f.savefig(filename+".png", bbox_inches='tight')
	pass


def plot_3d(filename, N, phi, x_w, x_e, y_s, y_n, z_d, z_u):
	'''
	TODO: How?
	plot a slice?
	plot 4D :^)
	'''
	pass
