clc
clear all
close all


N=3;
A=gallery('tridiag',N,-1,2,-1);
A=full(A);
[L,U]=lu(A);

disp('A L U')
full(A)
full(L)
full(U)
disp('Ainv Linv Uinv')
inv(A)
inv(L)
inv(U)

LL=eye(N);
UU=zeros(size(U));
for i=1:N
    for j=1:N
        if i-1==j
            LL(i,j)=-j/i;
        end
        if i==j
            UU(i,j)=(i+1)/j;
        end
        if i==j-1
            UU(i,j)=-1;
        end
    end
end

norm(LL-L)
norm(UU-U)

invL=zeros(size(L));
invU=zeros(size(U));
for i=1:N
    for j=1:N
        if i>=j
            invL(i,j)=j/i;
        end
        if i<=j
            invU(i,j)=i/(j+1);
        end
    end
end

norm(invL-inv(L))
norm(invU-inv(U))

%{
!! idea:
applying a discrete scheme to a vector is effectively a convolution where the kernel
depends on that scheme.
"A*x=b" can be seen as, convolve x with some kernel, the output is b."
To solve this system we're really just computing Ainv in some fashion, this
may be seen as a deconvolution. I.e. solving which input x gave the output
b. 
Can this be done efficiently using fourier transforms, pointwise division,
and inverse transform????


Gaussische eliminatie is eigenlijk:
stel je hebt vlakken loodrecht op elke kolom van A
het veegproces maakt die vlakken loodrecht op elkaar

LU is min of meer bijhouden welke stappen je maakt.

Zou misschien een soort omgekeerd veegproces kunnen doen, waarbij je begint
met vlakken die loodrecht op elkaar staan (identity matrix), dan gaat vegen 
totdat het de matrix wordt die je wilt hebben.
Min of meer lost het de puzzel op:
Uinv*Linv*A=eye

Als we Linv en Uinv zien als lineaire transformaties op de kolommen van A,
hoe ziet dat dan uit? Wat is het effect van Lin en wat is het effect van
Uinv?

Hoe zit het met andere decomposities?
SVD
Spectraal
QR
Schur
RQ
https://en.wikipedia.org/wiki/Matrix_decomposition
en nog veel meer lol

QR is eigenlijk Gram-Schmidt, het is misschien ook mogelijk om te beginnen
met een matrix Q en projecties uit te voeren op elk van die vectoren met
rijen van de matrix die je wilt hebben.
Daar R uit verkrijgen. -> Is probleem, stel Q=eye, dan zeg je A=R. Dit
werkt niet voor non-triangular A.

soort "Reverse LU" of "Reverse QR" eigenlijk?

Als men weet dat A symmetrisch moet zijn kun je ook Cholesky gebruiken, dan
hoef je alleen L te bepalen. 

Of een soort "Factorisatie van gediscretiseerde differentiaal operator"
ofzo zoeken we eigenlijk.
Is de originele discrete differentiaal operator te schrijven als een
product in de vorm van:
(phi onder diagonaal)*(phi boven diagonaal)?

In 1D met constante coefficienten is het msischien te doen, dan heb je
tridiagonal toeplitz. Zou een TDMA-achtig iets kunnen gebruiken waarbij je
de coefficienten in TDMA kiest. 

Is er een of andere manier om een sparse decompositie te krijgen van A?
%}

% [U,S,V]=svd(full(A));
% disp('A U S V')
% full(A)
% full(U)
% full(S)
% full(V)
% 
% [Q,R]=qr(full(A));
% disp('A Q R')
% full(A)
% full(Q)
% full(R)


clear all
close all

N=3;
A=gallery('tridiag',N,-1,2,-1);
A=full(A);
b=[1;2;3];
x=A\b

D=[-1;2;-1];
WD=fft(D)
Wb=fft(b)
Wphi=Wb./WD
phi=ifft(Wphi)
Wphi(1)=9;
phi=ifft(Wphi)

%{
Originele idee was:
Differentiaal operator is eigenlijk te zien als een discrete convolutie,
kunnen we aan hand van het "convolution theorem" dit inverteren?
Antwoord is nee,
want d^2/dx^2(constante) is altijd 0 voor elke constante
dus dit is geen inverteerbare operator. Dit is ook te zien aan dat de fft
van D een DC component heeft die 0 is. 

Het probleem -d^2/dx^2 phi=S is wel uniek oplosbaar doordat je de
randvoorwaarden meegeeft, maar deze zitten NIET in D, maar in b.

Problem is dat de kernel D geen DC component heeft.
Merk op dat de matrix A ook niet overal diezelfde kernel gebruikt, de
randen zijn anders.
-> tuurlijk niet!
want d^2/dx^2(constante) is altijd 0 voor elke constante, ongeacht wat je
doet met A omschrijven blijft dit probleem bestaan.

Vermoeden is dat somehow de rvw in D gestopt moeten worden wil dit werken. 
%}

clear all
close all

%{
Test voor -d^2/dx^2phi=S
%}
phi_L=3;
phi_R=3;

%Add trivial equation to capture the boundary conditions
N=5;
L=1;
dx=L/(N-1);
S=1;
A=gallery('tridiag',N,-1,2,-1)/(dx*dx);
A=full(A);
A(1,:)=0;
A(1,1)=1;
A(end,:)=0;
A(end,end)=1;
b=ones(N,1)*S;
b(1)=+phi_L;
b(end)=phi_R;
phi_matrix=A\b
x=linspace(0,L,N)';

c1=(phi_R-phi_L+S*L.^2/2)/L;
phi_exact=(-S.*x.^2/2+c1.*x+phi_L)

%Nudge in the boundary conditions by modifying b
N=N-2;
A=gallery('tridiag',N,-1,2,-1)/(dx*dx);
A=full(A);
b=ones(N,1)*S;
b(1)=b(1)+phi_L/(dx*dx);
b(N)=b(N)+phi_R/(dx*dx);
phi_matrix2=zeros(N+2,1);
phi_matrix2(1)=phi_L;
phi_matrix2(end)=phi_R;
phi_matrix2(2:end-1)=A\b;
phi_matrix2

%Nudge in the boundary conditions by solving a rectangular system
A=gallery('tridiag',N,-1,2,-1)/(dx*dx);
A=full(A);
A=[zeros(N,1) A zeros(N,1)];
A(1,1)=-1/(dx*dx);
A(end,end)=-1/(dx*dx);
b=ones(N,1)*S;
b(1)=b(1)+phi_L/(dx*dx);
b(N)=b(N)+phi_R/(dx*dx);
phi_matrix3=zeros(N+2,1);
phi_matrix3=A\b;
phi_matrix3(1)=phi_L;
phi_matrix3(end)=phi_R;
phi_matrix3


clear all
close all

%{
Test voor -d^2/dx^2phi=S
%}
phi_L=0;
phi_R=0;

%Add trivial equation to capture the boundary conditions
N=11;
L=1;
dx=L/(N-1);
S=1;
A=gallery('tridiag',N,1,2,-1)/(dx*dx);
A=full(A);
A(1,:)=0;
A(1,1)=1;
A(end,:)=0;
A(end,end)=1;
b=ones(N,1)*S;
b(1)=+phi_L;
b(end)=phi_R;
phi_matrix=A\b
x=linspace(0,L,N)';


D=[1;2;-1]/(dx*dx);
D=[zeros((N-3)/2,1);D;zeros((N-3)/2,1)];
b=ones(N,1)*S;
b(1)=b(1)+phi_L/(dx*dx);
b(N)=b(N)+phi_R/(dx*dx);
WD=fft(D)
Wb=fft(b)
Wphi=Wb./WD;
phi=ifft(Wphi)

%Wphi(1)=(phi_L+phi_R)/2;
%phi=ifft(Wphi)





