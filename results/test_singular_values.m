clear all
close all
clc
rng(6)
N=1000;
tol=1e-12;
maxit=1000;

A=rand(N,N);
A=A-A';
[U,S,V]=svd(A);

S_many=diag(S);
S_many(1:N-1)=1e15;
S_many(end)=1;
S_many=diag(S_many);
A_many=U*S_many*V';

S_one=diag(S);
S_one(1)=1e15;
S_one(2:end)=1;
S_one=diag(S_one);
A_one=U*S_one*V';

b_random=rand(N,1);
%Control
disp('Control')
disp('Unmodified A, largest singular vector')
gmres(A,V(:,1),[],tol,maxit);
disp('Unmodified A, smallest singular vector')
gmres(A,V(:,end),[],tol,maxit);
disp('Unmodified A, random b')
gmres(A,b_random,[],tol,maxit);
disp(' ')

%Matrix with many large singular values, one small
disp('Many large, one small')
disp('Largest singular vector')
gmres(A_many,V(:,1),[],tol,maxit);
disp('Smallest singular vector')
gmres(A_many,V(:,end),[],tol,maxit);
disp('random b')
gmres(A_many,b_random,[],tol,maxit);
disp(' ')

%Matrix with one large singular values, many small
disp('One large, many small')
disp('Largest singular vector')
gmres(A_one,V(:,1),[],tol,maxit);
disp('Smallest singular vector')
gmres(A_one,V(:,end),[],tol,maxit);
disp('random b')
gmres(A_one,b_random,[],tol,maxit);
disp(' ')

%{
Observations:
Based on
gmres(A,V(:,1),[],tol,maxit);
gmres(A,V(:,end),[],tol,maxit);
it can be concluded that converging to the smallest singular vector is more
difficult than converging to the largest singular vector.
gmres(A_many,V(:,1),[],tol,maxit);
gmres(A_many,V(:,end),[],tol,maxit);
Also shows this.

gmres(A,rand(N,1),[],tol,maxit);
Converges in N iterations.

One large, many small is more difficult than many large, one small.
%}