def error(N,scheme):
	if scheme=="UW":
		c=1
	elif scheme=="CD":
		c=1/2
	elif scheme=="HF":
		c=1/3
	elif scheme=="CF":
		c=1/4
	elif scheme=="CFCF":
		c=1/5
	if scheme=="UW":
		return c/N**1
	else:
		return c/N**2

N_list=[5,11,21,41,81,161,321]
schemes=["UW","CD","HF","CF","CFCF"]


for scheme in schemes:
	for N in N_list:
		print("1D\t"+scheme+"\t"+str(N)+"\t"+str(error(N,scheme)))
for scheme in schemes:
	for N in N_list:
		print("2D\t"+scheme+"\t"+str(N)+"\t"+str(error(N,scheme)))
for scheme in schemes:
	for N in N_list:
		print("3D\t"+scheme+"\t"+str(N)+"\t"+str(error(N,scheme)))