import traceback
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import math
import scipy.interpolate
import subprocess
import copy
import json
font = {'family': 'normal',
        'weight': 'normal',
        'size': 15}
matplotlib.rc('font', **font)

def save_to_file(results,filename="results.json"):
	with open(filename, 'w') as outfile:
		json.dump(results, outfile)

def load_from_file(filename="results.json"):
	with open(filename, 'r') as f:
		results = json.load(f)
	return results

#results=load_from_file()

#lines=subprocess.check_output(['/home/chris/Documents/repos/ChrisSchoutrop/krylov_schemes/code/convergence_rate'])
lines=subprocess.check_output(['cat','convergence_rate.log'])
#Convert the long string into lines
#https://stackoverflow.com/questions/29643544/python-a-bytes-like-object-is-required-not-str
lines=lines.decode().split('\n')
#Remove empty strings from the list
#https://stackoverflow.com/questions/3845423/remove-empty-strings-from-a-list-of-strings
lines = list(filter(None, lines))
results={}
'''
Schema:
results={
	"1D":{
		"CD":{
			"N:"=[],
			"error:"=[]
			}
		"UW":{
			"N:"=[],
			"error:"=[]
			}
		}
	}
'''
for line in lines:
	line=line.split('\t')
	print(line)
	if line[0] not in results:
		#Dimension of the problem
		#"1D"/"2D"/"3D"
		results[line[0]]={}
	if line[1] not in results[line[0]]:
		#Discretization scheme
		#"UW"/"CD"/ etc.
		results[line[0]][line[1]]={"N":[],"error":[]}
	#Append N
	results[line[0]][line[1]]["N"].append(line[2])
	#Append error
	results[line[0]][line[1]]["error"].append(line[3])

print(results)
for dimension in results.keys():
	#Loop over 1D/2D/3D
	f = plt.figure()
	ax = plt.gca()

	for scheme in results[dimension]:
		#Loop over schemes
		#Convert results from strings to numbers
		#https://stackoverflow.com/questions/1614236/in-python-how-do-i-convert-all-of-the-items-in-a-list-to-floats
		plt.plot(np.float_(results[dimension][scheme]["N"]),np.float_(results[dimension][scheme]["error"]),label=scheme)
	ax.legend(fontsize='small',frameon=False)
	plt.xlabel("N", usetex=False)
	plt.ylabel("$||\\varphi-\\varphi_{exact}||_1/N^D$", usetex=False)
	plt.xscale('log')
	plt.yscale('log')
	plt.autoscale(enable=True, axis='x', tight=True)
	plt.autoscale(enable=True, axis='y', tight=False)
	filename=dimension
	f.savefig(filename+".pdf", bbox_inches='tight')
	f.savefig(filename+".png", bbox_inches='tight')
	plt.clf()
	plt.close()
	#TODO: Print order using a fit

save_to_file(results)