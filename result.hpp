#ifndef RESULT_H
#define RESULT_H
#include <string>
#include <iostream>
struct Result
{

	std::string m_algorithm;
	double m_true_residual;
	double m_estimated_residual;
	int m_iterations;
	int m_return_flag;
	double m_u;
	double m_beta;
	double m_e;
	double m_time;
	int m_N;
	int m_max_iterations;
	double m_tolerance;
	int m_S;
	int m_L;
	int64_t m_timestamp;
	std::string m_scheme;
	double m_cond;
	void print();
	void set_post_info(double true_residual, double estimated_residual, int iterations,
		int return_flag, double time);
	void set_pre_info(std::string algorithm, double u, double beta, double e, int S, int L,
		int64_t timstamp, std::string scheme, double tolerance, int N, int max_iterations, double cond);
	friend std::ostream& operator<<(std::ostream& os, const Result& result);
};
inline std::ostream& operator<<(std::ostream& os, const Result& result)
{
	//https://stackoverflow.com/questions/12802536/c-multiple-definitions-of-operator
	os <<
		result.m_algorithm << "\t" <<
		result.m_true_residual << "\t" <<
		result.m_estimated_residual << "\t" <<
		result.m_iterations << "\t" <<
		result.m_return_flag << "\t" <<
		result.m_u << "\t" <<
		result.m_beta << "\t" <<
		result.m_e << "\t" <<
		result.m_time << "\t" <<
		result.m_N << "\t" <<
		result.m_max_iterations << "\t" <<
		result.m_tolerance << "\t" <<
		result.m_S << "\t" <<
		result.m_L << "\t" <<
		result.m_timestamp << "\t" <<
		result.m_scheme << "\t" <<
		result.m_cond << "\t" <<
		"\n";
	return os;
}
#endif //RESULT_H