#include <iostream>
#include <cmath>

#include <Eigen/Core>
#include <Eigen/SVD>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>

#include <Eigen/UmfPackSupport>
#include <Eigen/SuperLUSupport>

class CondestLU
{
		/*
		        Estimate the 1-norm condition number by using a LU decomposition.
		        This is based on Eigen's implementation for rcond, which is used if A is dense.
		        Otherwise the method adapted for sparse matrices is used.
		*/
	public:
		static double condest(const Eigen::MatrixXd& A)
		{
			using namespace Eigen;
			PartialPivLU<MatrixXd> lu_solver;
			lu_solver.compute(A);
			return 1.0 / lu_solver.rcond();
		}
		static double condest(const Eigen::SparseMatrix<double, Eigen::RowMajor>& A)
		{
			using namespace Eigen;
			SparseMatrix<double, ColMajor> A1 = A.eval();
			SparseMatrix<double, ColMajor> AT = A.adjoint().eval();
			SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> > lu_solver_A;
			SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> > lu_solver_AT;
			lu_solver_A.compute(A1);
			lu_solver_AT.compute(AT);
			double l1_norm = (Eigen::RowVectorXd::Ones(A.rows()) * A.cwiseAbs()).maxCoeff();
			double l1_norm_inv = srcond_invmatrix_L1_norm_estimate(lu_solver_A, lu_solver_AT);
			return l1_norm * l1_norm_inv;
		}
		static double condestUMFPACK(const Eigen::SparseMatrix<double, Eigen::RowMajor>& A)
		{
			//TODO: Figure out why this segfaults/ has problems with "inplace solve"
			//If one comments out:
			//eigen_assert(b.derived().data() != x.derived().data() && " Umfpack does not support inplace solve");
			//in UmfPackSupport.h and it works fine.
			//The results umfpack yields are not the same as superlu and eigen's lu.
			//Somehow there really is an issue, eventhough the solves should not be inplace.
			using namespace Eigen;
			SparseMatrix<double, ColMajor> A1 = A.eval();
			SparseMatrix<double, ColMajor> AT = A.adjoint().eval();
			Eigen::UmfPackLU<SparseMatrix<double, ColMajor>> lu_solver_A;
			Eigen::UmfPackLU<SparseMatrix<double, ColMajor>> lu_solver_AT;

			lu_solver_A.compute(A1);
			lu_solver_AT.compute(AT);
			double l1_norm = (Eigen::RowVectorXd::Ones(A.rows()) * A.cwiseAbs()).maxCoeff();
			double l1_norm_inv = srcond_invmatrix_L1_norm_estimate(lu_solver_A, lu_solver_AT);
			return l1_norm * l1_norm_inv;
		}
		static double condestSuperLU(const Eigen::SparseMatrix<double, Eigen::RowMajor>& A)
		{
			using namespace Eigen;
			Eigen::SuperLU<SparseMatrix<double, RowMajor>> lu_solver;
			lu_solver.analyzePattern(A);
			double rcond;
			lu_solver.factorize(A, rcond);
			return 1.0 / rcond;
			// using namespace Eigen;
			// SparseMatrix<double, ColMajor> A1 = A.eval();
			// SparseMatrix<double, ColMajor> AT = A.adjoint().eval();
			// Eigen::SuperLU<SparseMatrix<double, ColMajor>> lu_solver_A;
			// Eigen::SuperLU<SparseMatrix<double, ColMajor>> lu_solver_AT;

			// lu_solver_A.compute(A1);
			// lu_solver_AT.compute(AT);
			// double l1_norm = (Eigen::RowVectorXd::Ones(A.rows()) * A.cwiseAbs()).maxCoeff();
			// double l1_norm_inv = srcond_invmatrix_L1_norm_estimate(lu_solver_A, lu_solver_AT);
			// return l1_norm * l1_norm_inv;
		}
	private:
		static Eigen::VectorXd srcond_compute_sign(const Eigen::VectorXd& v)
		{
			return (v.array() < static_cast<double>(0))
				.select(-Eigen::VectorXd::Ones(v.size()), Eigen::VectorXd::Ones(v.size()));
		}
		template <typename Decomposition>
		static typename Decomposition::RealScalar srcond_invmatrix_L1_norm_estimate(const Decomposition& dec, const Decomposition& decT)
		{
			using namespace Eigen;
			const Index n = dec.rows();

			if (n == 0)
			{
				return 0;
			}

			VectorXd v = (VectorXd::Ones(n) / double(n)).eval();
			VectorXd w = v.eval();
			//std::cout << "123" << std::endl;
			w = dec.solve(v).eval();
			v = w.eval();

			double lower_bound = v.template lpNorm<1>();

			if (n == 1)
			{
				return lower_bound;
			}

			// Gradient ascent algorithm follows: We know that the optimum is achieved at
			// one of the simplices v = e_i, so in each iteration we follow a
			// super-gradient to move towards the optimal one.
			double old_lower_bound = lower_bound;
			VectorXd sign_vector(n);
			VectorXd old_sign_vector;
			uint64_t v_max_abs_index = -1;
			uint64_t old_v_max_abs_index = v_max_abs_index;
			//std::cout << "143" << std::endl;

			for (int k = 0; k < 4; ++k)
			{
				sign_vector = srcond_compute_sign(v);

				if (k > 0 &&  sign_vector == old_sign_vector)
				{
					// Break if the solution stagnated.
					break;
				}

				// v_max_abs_index = argmax |real( inv(matrix)^T * sign_vector )|
				v = decT.solve(sign_vector); //Use the decomposition of A^T
				v.real().cwiseAbs().maxCoeff(&v_max_abs_index);

				if (v_max_abs_index == old_v_max_abs_index)
				{
					// Break if the solution stagnated.
					break;
				}

				// Move to the new simplex e_j, where j = v_max_abs_index.
				v = VectorXd::Unit(n, v_max_abs_index).eval();
				//std::cout << "164" << std::endl;
				w = dec.solve(v).eval();  // v = inv(matrix) * e_j.
				v = w.eval();
				lower_bound = v.template lpNorm<1>();

				if (lower_bound <= old_lower_bound)
				{
					// Break if the gradient step did not increase the lower_bound.
					break;
				}

				old_sign_vector = sign_vector;


				old_v_max_abs_index = v_max_abs_index;
				old_lower_bound = lower_bound;
			}

			double alternating_sign(double(1));

			for (uint64_t i = 0; i < n; ++i)
			{
				// The static_cast is needed when Scalar is a complex and RealScalar implements expression templates
				v[i] = alternating_sign * static_cast<double>(double(1) + (double(i) / (double(n - 1))));
				alternating_sign = -alternating_sign;
			}

			v = dec.solve(v);
			const double alternate_lower_bound = (2 * v.template lpNorm<1>()) / (3 * double(n));
			return numext::maxi(lower_bound, alternate_lower_bound);

		}
};