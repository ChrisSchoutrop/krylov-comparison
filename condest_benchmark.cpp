#include <iostream>
#include <Eigen/SVD>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <chrono>
#include <cmath>
#include <random>
#include <Eigen/SparseLU>
#include "discretizer.hpp"
#include "condest.hpp"
using namespace Eigen;

int main()
{
	using namespace Eigen;
	using namespace std::chrono;

	std::cout << "Sparse 1D" << std::endl;

	for (int N = 3; N < 1000; N = N * 2)
	{
		std::string scheme = "CD";
		std::cout << "N: " << N << std::endl;
		/*
		        Sparse systems
		*/
		auto start = high_resolution_clock::now();
		auto stop = high_resolution_clock::now();
		double cond;

		Eigen::SparseMatrix<double, Eigen::RowMajor> A(N, N  );
		Eigen::VectorXd b(N  );
		Input1D input;
		input.x_w = 0;
		input.x_e = 1;
		input.N_x = N;
		input.phi_w = 1.0;
		input.phi_e = 1.0;
		double dx = (input.x_e - input.x_w) / (input.N_x - 1);
		input.u = [](double x)
		{
			return 0.0;
		};
		input.e = [](double x)
		{
			return 1.0;
		};
		input.s = [](double x)
		{
			return 1.0;
		};
		input.sl = [](double x)
		{
			return 0.0;
		};
		discretizer::get_flux(input, scheme);

		//construct_2D_indexing(A, b, N, u, e, beta);
		discretizer::discretize(input, A, b);

		/*
		        Test BDCSVD
		*/
		if (N  < 2e3)
		{
			start = high_resolution_clock::now();
			cond = CondestBDCSVD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "BDCSVD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}

		/*
		        Test JacobiSVD
		*/
		if (N  < 2e3)
		{
			start = high_resolution_clock::now();
			cond = CondestJacobiSVD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "JacSVD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}

		/*
		        Test LU method adapted from Eigen's dense method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test GD method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestGD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "GD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test NLCG method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestNLCG::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "NLCG\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLSQR::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR+GD method
		*/
		{
			start = high_resolution_clock::now();
			Eigen::VectorXd v_min, v_max;
			double sigma_max, sigma_min;
			cond = CondestLSQR::condest(A, v_min, v_max, sigma_min, sigma_max);
			std::cout << cond << std::endl;
			double sigma_max_GD = CondestGD::sigma_max(A, v_max);
			double sigma_min_GD = CondestGD::sigma_min(A, v_min);
			cond = std::max(sigma_max_GD, sigma_max) / std::min(sigma_min_GD, sigma_min);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR+GD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR+NLCG method
		*/
		{
			start = high_resolution_clock::now();
			Eigen::VectorXd v_min, v_max;
			double sigma_max, sigma_min;
			cond = CondestLSQR::condest(A, v_min, v_max, sigma_min, sigma_max);
			std::cout << cond << std::endl;
			double sigma_max_NLCG = CondestNLCG::sigma_max(A, v_max);
			double sigma_min_NLCG = CondestNLCG::sigma_min(A, v_min);
			cond = std::max(sigma_max_NLCG, sigma_max) / std::min(sigma_min_NLCG, sigma_min);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR+NLCG\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LU UMFPACK
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condestUMFPACK(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "UMFPACKLU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test SuperLU
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condestSuperLU(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "SuperLU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
	}

	//return 0;
	std::cout << "Dense" << std::endl;

	for (int N = 25; N < 801; N = N * 2)
	{
		/*
		        Dense systems
		*/
		std::cout << "N: " << N << std::endl;
		double cond;
		auto start = high_resolution_clock::now();
		auto stop = high_resolution_clock::now();
		MatrixXd A = MatrixXd::Random(N, N);

		/*
		        Test BDCSVD
		*/
		{
			start = high_resolution_clock::now();

			cond = CondestBDCSVD::condest(A);

			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "BDCSVD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
		        Test JacobiSVD
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestJacobiSVD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "JacSVD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
		        Test LU method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condest(A);
			std::cout << cond << std::endl;
			std::cout << "LU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test GD method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestGD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "GD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test NLCG method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestNLCG::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "NLCG\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLSQR::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR+GD method
		*/
		{
			start = high_resolution_clock::now();
			Eigen::VectorXd v_min, v_max;
			double sigma_max, sigma_min;
			cond = CondestLSQR::condest(A, v_min, v_max, sigma_min, sigma_max);
			std::cout << cond << std::endl;
			double sigma_max_GD = CondestGD::sigma_max(A, v_max);
			double sigma_min_GD = CondestGD::sigma_min(A, v_min);
			cond = std::max(sigma_max_GD, sigma_max) / std::min(sigma_min_GD, sigma_min);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR+GD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR+NLCG method
		*/
		{
			start = high_resolution_clock::now();
			Eigen::VectorXd v_min, v_max;
			double sigma_max, sigma_min;
			cond = CondestLSQR::condest(A, v_min, v_max, sigma_min, sigma_max);
			std::cout << cond << std::endl;
			double sigma_max_NLCG = CondestNLCG::sigma_max(A, v_max);
			double sigma_min_NLCG = CondestNLCG::sigma_min(A, v_min);
			cond = std::max(sigma_max_NLCG, sigma_max) / std::min(sigma_min_NLCG, sigma_min);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR+NLCG\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
	}

	std::cout << "Sparse 2D" << std::endl;

	for (int N = 3; N < 400; N = N * 2)
	{
		std::string scheme = "CFCF";
		std::cout << "N: " << N << std::endl;
		/*
		        Sparse systems
		*/
		auto start = high_resolution_clock::now();
		auto stop = high_resolution_clock::now();
		double cond;

		Eigen::SparseMatrix<double, Eigen::RowMajor> A(N * N, N * N );
		Eigen::VectorXd b(N * N );
		Input2D input;
		input.x_w = 0;
		input.x_e = 1;
		input.y_s = 0;
		input.y_n = 1;
		input.N_x = N;
		input.N_y = N;
		input.phi_w = [](double y)
		{
			return 1.0;
		};
		input.phi_e = [](double y)
		{
			return 1.0;
		};
		input.phi_n = [](double x)
		{
			return 1.0;
		};
		input.phi_s = [](double x)
		{
			return 1.0;
		};
		double dx = (input.x_e - input.x_w) / (input.N_x - 1);
		double dy = (input.y_n - input.y_s) / (input.N_y - 1);
		input.u_x = [](double x, double y)
		{
			return 1.0;
		};
		input.u_y = [](double x, double y)
		{
			return 1.0;
		};
		input.e = [](double x, double y)
		{
			return 1.0;
		};
		input.s = [](double x, double y)
		{
			return 1.0;
		};
		input.sl = [](double x, double y)
		{
			return 0.0;
		};
		discretizer::get_flux(input, scheme);

		//construct_2D_indexing(A, b, N, u, e, beta);
		if (scheme == "CFCF")
		{
			discretizer::discretizecf(input, A, b);
		}
		else
		{
			discretizer::discretize(input, A, b);
		}

		/*
		        Test BDCSVD
		*/
		if (N * N  < 2e3)
		{
			start = high_resolution_clock::now();
			cond = CondestBDCSVD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "BDCSVD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}

		/*
		        Test JacobiSVD
		*/
		if (N * N  < 2e3)
		{
			start = high_resolution_clock::now();
			cond = CondestJacobiSVD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "JacSVD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}

		/*
		        Test LU method adapted from Eigen's dense method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test GD method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestGD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "GD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test NLCG method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestNLCG::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "NLCG\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLSQR::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR+GD method
		*/
		{
			start = high_resolution_clock::now();
			Eigen::VectorXd v_min, v_max;
			double sigma_max, sigma_min;
			cond = CondestLSQR::condest(A, v_min, v_max, sigma_min, sigma_max);
			std::cout << cond << std::endl;
			double sigma_max_GD = CondestGD::sigma_max(A, v_max);
			double sigma_min_GD = CondestGD::sigma_min(A, v_min);
			cond = std::max(sigma_max_GD, sigma_max) / std::min(sigma_min_GD, sigma_min);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR+GD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR+NLCG method
		*/
		{
			start = high_resolution_clock::now();
			Eigen::VectorXd v_min, v_max;
			double sigma_max, sigma_min;
			cond = CondestLSQR::condest(A, v_min, v_max, sigma_min, sigma_max);
			std::cout << cond << std::endl;
			double sigma_max_NLCG = CondestNLCG::sigma_max(A, v_max);
			double sigma_min_NLCG = CondestNLCG::sigma_min(A, v_min);
			cond = std::max(sigma_max_NLCG, sigma_max) / std::min(sigma_min_NLCG, sigma_min);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR+NLCG\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LU UMFPACK
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condestUMFPACK(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "UMFPACKLU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test SuperLU
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condestSuperLU(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "SuperLU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
	}


	std::cout << "Sparse 3D" << std::endl;


	for (int N = 3; N < 200; N = N * 2)
	{
		std::cout << "N: " << N << std::endl;
		std::string scheme = "CFCF";
		/*
		        Sparse systems
		*/
		auto start = high_resolution_clock::now();
		auto stop = high_resolution_clock::now();
		double cond;

		Eigen::SparseMatrix<double, Eigen::RowMajor> A(N * N * N, N * N * N);
		Eigen::VectorXd b(N * N * N);
		Input3D input;
		input.x_w = 0;
		input.x_e = 1;
		input.y_s = 0;
		input.y_n = 1;
		input.z_d = 0;
		input.z_u = 1;
		input.N_x = N;
		input.N_y = N;
		input.N_z = N;
		input.phi_w = [](double y, double z)
		{
			return 1.0;
		};
		input.phi_e = [](double y, double z)
		{
			return 1.0;
		};
		input.phi_n = [](double x, double z)
		{
			return 1.0;
		};
		input.phi_s = [](double x, double z)
		{
			return 1.0;
		};
		input.phi_d = [](double x, double y)
		{
			return 1.0;
		};
		input.phi_u = [](double x, double y)
		{
			return 1.0;
		};
		double dx = (input.x_e - input.x_w) / (input.N_x - 1);
		double dy = (input.y_n - input.y_s) / (input.N_y - 1);
		double dz = (input.z_u - input.z_d) / (input.N_z - 1);
		input.u_x = [](double x, double y, double z)
		{
			return 1.0;
		};
		input.u_y = [](double x, double y, double z)
		{
			return 1.0;
		};
		input.u_z = [](double x, double y, double z)
		{
			return 1.0;
		};
		input.e = [](double x, double y, double z)
		{
			return 1.0;
		};
		input.s = [](double x, double y, double z)
		{
			return 1.0;
		};
		input.sl = [](double x, double y, double z)
		{
			return 0.0;
		};
		discretizer::get_flux(input, scheme);

		//construct_2D_indexing(A, b, N, u, e, beta);
		if (scheme == "CFCF")
		{
			discretizer::discretizecf(input, A, b);
		}
		else
		{
			discretizer::discretize(input, A, b);
		}

		/*
		        Test BDCSVD
		*/
		if (N * N * N < 2e3)
		{
			start = high_resolution_clock::now();
			cond = CondestBDCSVD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "BDCSVD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}

		/*
		        Test JacobiSVD
		*/
		if (N * N * N < 2e3)
		{
			start = high_resolution_clock::now();
			cond = CondestJacobiSVD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "JacSVD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}

		/*
		        Test LU method adapted from Eigen's dense method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test GD method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestGD::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "GD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test NLCG method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestNLCG::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "NLCG\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR method
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLSQR::condest(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR+GD method
		*/
		{
			start = high_resolution_clock::now();
			Eigen::VectorXd v_min, v_max;
			double sigma_max, sigma_min;
			cond = CondestLSQR::condest(A, v_min, v_max, sigma_min, sigma_max);
			std::cout << cond << std::endl;
			double sigma_max_GD = CondestGD::sigma_max(A, v_max);
			double sigma_min_GD = CondestGD::sigma_min(A, v_min);
			cond = std::max(sigma_max_GD, sigma_max) / std::min(sigma_min_GD, sigma_min);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR+GD\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LSQR+NLCG method
		*/
		{
			start = high_resolution_clock::now();
			Eigen::VectorXd v_min, v_max;
			double sigma_max, sigma_min;
			cond = CondestLSQR::condest(A, v_min, v_max, sigma_min, sigma_max);
			std::cout << cond << std::endl;
			double sigma_max_NLCG = CondestNLCG::sigma_max(A, v_max);
			double sigma_min_NLCG = CondestNLCG::sigma_min(A, v_min);
			cond = std::max(sigma_max_NLCG, sigma_max) / std::min(sigma_min_NLCG, sigma_min);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "LSQR+NLCG\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test LU UMFPACK
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condestUMFPACK(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "UMFPACKLU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
		/*
			Test SuperLU
		*/
		{
			start = high_resolution_clock::now();
			cond = CondestLU::condestSuperLU(A);
			stop = high_resolution_clock::now();
			std::cout << cond << std::endl;
			std::cout << "SuperLU\t Time: " << duration_cast<milliseconds>(stop - start).count() << " ms" << std::endl;
		}
	}

}


