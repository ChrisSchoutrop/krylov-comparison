#include <iostream>
#include <Eigen/SuperLUSupport>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/LU>
#include <Eigen/UmfPackSupport>
int main()
{
	using namespace Eigen;
	MatrixXd A_dense = MatrixXd::Random(5, 5);
	SparseMatrix<double, ColMajor> A = A_dense.sparseView();
	VectorXd x = VectorXd::Random(5);
	VectorXd b = A * x;

	VectorXd x_lu;
	Eigen::SuperLU<SparseMatrix<double, ColMajor>> m_lu;
	//Eigen::UmfPackLU<SparseMatrix<double, ColMajor>> m_lu;
	//Eigen::PartialPivLU<SparseMatrix<double,ColMajor>> m_lu;
	m_lu.compute(A);
	x_lu = m_lu.solve(b);
	std::cout << "x:\n" << x << "\n";
	std::cout << "x_lu:\n" << x_lu << "\n";
	std::cout << (x - x_lu).norm() << std::endl;
	/*
	        problem is dat eigen alleen werkt met versie 4 van SuperLU
	        De github heeft versie 6, dus er moet vanalles veranderd worden vermoed ik.
	*/
}