#include <array>
#ifndef FLUX_H
#define FLUX_H
class flux
{
		/*
		        Numerical flux is given by
		        F_{j+1/2}=
				\alpha_{j+1/2}\varphi_j
				+\beta_{j+1/2}\varphi_{j+1}
		        +\Delta x(
					\gamma_{j+1/2}s_j
					+\delta_{j+1/2}s_{j+1}
					)

		        Here the coefficients \alpha,\beta,\gamma,\delta are computed.
				Coefficients obtained out from ACFD4.pdf
			Note: every sign is a +!
		*/
	public:
		static std::array<double, 4> UW(const double u, const double e, const double dx);
		static std::array<double, 4> CD(const double u, const double e, const double dx);
		static std::array<double, 4> HF(const double u, const double e, const double dx);
		static std::array<double, 4> CF(const double u, const double e, const double dx);
	private:
};
#endif //FLUX_H