#ifndef MF_H
#define MF_H
#include <vector>
class mf
{
	public:
		static double B(const double z);
		static double C(const double z);
		static double W(const double z);
		static double sgn(const double z);
		static double A(const double u, const double e);
		static double P(const double A, const  double dx);
		static double P(const double u, const  double e, const double dx);
		static std::vector<double> logspace(const double a, const double b, const int N = 100,
			const double base = 10);
	private:
};
#endif //MF_H