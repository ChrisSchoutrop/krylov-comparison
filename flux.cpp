#include <array>
#include "mf.hpp"
#include "flux.hpp"
std::array<double, 4> flux::UW(const double u, const double e, const double dx)
{
	std::array<double, 4> output{0, 0, 0, 0};
	enum coefficient {alpha, beta, gamma, delta};
	//UW: Slide 15

	//Advection term
	if (u > 0)
	{
		output[alpha] = u;
	}
	else
	{
		output[beta] = u;
	}

	//Diffusion term
	output[alpha] += e / dx;
	output[beta] -= e / dx;
	return output;
};
std::array<double, 4> flux::CD(const double u, const double e, const double dx)
{
	std::array<double, 4> output{0, 0, 0, 0};
	enum coefficient {alpha, beta, gamma, delta};
	//CD: Slide 11

	//Advection term
	output[alpha] = 0.5 * u;
	output[beta] = 0.5 * u;

	//Diffusion term
	output[alpha] += e / dx;
	output[beta] -= e / dx;
	return output;
};
std::array<double, 4> flux::HF(const double u, const double e, const double dx)
{
	std::array<double, 4> output{0, 0, 0, 0};
	enum coefficient {alpha, beta, gamma, delta};
	//HF: Slide 21

	double P = mf::P(u, e, dx);
	output[alpha] = e * mf::B(-P) / dx;
	output[beta] = -e * mf::B(P) / dx;
	return output;
};
std::array<double, 4> flux::CF(const double u, const double e, const double dx)
{
	std::array<double, 4> output{0, 0, 0, 0};
	enum coefficient {alpha, beta, gamma, delta};
	//CF: Slide 31
	double P = mf::P(u, e, dx);
	output[alpha] = e * mf::B(-P) / dx;
	output[beta] = -e * mf::B(P) / dx;
	output[gamma] = dx * (mf::C(-P));
	output[delta] = -dx * (mf::C(P));
	return output;
};