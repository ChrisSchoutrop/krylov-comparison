#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include "IDRStab.h"
#include <vector>
#include <string>
#include <iostream>
#include <type_traits>
#include <cmath>
#include "discretizer.hpp"
#include "input_struct.hpp"
#include "mf.hpp"
#include "result.hpp"
/*
        reset;g++ -o convergence_rate convergence_rate.cpp mf.cpp flux.cpp discretizer.cpp input_struct.cpp result.cpp -I eigen-eigen-323c052e1731/ -O3 -g
*/
/*
	In[399]:=  ClearAll["Global`*"]
	Clear["Global`*"]
	Clear["`*"]
	phi[x_] := a Sin[b x] + c
	Solve[D[u*phi[x] - e D[phi[x], x], x] == s - beta*phi[x], s]
	ClearAll["Global`*"]
	Clear["Global`*"]
	Clear["`*"]
	phi[x_, y_] := a Sin[b (x + y)] + c
	Solve[Div[{u, u}*phi[x, y] - e Grad[phi[x, y], {x, y}], {x, y}] ==
	s - beta*phi[x, y], s]
	ClearAll["Global`*"]
	Clear["Global`*"]
	Clear["`*"]
	phi[x_, y_, z_] := a Sin[b (x + y + z)] + c
	Solve[Div[{u, u, u}*phi[x, y, z] -
	e Grad[phi[x, y, z], {x, y, z}], {x, y, z}] ==
	s - beta*phi[x, y, z], s]


	Out[403]= {{s ->
	beta c + a b u Cos[b x] + a beta Sin[b x] + a b^2 e Sin[b x]}}

	Out[408]= {{s ->
	beta c + 2 a b u Cos[b (x + y)] + a beta Sin[b (x + y)] +
	2 a b^2 e Sin[b (x + y)]}}

	Out[413]= {{s ->
	beta c + 3 a b u Cos[b (x + y + z)] + a beta Sin[b (x + y + z)] +
	3 a b^2 e Sin[b (x + y + z)]}}
*/

typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SparseMatrixType;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorType;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> DenseMatrixTypeCol;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DenseMatrixTypeRow;
double exact_solution1(double a, double b, double c, double x)
{
	return a * std::sin(b * x) + c;
}
double exact_solution1_s(double a, double b, double c, double u, double e, double sl, double x)
{
	return sl * c + a * b * u * std::cos(b * x) + a * sl * std::sin(b * x) + a * b * b * e * std::sin(b * x);
}
double exact_solution2(double a, double b, double c, double x, double y)
{
	return a * std::sin(b * (x + y)) + c;
}
double exact_solution2_s(double a, double b, double c, double u, double e, double sl, double x,
	double y)
{
	return  sl * c + 2 * a * b * u * std::cos(b * (x + y)) + a * sl * std::sin(b * (x + y)) +
		2 * a * b * b * e * std::sin(b * (x + y));
}
double exact_solution3(double a, double b, double c, double x, double y, double z)
{
	return a * std::sin(b * (x + y + z)) + c;
}
double exact_solution3_s(double a, double b, double c, double u, double e, double sl, double x,
	double y, double z)
{
	return  sl * c + 3 * a * b * u * std::cos(b * (x + y + z)) + a * sl * std::sin(b * (x + y + z)) +
		3 * a * b * b * e * std::sin(b * (x + y + z));
}
double compare_solutions(VectorType value, VectorType expected)
{
	double deviation = (value - expected).lpNorm<1>();
	return deviation / expected.size();
}
double solve1d(int N_x, std::string scheme)
{
	Input1D input;
	input.N_x = N_x;

	input.x_w = 0;
	input.x_e = 1;
	double a = 1;
	double b = 6;
	double c = 2;
	double u = 1;
	double e = 1;
	double sl = 10;

	input.phi_w = exact_solution1(a, b, c, input.x_w);
	input.phi_e = exact_solution1(a, b, c, input.x_e);

	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	input.u = [u](double x)
	{
		return u;
	};
	input.e = [e](double x)
	{
		return e;
	};
	input.s = [a, b, c, u, e, sl](double x)
	{
		return exact_solution1_s(a, b, c, u, e, sl, x);
	};
	input.sl = [sl](double x)
	{
		return sl;
	};

	if (scheme == "CF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CF(u, e, dx);
		};
	}
	else if (scheme == "HF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::HF(u, e, dx);
		};
	}
	else if (scheme == "CD")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CD(u, e, dx);
		};
	}
	else if (scheme == "UW")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::UW(u, e, dx);
		};
	}

	SparseMatrixType A;
	VectorType rhs;
	discretizer::discretize(input, A, rhs);
	Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
	solver.analyzePattern(A);
	solver.factorize(A);
	VectorType phi = solver.solve(rhs);
	VectorType phi_exact(input.N_x);

	for (size_t i = 0; i < input.N_x; i++)
	{
		phi_exact(i) = exact_solution1(a, b, c, input.x_w + dx * i);
	}

	return compare_solutions(phi, phi_exact);
}
double solve2d(int N_x, std::string scheme)
{
	Input2D input;
	int N = N_x;
	input.x_w = 0;
	input.x_e = 1;
	input.y_s = 0;
	input.y_n = 1;
	input.N_x = N;
	input.N_y = N;
	double a = 1;
	double b = 6;
	double c = 2;
	double u = 1;
	double e = 1;
	double sl = 10;

	input.phi_w = [a, b, c, input](double y)
	{
		return exact_solution2(a, b, c, input.x_w, y);
	};
	input.phi_e = [a, b, c, input](double y)
	{
		return exact_solution2(a, b, c, input.x_e, y);
	};
	input.phi_n = [a, b, c, input](double x)
	{
		return exact_solution2(a, b, c,  x, input.y_n);
	};
	input.phi_s = [a, b, c, input](double x)
	{
		return exact_solution2(a, b, c, x, input.y_s);
	};

	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	input.u_x = [u](double x, double y)
	{
		return u;
	};
	input.u_y = [u](double x, double y)
	{
		return u;
	};
	input.e = [e](double x, double y)
	{
		return e;
	};
	input.s = [a, b, c, u, e, sl](double x, double y)
	{
		return exact_solution2_s(a, b, c, u, e, sl, x, y);
	};
	input.sl = [sl](double x, double y)
	{
		return sl;
	};

	if (scheme == "CF" || scheme == "CFCF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CF(u, e, dx);
		};
	}
	else if (scheme == "HF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::HF(u, e, dx);
		};
	}
	else if (scheme == "CD")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CD(u, e, dx);
		};
	}
	else if (scheme == "UW")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::UW(u, e, dx);
		};
	}

	SparseMatrixType A;
	VectorType rhs;

	if (scheme == "CFCF")
	{
		discretizer::discretizecf(input, A, rhs);
	}
	else
	{
		discretizer::discretize(input, A, rhs);
	}

	Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
	solver.analyzePattern(A);
	solver.factorize(A);
	//Eigen::BiCGSTAB<SparseMatrixType> solver(A);
	//Eigen::IDRStab<SparseMatrixType> solver(A);
	//solver.compute(A);
	//solver.setTolerance(1e-12);
	//solver.setMaxIterations(2000);
	//VectorType phi = VectorType::Ones(b.rows());
	//phi = solver.solveWithGuess(rhs, phi);
	VectorType phi = solver.solve(rhs);
	VectorType phi_exact(input.N_x * input.N_y);

	for (int i = 0; i < input.N_x; i++)
	{
		for (int j = 0; j < input.N_y; j++)
		{
			//Index of the cell
			int ix_c = i + input.N_x * j;
			//Position of the nodal point
			double x = input.x_w + i * dx;
			double y = input.y_s + j * dy;
			phi_exact(ix_c) = exact_solution2(a, b, c, x, y);
		}


	}

	return compare_solutions(phi, phi_exact);
}
double solve3d(int N_x, std::string scheme)
{
	Input3D input;
	int N = N_x;
	input.x_w = 0;
	input.x_e = 1;
	input.y_s = 0;
	input.y_n = 1;
	input.z_d = 0;
	input.z_u = 1;
	input.N_x = N;
	input.N_y = N;
	input.N_z = N;
	double a = 1;
	double b = 6;
	double c = 2;
	double u = 1;
	double e = 1;
	double sl = 10;

	input.phi_w = [a, b, c, input](double y, double z)
	{
		return exact_solution3(a, b, c, input.x_w, y, z);
	};
	input.phi_e = [a, b, c, input](double y, double z)
	{
		return exact_solution3(a, b, c, input.x_e, y, z);
	};
	input.phi_n = [a, b, c, input](double x, double z)
	{
		return exact_solution3(a, b, c,  x, input.y_n, z);
	};
	input.phi_s = [a, b, c, input](double x, double z)
	{
		return exact_solution3(a, b, c,  x, input.y_s, z);
	};
	input.phi_d = [a, b, c, input](double x, double y)
	{
		return exact_solution3(a, b, c, x, y, input.z_d);
	};
	input.phi_u = [a, b, c, input](double x, double y)
	{
		return exact_solution3(a, b, c, x, y, input.z_u);
	};

	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	double dz = (input.z_u - input.z_d) / (input.N_z - 1);
	input.u_x = [u](double x, double y, double z)
	{
		return u;
	};
	input.u_y = [u](double x, double y, double z)
	{
		return u;
	};
	input.u_z = [u](double x, double y, double z)
	{
		return u;
	};
	input.e = [e](double x, double y, double z)
	{
		return e;
	};
	input.s = [a, b, c, u, e, sl](double x, double y, double z)
	{
		return exact_solution3_s(a, b, c, u, e, sl, x, y, z);
	};
	input.sl = [sl](double x, double y, double z)
	{
		return sl;
	};

	if (scheme == "CF" || scheme == "CFCF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CF(u, e, dx);
		};
	}
	else if (scheme == "HF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::HF(u, e, dx);
		};
	}
	else if (scheme == "CD")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CD(u, e, dx);
		};
	}
	else if (scheme == "UW")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::UW(u, e, dx);
		};
	}

	SparseMatrixType A;
	VectorType rhs;

	if (scheme == "CFCF")
	{
		discretizer::discretizecf(input, A, rhs);
	}
	else
	{
		discretizer::discretize(input, A, rhs);
	}

	//Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
	//solver.analyzePattern(A);
	//solver.factorize(A);
	//VectorType phi=solver.solve(rhs);
	// Eigen::BiCGSTAB<SparseMatrixType, Eigen::DiagonalPreconditioner<double>> solver;
	Eigen::IDRStab<SparseMatrixType, Eigen::DiagonalPreconditioner<double>> solver;
	solver.compute(A);
	solver.setTolerance(1e-12);
	solver.setMaxIterations(2000);
	VectorType phi = VectorType::Ones(rhs.rows());
	phi = solver.solveWithGuess(rhs, phi);

	VectorType phi_exact(input.N_x * input.N_y * input.N_z);

	for (int i = 0; i < input.N_x; i++)
	{
		for (int j = 0; j < input.N_y; j++)
		{
			for (int k = 0; k < input.N_z; k++)
			{

				//Index of the cell
				int ix_c = i + input.N_x * j + input.N_x * input.N_y * k;
				//Position of the nodal point
				double x = input.x_w + i * dx;
				double y = input.y_s + j * dy;
				double z = input.z_d + k * dz;
				phi_exact(ix_c) = exact_solution3(a, b, c, x, y, z);
			}
		}


	}

	return compare_solutions(phi, phi_exact);
}

int main()
{
	std::vector<int> N_vec{5, 11, 21, 41, 81, 161, 321};
	std::vector<std::string>schemes{"UW", "CD", "HF", "CF"};


	//Note: This can be done in one loop, but the 3D computation may take way too long
	//Hence separated in 3
	for (auto N : N_vec)
	{
		for (auto scheme : schemes)
		{
			std::cout << "1D\t" << scheme << "\t" << N << "\t" << solve1d(N, scheme) << std::endl;
		}
	}

	for (auto N : N_vec)
	{
		for (auto scheme : schemes)
		{
			std::cout << "2D\t" << scheme << "\t" << N << "\t" << solve2d(N, scheme) << std::endl;
		}

		std::cout << "2D\t" << "CFCF\t" << N << "\t" << solve2d(N, "CFCF") << std::endl;
	}

	for (auto N : N_vec)
	{
		for (auto scheme : schemes)
		{
			std::cout << "3D\t" << scheme << "\t" << N << "\t" << solve3d(N, scheme) << std::endl;
		}

		std::cout << "3D\t" << "CFCF\t" << N << "\t" << solve3d(N, "CFCF") << std::endl;
	}

}