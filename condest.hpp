#include <iostream>
#include <cmath>

#include <Eigen/Core>
#include <Eigen/SVD>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>

#include <Eigen/UmfPackSupport>
#include <Eigen/SuperLUSupport>

#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/math/special_functions/erf.hpp>
#include <boost/math/tools/minima.hpp>

#include <chrono>
#include <cmath>
#include <random>
#include <openblas/lapacke.h>

//TODO: Split into header+implementation
//Not a high priority, SuperLU works best, the other methods aren't preferred.

class CondestJacobiSVD
{
		/*
		        Compute the 2-norm condition number using Eigen's JacobiSVD
		*/
	public:
		static double condest(const Eigen::MatrixXd& A)
		{
			using namespace Eigen;
			JacobiSVD<MatrixXd> jacobisvd(A);
			return jacobisvd.singularValues()(0) / jacobisvd.singularValues()(jacobisvd.singularValues().size() - 1);
		}
};
class CondestBDCSVD
{
		/*
		        Compute the 2-norm condition number using Eigen's BDCSVD
		*/
	public:
		static double condest(const Eigen::MatrixXd& A)
		{
			using namespace Eigen;
			BDCSVD<MatrixXd> bdcsvd(A);
			return bdcsvd.singularValues()(0) / bdcsvd.singularValues()(bdcsvd.singularValues().size() - 1);
		}
};
class CondestLU
{
		/*
		        Estimate the 1-norm condition number by using a LU decomposition.
		        This is based on Eigen's implementation for rcond, which is used if A is dense.
		        Otherwise the method adapted for sparse matrices is used.
		*/
	public:
		static double condest(const Eigen::MatrixXd& A)
		{
			using namespace Eigen;
			PartialPivLU<MatrixXd> lu_solver;
			lu_solver.compute(A);
			return 1.0 / lu_solver.rcond();
		}
		static double condest(const Eigen::SparseMatrix<double, Eigen::RowMajor>& A)
		{
			return 0.0;
			// using namespace Eigen;
			// SparseMatrix<double, ColMajor> A1 = A.eval();
			// //SparseMatrix<double, ColMajor> AT = A.adjoint().eval();
			// SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> > lu_solver_A;
			// //SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> > lu_solver_AT;
			// lu_solver_A.compute(A1);
			// //lu_solver_AT.compute(AT);
			// double l1_norm = (Eigen::RowVectorXd::Ones(A.rows()) * A.cwiseAbs()).maxCoeff();
			// double l1_norm_inv = srcond_invmatrix_L1_norm_estimate(lu_solver_A);
			// return l1_norm * l1_norm_inv;
		}
		static double condestUMFPACK(const Eigen::SparseMatrix<double, Eigen::RowMajor>& A)
		{
			return 0.0;
			// //TODO: Figure out why this segfaults/ has problems with "inplace solve"
			// //If one comments out:
			// //eigen_assert(b.derived().data() != x.derived().data() && " Umfpack does not support inplace solve");
			// //in UmfPackSupport.h and it works fine.
			// //The results umfpack yields are not the same as superlu and eigen's lu.
			// //Somehow there really is an issue, eventhough the solves should not be inplace.
			// using namespace Eigen;
			// SparseMatrix<double, ColMajor> A1 = A.eval();
			// //SparseMatrix<double, ColMajor> AT = A.adjoint().eval();
			// Eigen::UmfPackLU<SparseMatrix<double, ColMajor>> lu_solver_A;
			// //Eigen::UmfPackLU<SparseMatrix<double, ColMajor>> lu_solver_AT;

			// lu_solver_A.compute(A1);
			// //lu_solver_AT.compute(AT);
			// double l1_norm = (Eigen::RowVectorXd::Ones(A.rows()) * A.cwiseAbs()).maxCoeff();
			// double l1_norm_inv = srcond_invmatrix_L1_norm_estimate(lu_solver_A);
			// return l1_norm * l1_norm_inv;
		}
		static double condestSuperLU(const Eigen::SparseMatrix<double, Eigen::RowMajor>& A)
		{
			using namespace Eigen;
			//SparseMatrix<double, ColMajor> A1 = A.eval();
			//SparseMatrix<double, ColMajor> AT = A.adjoint().eval();
			Eigen::SuperLU<SparseMatrix<double, ColMajor>> lu_solver_A;
			//Eigen::SuperLU<SparseMatrix<double, ColMajor>> lu_solver_AT;
			double rcond;
			lu_solver_A.analyzePattern(A);
			lu_solver_A.factorize(A, rcond);
			return 1.0 / rcond;
			//lu_solver_AT.compute(AT);
			//double l1_norm = (Eigen::RowVectorXd::Ones(A.rows()) * A.cwiseAbs()).maxCoeff();
			//double l1_norm_inv = srcond_invmatrix_L1_norm_estimate(lu_solver_A);
			//return l1_norm * l1_norm_inv;
		}
	private:
		static Eigen::VectorXd srcond_compute_sign(const Eigen::VectorXd& v)
		{
			return (v.array() < static_cast<double>(0))
				.select(-Eigen::VectorXd::Ones(v.size()), Eigen::VectorXd::Ones(v.size()));
		}
		template <typename Decomposition>
		//static typename Decomposition::RealScalar srcond_invmatrix_L1_norm_estimate(const Decomposition& dec, const Decomposition& decT)
		static typename Decomposition::RealScalar srcond_invmatrix_L1_norm_estimate(const Decomposition& dec)
		{
			using namespace Eigen;
			const Index n = dec.rows();

			if (n == 0)
			{
				return 0;
			}

			VectorXd v = (VectorXd::Ones(n) / double(n)).eval();
			VectorXd w = v.eval();
			//std::cout << "123" << std::endl;
			w = dec.solve(v).eval();
			v = w.eval();

			double lower_bound = v.template lpNorm<1>();

			if (n == 1)
			{
				return lower_bound;
			}

			// Gradient ascent algorithm follows: We know that the optimum is achieved at
			// one of the simplices v = e_i, so in each iteration we follow a
			// super-gradient to move towards the optimal one.
			double old_lower_bound = lower_bound;
			VectorXd sign_vector(n);
			VectorXd old_sign_vector;
			uint64_t v_max_abs_index = -1;
			uint64_t old_v_max_abs_index = v_max_abs_index;
			//std::cout << "143" << std::endl;

			for (int k = 0; k < 4; ++k)
			{
				sign_vector = srcond_compute_sign(v);

				if (k > 0 &&  sign_vector == old_sign_vector)
				{
					// Break if the solution stagnated.
					break;
				}

				// v_max_abs_index = argmax |real( inv(matrix)^T * sign_vector )|
				//v = decT.solve(sign_vector); //Use the decomposition of A^T
				v.transpose() = dec.solve(sign_vector.transpose());
				v.real().cwiseAbs().maxCoeff(&v_max_abs_index);

				if (v_max_abs_index == old_v_max_abs_index)
				{
					// Break if the solution stagnated.
					break;
				}

				// Move to the new simplex e_j, where j = v_max_abs_index.
				v = VectorXd::Unit(n, v_max_abs_index).eval();
				//std::cout << "164" << std::endl;
				w = dec.solve(v).eval();  // v = inv(matrix) * e_j.
				v = w.eval();
				lower_bound = v.template lpNorm<1>();

				if (lower_bound <= old_lower_bound)
				{
					// Break if the gradient step did not increase the lower_bound.
					break;
				}

				old_sign_vector = sign_vector;


				old_v_max_abs_index = v_max_abs_index;
				old_lower_bound = lower_bound;
			}

			double alternating_sign(double(1));

			for (uint64_t i = 0; i < n; ++i)
			{
				// The static_cast is needed when Scalar is a complex and RealScalar implements expression templates
				v[i] = alternating_sign * static_cast<double>(double(1) + (double(i) / (double(n - 1))));
				alternating_sign = -alternating_sign;
			}

			v = dec.solve(v);
			const double alternate_lower_bound = (2 * v.template lpNorm<1>()) / (3 * double(n));
			return numext::maxi(lower_bound, alternate_lower_bound);

		}
};
class CondestNLCG
{
		/*
		        Estimate the condition number, largest singular value, smallest singular value
		        using NonLinear Conjugate Gradient.

		        This method is based on "An Introduction to the Conjugate Gradient Method Without
		        the Agonizing Pain by Jonathan Richard Shewchuk August 4, 1994.
		*/
	public:
		template<typename T>
		static double condest(const T& A)
		{
			//Start from a random vector x as initial guess for the singular vectors
			Eigen::VectorXd x = Eigen::VectorXd::Random(A.rows());
			double sigma_min = NLCG(A, false, x);
			x = Eigen::VectorXd::Random(A.rows());
			double sigma_max = NLCG(A, true, x);
			return sigma_max / sigma_min;
		}
		template<typename T>
		static double condest(const T& A, Eigen::VectorXd x_min, Eigen::VectorXd x_max)
		{
			//Start from initial guess for the singular vectors x_min and x_max.
			double sigma_min = NLCG(A, false, x_min);
			double sigma_max = NLCG(A, true, x_max);
			return sigma_max / sigma_min;
		}
		template<typename T>
		static double sigma_min(const T& A)
		{
			//Start from a random x as guess for the smallest singular vector
			Eigen::VectorXd x_min = Eigen::VectorXd::Random(A.rows());
			return NLCG(A, false, x_min);
		}
		template<typename T>
		static double sigma_max(const T& A)
		{
			//Start from a random x as guess for the largest singular vector
			Eigen::VectorXd x_max = Eigen::VectorXd::Random(A.rows());
			return NLCG(A, true, x_max);
		}
		template<typename T>
		static double sigma_min(const T& A, const Eigen::VectorXd& x_min)
		{
			//Start from initial guess for the singular vectors x_min.
			return NLCG(A, false, x_min);
		}
		template<typename T>
		static double sigma_max(const T& A, const Eigen::VectorXd& x_max)
		{
			//Start from initial guess for the singular vectors x_max.
			return NLCG(A, true, x_max);
		}
	private:
		template<typename T>
		static double NLCG(const T& A, bool maximize, Eigen::VectorXd x)
		{
			/*
				Use Non-Linear Conjugate Gradient to find maxima/minima of f.
			*/
			using namespace Eigen;

			VectorXd d, d_old, r, r_old, x_old, grad_f, grad_f_old;
			double f_val, f_old, f_extreme, alpha, beta, alpha_max = 20;
			std::pair<double, double> alpha_f;
			boost::uintmax_t it = 10;
			double tol = 1e-6;
			//int bits = std::numeric_limits<double>::digits;
			int bits = -std::log2(tol);
			uint64_t i = 0;

			f_val = f(A, x);
			f_extreme = f_val;

			if (maximize)
			{
				//Take steps up the gradient
				r = gradf(A, x, f_val);
			}
			else
			{
				//Take steps down the gradient
				r = -gradf(A, x, f_val);
			}

			//The first search direction is in the direction of the gradient
			d = r;

			//Define the objective functions for the line search
			auto f_line_min = [&](double alpha)
			{
				//Function to minimize f
				return f(A, x + alpha * d);
			};
			auto f_line_max = [&](double alpha)
			{
				//Function to maximize f
				return -f(A, x + alpha * d);
			};

			if (maximize)
			{
				//Find the alpha that maximizes f(x+alpha*d)
				alpha_f = boost::math::tools::brent_find_minima(f_line_max, 0.0, alpha_max, bits, it);
				//Negate the extra - needed to use the minimize function for maximizing.
				alpha_f.second = -alpha_f.second;
			}
			else
			{
				//Find the alpha that minimizes f(x+alpha*d)
				alpha_f = boost::math::tools::brent_find_minima(f_line_min, 0.0, alpha_max, bits, it);
			}

			//Find the alpha that minimizes f(x+alpha*d)
			alpha = alpha_f.first;

			if (alpha == alpha_max)
			{
				//Increase the bound on alpha if it was too small
				alpha_max = alpha_max * 2;
			}

			f_val = alpha_f.second;

			while (true)
			{
				x_old = x;
				x = x + alpha * d;
				r_old = r;

				if (maximize)
				{
					//Take steps up the gradient
					r = gradf(A, x, f_val);
				}
				else
				{
					//Take steps down the gradient
					r = -gradf(A, x, f_val);
				}

				//Fletcher-Reeves beta:
				//beta = r.squaredNorm() / r_old.squaredNorm();
				//Polak-Ribiere beta:
				beta = std::max(r.dot(r - r_old) / r_old.squaredNorm(), 0.0);

				//Update the search direction
				d = r + beta * d;

				if (maximize)
				{
					//Find the alpha that maximizes f(x+alpha*d)
					alpha_f = boost::math::tools::brent_find_minima(f_line_max, 0.0, alpha_max, bits, it);
					//Negate the extra - needed to use the minimize function for maximizing.
					alpha_f.second = -alpha_f.second;
				}
				else
				{
					//Find the alpha that minimizes f(x+alpha*d)
					alpha_f = boost::math::tools::brent_find_minima(f_line_min, 0.0, alpha_max, bits, it);
				}

				alpha = alpha_f.first;

				if (alpha == alpha_max)
				{
					//Increase the bound on alpha if it was too small
					alpha_max = alpha_max * 2;
				}

				f_old = f_val;
				f_val = alpha_f.second;

				//Check for convergence or breakdown
				//std::cout << "i: " << i << "\t f: " << f_val << "\t alpha: " << alpha << "\t d.norm()" << d.norm() << std::endl;

				if (std::isfinite(alpha) == false)
				{
					break;
				}

				if (maximize == false && f_val < f_extreme)
				{
					//Keep track of the smallest value found thus far
					f_extreme = f_val;
				}
				else if (maximize == true && f_val > f_extreme)
				{
					//Keep track of the largest value found thus far
					f_extreme = f_val;
				}


				if (i > 0 && (((alpha * r).lpNorm<Infinity>() < tol * x.lpNorm<Infinity>())
						|| (std::abs(f_old - f_val) <= tol * std::abs(f_val))
						|| ((x_old - x).lpNorm<Infinity>() < tol * x.lpNorm<Infinity>())) )
				{
					break;
				}

				++i;
			}

			return f_extreme;


		}
		template<typename T>
		static Eigen::VectorXd gradf(const T& A, const Eigen::VectorXd& x, const double f_x)
		{
			using namespace Eigen;
			int N = x.rows();
			VectorXd grad(N);

			// for (int i = 0; i < N; ++i)
			// {
			// 	//Finite difference approximation
			// 	double dx = x(i) * std::sqrt(std::numeric_limits<double>::epsilon());
			// 	grad(i) = (f(A, x + VectorXd::Unit(N, i) * dx) - f_x) / dx;
			// }

			VectorXd Ax = A * x;
			double Ax_norm = Ax.norm();
			double x_norm = x.norm();
			double x_norm3 = x_norm * x_norm * x_norm;

			// for (int i = 0; i < N; ++i)
			// {
			// 	//Exact expression, O(N) less matvecs (Assuming 2-norm).
			// 	//No evaluation of f_x needed.
			// 	grad(i) = Ax.dot(A.col(i)) / (Ax_norm * x_norm) - Ax_norm * x(i) / x_norm3;
			// }
			//Exact expression in terms of Matrix-Vector products (Assuming 2-norm).
			grad = (A.transpose() * Ax) / (Ax_norm * x_norm) - Ax_norm * x / x_norm3;

			return grad;
		}
		template<typename T>
		static double f(const T& A, const Eigen::VectorXd& x)
		{
			return (A * x).norm() / x.norm();
		}
};
class CondestGD
{
		/*
		        Estimate the condition number, largest singular value, smallest singular value
		        using Gradient Descent.
		*/
	public:
		template<typename T>
		static double condest(const T& A)
		{
			//Start from a random vector x as initial guess for the singular vectors
			Eigen::VectorXd x = Eigen::VectorXd::Random(A.rows());
			double sigma_min = GD(A, false, x);
			x = Eigen::VectorXd::Random(A.rows());
			double sigma_max = GD(A, true, x);
			return sigma_max / sigma_min;
		}
		template<typename T>
		static double condest(const T& A, Eigen::VectorXd x_min, Eigen::VectorXd x_max)
		{
			//Start from initial guess for the singular vectors x_min and x_max.
			double sigma_min = GD(A, false, x_min);
			double sigma_max = GD(A, true, x_max);
			return sigma_max / sigma_min;
		}
		template<typename T>
		static double sigma_min(const T& A)
		{
			//Start from a random x as guess for the smallest singular vector
			Eigen::VectorXd x = Eigen::VectorXd::Random(A.rows());
			return GD(A, false, x);
		}
		template<typename T>
		static double sigma_max(const T& A)
		{
			//Start from a random x as guess for the largest singular vector
			Eigen::VectorXd x = Eigen::VectorXd::Random(A.rows());
			return GD(A, true, x);
		}
		template<typename T>
		static double sigma_min(const T& A, const Eigen::VectorXd& x_min)
		{
			//Start from initial guess for the singular vectors x_min.
			return GD(A, false, x_min);
		}
		template<typename T>
		static double sigma_max(const T& A, const Eigen::VectorXd& x_max)
		{
			//Start from initial guess for the singular vectors x_max.
			return GD(A, true, x_max);
		}
	private:
		template<typename T>
		static double GD(const T& A, bool maximize, Eigen::VectorXd x)
		{
			using namespace Eigen;
			/*
			        Apply gradient descent to |Ax|/|x|
			        if maxmimize, apply gradient ascent to find a maximum instead.
			*/
			double tol = 1e-6;

			double f_x, f_xold, gamma;
			VectorXd gradf_old, x_old, grad_f;

			double f_x_extreme = f(A, x);
			uint64_t i = 0;
			f_x = f_x_extreme;

			while (true)
			{
				grad_f = gradf(A, x, f_x);

				if (maximize)
				{
					//Taking steps up the gradient instead of down
					grad_f = -grad_f;
				}

				if (i == 0)
				{
					gamma = 0.1;
				}
				else
				{
					//Barzilai–Borwein choice for gamma.
					gamma = (x - x_old).dot(grad_f - gradf_old) / (grad_f - gradf_old).squaredNorm();
					gamma = 0.9 * std::abs(gamma); //0.9 Safety factor seems to smooth the convergence.
				}

				//std::cout << "i: " << i << "\t f: " << f_x << "\t gamma: " << gamma << std::endl;

				//Check for convergence or breakdown
				if (std::isfinite(gamma) == false)
				{
					break;
				}

				if (i > 0 && (((gamma * grad_f).lpNorm<Infinity>() < tol * x.lpNorm<Infinity>())
						|| (std::abs(f_xold - f_x) <= tol * std::abs(f_x))
						|| ((x_old - x).lpNorm<Infinity>() < tol * x.lpNorm<Infinity>())) )
					//if (gamma < 1e-6)
				{
					break;
				}

				x_old = x;
				x = x - gamma * grad_f;
				f_xold = f_x;
				gradf_old = grad_f;
				//f is not strictly needed, but it helps in determining convergence
				//and reduce the number of iterations needed.
				f_x = f(A, x);

				if (maximize && f_x > f_x_extreme)
				{
					//Keep track of the largest value found thus far
					f_x_extreme = f_x;
				}
				else if (maximize == false && f_x < f_x_extreme)
				{
					//Keep track of the smallest value found thus far
					f_x_extreme = f_x;
				}

				++i;


			}

			return f_x_extreme;

		}
		template<typename T>
		static Eigen::VectorXd gradf(const T& A, const Eigen::VectorXd& x, const double f_x)
		{
			using namespace Eigen;
			int N = x.rows();
			VectorXd grad(N);

			// for (int i = 0; i < N; ++i)
			// {
			// 	//Finite difference approximation
			// 	double dx = x(i) * std::sqrt(std::numeric_limits<double>::epsilon());
			// 	grad(i) = (f(A, x + VectorXd::Unit(N, i) * dx) - f_x) / dx;
			// }

			VectorXd Ax = A * x;
			double Ax_norm = Ax.norm();
			double x_norm = x.norm();
			double x_norm3 = x_norm * x_norm * x_norm;

			// for (int i = 0; i < N; ++i)
			// {
			// 	//Exact expression, O(N) less matvecs (Assuming 2-norm).
			// 	//No evaluation of f_x needed.
			// 	grad(i) = Ax.dot(A.col(i)) / (Ax_norm * x_norm) - Ax_norm * x(i) / x_norm3;
			// }
			//Exact expression in terms of Matrix-Vector products (Assuming 2-norm).
			grad = (A.transpose() * Ax) / (Ax_norm * x_norm) - Ax_norm * x / x_norm3;

			return grad;
		}
		template<typename T>
		static double f(const T& A, const Eigen::VectorXd& x)
		{
			return (A * x).norm() / x.norm();
		}
};
class CondestRNG
{
		//TODO: Check if this can be put inside LSQR, since that's the only one using it
	private:
		boost::mt19937 m_randomGenerator;
		boost::normal_distribution<> m_distribution;
		boost::variate_generator<boost::mt19937, boost::normal_distribution<> > m_variateGenerator = boost::variate_generator<boost::mt19937, boost::normal_distribution<> >(m_randomGenerator, m_distribution);
		//https://stackoverflow.com/questions/11490988/c-compile-time-error-expected-identifier-before-numeric-constant
		//https://stackoverflow.com/questions/39183275/c-oop-how-to-create-a-random-number-generator-object/39183450
	public:
		CondestRNG()
		{

			std::random_device randomDevice;

			if (randomDevice.entropy() == 0)
			{
				//random device may or may not be usable, if it isn't seed with current time instead
				//https://stackoverflow.com/questions/19555121/how-to-get-current-timestamp-in-milliseconds-since-1970-just-the-way-java-gets
				uint64_t m_seed = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch()).count();
				m_randomGenerator = boost::mt19937(m_seed);
			}
			else
			{
				m_randomGenerator = boost::mt19937(randomDevice());
			}

			m_distribution = boost::normal_distribution<>(0.0, 1.0);
			m_variateGenerator = boost::variate_generator<boost::mt19937, boost::normal_distribution<>>(m_randomGenerator, m_distribution);
		}
		double randn()
		{
			return m_variateGenerator();
		};
		Eigen::VectorXd randn(uint64_t n)
		{
			Eigen::VectorXd random_vector(n);

			for (uint64_t i = 0; i < n; ++i)
			{
				random_vector(i) = randn();
			}

			return random_vector;
		}
};
class CondestLSQR
{
		/*
		        Estimates the condition number using the LSQR-like approach of
		        Spectral condition-number estimation of larges parse matrices
		        Haim Avron, Alex Druinsky, Sivan Toledo
		        DOI: 10.1002/nla.2235
		*/
	public:
		template<typename M>
		static double condest(const M& A)
		{
			//Start without initial guess
			Eigen::VectorXd v_hat_min;
			Eigen::VectorXd v_hat_max;
			double sigma_hat_min;
			double sigma_hat_max;
			return condest(A, v_hat_min, v_hat_max, sigma_hat_min, sigma_hat_max);
		}
		template<typename M>
		static double condest(const M& A, Eigen::VectorXd& v_hat_min, Eigen::VectorXd& v_hat_max, double& sigma_hat_min, double& sigma_hat_max)
		{
			//Start with some initial guess
			using namespace Eigen;
			uint64_t m = A.rows();
			uint64_t n = A.cols();
			//Parameters
			double c1 = 8 * std::numeric_limits<double>::epsilon();
			double c2 = 1e-3;
			double c3 = 1 / (64 * std::numeric_limits<double>::epsilon());
			double c4 = std::sqrt(std::numeric_limits<double>::epsilon());
			double c1t = 4 * std::numeric_limits<double>::epsilon();
			uint64_t maxit = 1000;

			double sigma_max;
			power_rect_explicit(A, v_hat_max, sigma_max);

			sigma_hat_max = sigma_max;
			sigma_hat_min = sigma_hat_max;

			//6
			CondestRNG RNG;
			VectorXd x_hat = RNG.randn(n);

			//7
			double tau = std::sqrt(2) * boost::math::erf_inv(c2) / x_hat.norm();

			//8
			VectorXd x_star = x_hat / x_hat.norm();

			//9
			VectorXd b = A * x_star;

			//10
			double beta = b.norm();
			VectorXd u = b / beta;

			//11
			VectorXd v = A * u;
			double alpha = v.norm();
			v = v / alpha;

			//12
			VectorXd w = v;

			//13
			VectorXd x = VectorXd::Zero(n);

			//14
			double phi_bar = beta;
			double rho_bar = alpha;

			//15
			uint64_t T = maxit;
			uint64_t t = 1;

			//16, note that T is modified inside the loop and should not be a for loop for that reason.

			double rho, c, s, theta, phi;
			VectorXd d;//, v_hat_min;
			std::vector<double> Rtt;
			std::vector<double> Rtmint;

			while (t < T)
			{
				//17
				u = A * v - alpha * u;

				//18
				beta = u.norm();

				//19
				u = u / beta;

				//20
				v = A.transpose() * u - beta * v;

				//21
				alpha = v.norm();

				//22
				v = v / alpha;

				//23 has confusing notation
				//-> From the original reference of LSQR by Paige & Saunders this is inferred to mean
				//sqrt(rho_bar^2+beta^2)
				//||()|| makes make a vector of those two components, then take the norm
				rho = std::sqrt(rho_bar * rho_bar + beta * beta);

				//24
				c = rho_bar / rho;
				s = beta / rho;

				//25
				theta = s * alpha;
				rho_bar = -c * alpha;

				//26
				phi = c * phi_bar;
				phi_bar = s * phi_bar;

				//27
				x = x + (phi / rho) * w;

				//28
				w = v - (theta / rho) * w;

				//29
				Rtt.push_back(rho);

				//30
				if (t > 1)
				{
					Rtmint.push_back(theta);
				}

				//31
				d = x_star - x;

				//32
				if (d.norm() == 0)
				{
					//TODO: Should this be d.norm()<epsilon?
					sigma_hat_min = sigma_hat_max;
					v_hat_min = v_hat_max;
				}

				//33, 33 is a comment.
				//34
				if ((A * d).norm() < sigma_hat_min * d.norm())
				{
					//35
					sigma_hat_min = (A * d).norm() / d.norm();
					v_hat_min = d;
				}

				//37
				if (sigma_hat_min / sigma_hat_max <= c4)
				{
					c1 = c1t;
				}

				//38
				if (T == maxit && ((A * d).norm() / (sigma_hat_max * x.norm() + b.norm()) < c1 || d.norm() <= tau || sigma_hat_max / sigma_hat_min >= c3) )
				{
					T = std::ceil(1.25 * t);
				}


				t++;
			}//End while


			//42
			//Estimate sigma_tilde_min=sigma_min(R) using inverse power iteration
			//Adapted from the libskylark code.
			int N = Rtt.size(), izero = 0, ione = 1, info = 0;
			std::vector<double> workspace(4 * N);
			dbdsqr_(strdup("Upper"), &N, &izero, &izero, &izero, &Rtt[0], &Rtmint[0],
				nullptr, &ione, nullptr, &ione, nullptr, &ione, &workspace[0], &info);
			double sigma_tilde_min = Rtt[Rtt.size() - 1];

			//43
			sigma_tilde_min = std::min(sigma_tilde_min, sigma_hat_min);

			//44
			return sigma_hat_max / sigma_hat_min;
		}
	private:
		static uint64_t klein_lu_power_bound(uint64_t kl_n, double kl_epsilon, double kl_delta)
		{
			//Helper function that gives the number of iterations
			//required to estimate sigma_max using power iteration.
			return std::ceil( (1.0 / kl_epsilon) * (std::log(kl_n) + std::log(1.0 / (kl_epsilon * kl_delta))) );
		}
		template<typename T>
		static void power_rect_explicit(const T& A, Eigen::VectorXd& v_hat_max, double&  sigma_max)
		{
			//Power iteration to determine the largest singular value sigma_max and
			//the corresponding vector v_hat_max.
			using namespace Eigen;
			uint64_t m = A.rows();
			uint64_t n = A.cols();
			CondestRNG RNG;
			VectorXd vv = RNG.randn(n);
			vv = vv / vv.norm();
			uint64_t itcount = klein_lu_power_bound(std::min(m, n), 0.1, 1e-12);

			for (uint64_t k = 0; k < itcount; k++)
			{
				v_hat_max = vv;
				VectorXd uu = A * vv;
				vv = A.adjoint() * uu;
				sigma_max = vv.norm();
				vv = vv / sigma_max;
			}

			v_hat_max = vv;
			sigma_max = (A * vv).norm() / vv.norm();
		}
};