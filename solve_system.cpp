#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <unsupported/Eigen/IterativeSolvers>
#include <Eigen/SparseLU>
#include "IDRStab.h"
#include "BiCGSTABL.h"
#include <vector>
#include <string>
#include <iostream>
#include <chrono>
#include <type_traits>
#include <cmath>
#include <limits>
#include "discretizer.hpp"
#include "input_struct.hpp"
#include <fstream>
#include "mf.hpp"
#include "result.hpp"

#include <amgcl/adapter/eigen.hpp>
#include <amgcl/backend/builtin.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/solver/bicgstabl.hpp>
#include <amgcl/solver/idrs.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/profiler.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/adapter/eigen.hpp>
#include <amgcl/backend/eigen.hpp>
#include "cxxopts.hpp"
#include <omp.h>

#include <unsupported/Eigen/SparseExtra>
#include <complex>

//#define PRECONDITIONER Eigen::IncompleteLUT<double>
//#define PRECONDITIONER Eigen::DiagonalPreconditioner<double>
#define PRECONDITIONER Eigen::IdentityPreconditioner

//https://stackoverflow.com/questions/2795023/c-template-typedef
template<typename T>
using SparseMatrixType = Eigen::SparseMatrix<T, Eigen::RowMajor>;
template<typename T>
using  VectorType = Eigen::Matrix<T, Eigen::Dynamic, 1>;

template<typename T, typename U>
void solve_system_eigen(Result& result, T& solver, const SparseMatrixType<U>& A, const VectorType<U>& b,
	double max_iterations, double tolerance, int S = -1, int L = -1)
{
	using namespace std::chrono;

	//Start the timer
	high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

	//Set up the solver
	solver.compute(A);
	solver.setTolerance(tolerance);
	solver.setMaxIterations(max_iterations);

	//Set IDRStab/BiCGSTABL parameters
	if constexpr(std::is_same_v<T, Eigen::IDRStab<SparseMatrixType<U>>>)
	{
		solver.setS(S);
		solver.setL(L);
	}

	if constexpr(std::is_same_v<T, Eigen::BiCGSTABL<SparseMatrixType<U>>>)
	{
		solver.setL(L);
	}

	//Solve the system
	VectorType<U> x = solver.solve(b);

	//Stop the timer
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	//Store the solver performance in the struct
	result.set_post_info((b - A * x).norm() / b.norm(), solver.error(), solver.iterations(),
		solver.info(), time_span.count());
}

template<typename T, typename B, typename U>
void solve_system_amgcl(Result& result, const SparseMatrixType<U>& A,
	const VectorType<U>& b,
	double max_iterations, double tolerance)
{
	//Definitions
	using namespace std::chrono;
	int iterations;
	double true_residual;
	double estimated_residual;

	//Set up the solver parameters
	typename T::params prm;
	prm.solver.tol = tolerance;
	prm.solver.maxiter = max_iterations;

	//Initial guess & result vector
	VectorType<U> x_eigen = VectorType<U>::Zero(A.rows());
	std::vector<U> x_amg = std::vector<U>(x_eigen.data(), x_eigen.data() + x_eigen.size());

	//Start the timer
	high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

	try
	{
		//Is this amgcl with amgcl backend?
		if constexpr(std::is_same_v<B, amgcl::backend::builtin<U>>)
		{
			//Convert Eigen's matrix format to one compatible with amgcl's backend
			size_t n = A.rows();
			const int* ptr = A.outerIndexPtr();
			const int* col = A.innerIndexPtr();
			const U* val = A.valuePtr();
			amgcl::backend::crs<U> A_amgcl(std::make_tuple(n,
					amgcl::make_iterator_range(ptr, ptr + n + 1),
					amgcl::make_iterator_range(col, col + ptr[n]),
					amgcl::make_iterator_range(val, val + ptr[n])));

			//Set up the solver
			T solve(A_amgcl, prm);

			//Solve the system for the given RHS:
			std::tie(iterations, estimated_residual) = solve(A_amgcl, b, x_amg);

			//Convert the result back into Eigen's format to compute the true residual.
			VectorType<U> x_result = Eigen::Map<VectorType<U>, Eigen::Unaligned>(x_amg.data(),
					x_amg.size());
			true_residual = (b - A * VectorType<U>(x_result)).norm() / b.norm();
		}
		//Is this amgcl with Eigen as backend?
		else if constexpr(std::is_same_v<B, amgcl::backend::eigen<U>>)
		{
			//Solve the system
			T solve(A, prm);
			std::tie(iterations, estimated_residual) = solve(A, b, x_eigen);

			//Calculate the true residual.
			true_residual = (b - A * x_eigen).norm() / b.norm();
		}
	}
	catch (const std::exception& ex)
	{
		std::cerr << "Exception in amgcl solving: " << ex.what() << std::endl;
		true_residual = std::numeric_limits<double>::quiet_NaN();
		estimated_residual = std::numeric_limits<double>::quiet_NaN();
		iterations = max_iterations;
	}
	catch (...)
	{
		std::cerr << "Unknown exception in amgcl solving" << std::endl;
		true_residual = std::numeric_limits<double>::quiet_NaN();
		estimated_residual = std::numeric_limits<double>::quiet_NaN();
		iterations = max_iterations;
	}

	//Stop the timer
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	//Store the solver performance in the struct
	result.set_post_info(true_residual, estimated_residual, iterations, -1, time_span.count());
}
template<typename U>
void run_solvers(const SparseMatrixType<U>& A, const VectorType<U>& b)
{
	double u = 0;
	double beta = 0;
	double e = 0;
	int N = 1;
	std::string scheme = "";
	double tolerance = 1e-12;
	int max_iterations = 2000;
	using namespace std::chrono;
	std::vector<int> L_vec{1, 2, 4};
	std::vector<int> S_vec{1, 2, 4};

	int64_t timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	{
		Result result;
		result.set_pre_info("amgcl", u, beta, e, -1, -1, timestamp, scheme, tolerance, N, max_iterations);
		// Setup the amgcl solver:
		typedef amgcl::backend::builtin<U> backend;
		typedef amgcl::make_solver <
		amgcl::amg <
		backend,
		amgcl::coarsening::smoothed_aggregation,
		amgcl::relaxation::spai0 >,
		amgcl::solver::bicgstab<backend >>
		amgcl_solver;
		solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
		std::cout << result;
	}
	// {
	// 	Result result;
	// 	result.set_pre_info("amgcl_bicgstabl_solver", u, beta, e, -1, -1, timestamp, scheme, tolerance, N,
	// 		max_iterations);
	// 	// Setup the amgcl solver:
	// 	typedef amgcl::backend::builtin<U> backend;
	// 	typedef amgcl::make_solver <
	// 	amgcl::amg <
	// 	backend,
	// 	amgcl::coarsening::smoothed_aggregation,
	// 	amgcl::relaxation::spai0 >,
	// 	amgcl::solver::bicgstabl<backend >>
	// 	amgcl_solver;
	// 	solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
	// 	std::cout << result;
	// }
	// {
	// 	Result result;
	// 	result.set_pre_info("amgcl_idrs_solver", u, beta, e, -1, -1, timestamp, scheme, tolerance, N,
	// 		max_iterations);
	// 	// Setup the amgcl solver:
	// 	typedef amgcl::backend::builtin<U> backend;
	// 	typedef amgcl::make_solver <
	// 	amgcl::amg <
	// 	backend,
	// 	amgcl::coarsening::smoothed_aggregation,
	// 	amgcl::relaxation::spai0 >,
	// 	amgcl::solver::idrs<backend >>
	// 	amgcl_solver;
	// 	solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
	// 	std::cout << result;
	// }
	// {
	// 	Result result;
	// 	result.set_pre_info("amgcl_eigen_bicgstab_solver", u, beta, e, -1, -1, timestamp, scheme, tolerance,
	// 		N, max_iterations);
	// 	// Setup the amgcl solver:
	// 	typedef amgcl::backend::eigen<U> backend;
	// 	typedef amgcl::make_solver <
	// 	amgcl::amg <
	// 	backend,
	// 	amgcl::coarsening::smoothed_aggregation,
	// 	amgcl::relaxation::spai0 >,
	// 	amgcl::solver::bicgstab<backend >>
	// 	amgcl_solver;
	// 	solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
	// 	std::cout << result;
	// }
	{
		for (auto L : L_vec)
		{
			for (auto S : S_vec)
			{
				Result result;
				result.set_pre_info("IDRStab", u, beta, e, S, L, timestamp, scheme, tolerance, N, max_iterations);
				Eigen::IDRStab<SparseMatrixType<U>, PRECONDITIONER> solver;
				solve_system_eigen(result, solver, A, b, max_iterations, tolerance, S, L);
				std::cout << result;
			}
		}

		for (auto L : L_vec)
		{
			Result result;
			result.set_pre_info("BiCGSTABL", u, beta, e, 1, L, timestamp, scheme, tolerance, N,
				max_iterations);
			Eigen::BiCGSTABL<SparseMatrixType<U>, PRECONDITIONER> solver;
			solve_system_eigen(result, solver, A, b, max_iterations, tolerance, 1, L);
			std::cout << result;
		}

		Result result;
		result.set_pre_info("BiCGSTAB", u, beta, e, 1, 1, timestamp, scheme, tolerance, N,
			max_iterations);
		Eigen::BiCGSTAB<SparseMatrixType<U>, PRECONDITIONER> solver_bicgstab;
		solve_system_eigen(result, solver_bicgstab, A, b, max_iterations, tolerance, 1, 1);
		std::cout << result;
	}
}


int main(int argc, char** argv)
{
	SparseMatrixType<std::complex<double>> A;
	VectorType<std::complex<double>> b;
	//SparseMatrixType<std::complex<double>> b;
	Eigen::loadMarket(A, "system_matrix_CTLN.mtx");
	Eigen::loadMarketVector(b, "rhs_CTLN.mtx");
	//loadMarket does not support reading in complex "dense matrices"
	//VectorType<std::complex<double>> b(b1);
	run_solvers(A, b);
}