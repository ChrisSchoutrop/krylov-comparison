clear && echo -en "\e[3J"
astyle  --style=allman  --style=break --indent=tab --indent=force-tab=8 --indent-classes --indent-switches --indent-cases --indent-namespaces --indent-after-parens --indent-preproc-define --indent-preproc-cond --indent-col1-comments --pad-oper --pad-comma --pad-header  --align-pointer=type  --align-reference=type --break-one-line-headers --add-braces  --attach-return-type --remove-comment-prefix --mode=c --suffix=none --preserve-date --verbose --lineend=linux --max-code-length=150  $(pwd)/*.cpp,*.h,*.hpp
#--break-blocks
cppcheck --enable=all eigen_bicgstab.cpp --report-progress
cppcheck --enable=all --language=c++ idrstab.h --report-progress
rm acos_2019.o

#Version with somewhat normal optimization flags
g++ acos_2019.cpp -std=c++1z -I$(pwd)/usr/include/eigen3 -I$(pwd)/eigen-eigen-323c052e1731 -O3 -g -fopenmp -march=native -Wall -o acos_2019.o

echo "Done compiling"

./acos_2019.o 2>&1 | tee results/acos_2019_bicgstab_fix.txt
