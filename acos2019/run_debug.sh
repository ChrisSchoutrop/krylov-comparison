clear && echo -en "\e[3J"
astyle  --style=allman  --style=break --indent=tab --indent=force-tab=8 --indent-classes --indent-switches --indent-cases --indent-namespaces --indent-after-parens --indent-preproc-define --indent-preproc-cond --indent-col1-comments --pad-oper --pad-comma --pad-header  --align-pointer=type  --align-reference=type --break-one-line-headers --add-braces  --attach-return-type --remove-comment-prefix --mode=c --suffix=none --preserve-date --verbose --lineend=linux --max-code-length=150  $(pwd)/*.cpp,*.h,*.hpp
#--break-blocks
cppcheck --enable=all eigen_bicgstab.cpp --report-progress
cppcheck --enable=all --language=c++ idrstab.h --report-progress
rm bicgstabell_test

#Debug symbols, no optimize
#g++ eigen_bicgstab.cpp -std=c++1z  -I$(pwd)/eigen-eigen-323c052e1731 -g -Wall -Og -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ALGORITHM=3 -D DEBUG=0 -D ELL=4 -D MAJORITY=Eigen::RowMajor -o bicgstabell_test
#-I/usr/include/eigen3

#Debug symbols
#g++ -std=c++1z  -I$(pwd)/eigen-eigen-323c052e1731 -g -Wall -Og -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ALGORITHM=3 -D DEBUG=0 -D ELL=4 -D MAJORITY=Eigen::RowMajor -o bicgstabell_test eigen_bicgstab.cpp

#google perf https://github.com/gperftools/gperftools
#g++ -std=c++1z  -I$(pwd)/eigen-eigen-323c052e1731 -g -Wall -Og -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D ALGORITHM=3 -D DEBUG=0 -D ELL=4 -D MAJORITY=Eigen::RowMajor -o bicgstabell_test eigen_bicgstab.cpp  -lprofiler

#Version with a ton of speculative optimizations
#g++ eigen_bicgstab.cpp -std=c++1z -I$(pwd)/eigen-eigen-323c052e1731 -Ofast -fopenmp -fomit-frame-pointer -fopenmp -march=native -fgcse-las -fgcse-sm -faggressive-loop-optimizations -fdevirtualize-speculatively -fdevirtualize-at-ltrans -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ALGORITHM=3 -D DEBUG=0 -D ELL=4 -D MAJORITY=Eigen::RowMajor -o bicgstabell_test

#Version with somewhat normal optimization flags
g++ eigen_bicgstab.cpp -std=c++1z -I$(pwd)/eigen-eigen-323c052e1731 -O3 -fopenmp -march=native -fgcse-las -fgcse-sm -faggressive-loop-optimizations -fdevirtualize-speculatively -fdevirtualize-at-ltrans -D PRECONDITIONER="Eigen::AMG<T>" -D ALGORITHM=3 -D DEBUG=0 -D ELL=2 -D MAJORITY=Eigen::RowMajor -o bicgstabell_test

#Version for uftrace (doesnt work with openmp)
#g++ -std=c++1z  -I$(pwd)/eigen-eigen-323c052e1731 -pg -Wall -O3 -fgcse-las -fgcse-sm -faggressive-loop-optimizations -fdevirtualize-speculatively -fdevirtualize-at-ltrans -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D ALGORITHM=3 -D DEBUG=0 -D ELL=4 -D MAJORITY=Eigen::RowMajor -o bicgstabell_test eigen_bicgstab.cpp

echo "Done compiling"
#time ./bicgstabell_test debug_matrix/input_A.txt debug_matrix/input_b.txt debug_matrix/output.txt 0 12
#time ./bicgstabell_test A_burgers.txt b_burgers.txt output.txt 0 12
#time ./bicgstabell_test kensuke_A.txt kensuke_b.txt output.txt 0 12
#time ./bicgstabell_test input_A_jesper.txt input_b_jesper.txt output.txt 0
#time ./bicgstabell_test input_A_idrstabfails.txt input_b_idrstabfails.txt output.txt 0
time ./bicgstabell_test input_A_difficult.txt input_b_difficult.txt output.txt 0
#time ./bicgstabell_test input_A_stoichio.txt input_b_stoichio.txt output.txt 0
#time ./bicgstabell_test input_A_lex.txt input_b_lex.txt output.txt 0
#time ./bicgstabell_test input_A_wopmas.txt input_b_wopmas.txt output.txt 0
#time ./bicgstabell_test asml_A_scaled.txt asml_b_scaled.txt asml_scaled_output_bicgstab.txt 0
#time ./bicgstabell_test idrstab_fails/input_A.txt idrstab_fails/input_b.txt o.txt 0
#time ./bicgstabell_test idrstab_fails_20_jan_2020/input_A.txt idrstab_fails_20_jan_2020/input_b.txt o.txt 0


#uftrace record ./bicgstabell_test input_A_jesper.txt input_b_jesper.txt output.txt 0

#uftrace graph Eigen::internal::idrstab
#uftrace report
