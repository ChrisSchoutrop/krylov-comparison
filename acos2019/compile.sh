astyle  --style=allman  --style=break --indent=tab --indent=force-tab=8 --indent-classes --indent-switches --indent-cases --indent-namespaces --indent-after-parens --indent-preproc-define --indent-preproc-cond --indent-col1-comments --break-blocks --pad-oper --pad-comma --pad-header  --align-pointer=type  --align-reference=type --break-one-line-headers --add-braces  --attach-return-type --remove-comment-prefix --mode=c --suffix=none --preserve-date --verbose --lineend=linux --max-code-length=100  $(pwd)/*.cpp,*.h

cppcheck --enable=all eigen_bicgstab.cpp

if [ "$1" = "identity" ]
then
	echo "Compiling benchmarks for Identity preconditioner"
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=1 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D MAJORITY=Eigen::ColMajor -o bicgstab_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=1 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D MAJORITY=Eigen::RowMajor -o bicgstab_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ELL=1 -D MAJORITY=Eigen::ColMajor -o bicgstabell1_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ELL=1 -D MAJORITY=Eigen::RowMajor -o bicgstabell1_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ELL=2 -D MAJORITY=Eigen::ColMajor -o bicgstabell2_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ELL=2 -D MAJORITY=Eigen::RowMajor -o bicgstabell2_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ELL=4 -D MAJORITY=Eigen::ColMajor -o bicgstabell4_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ELL=4 -D MAJORITY=Eigen::RowMajor -o bicgstabell4_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ELL=8 -D MAJORITY=Eigen::ColMajor -o bicgstabell8_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IdentityPreconditioner" -D ELL=8 -D MAJORITY=Eigen::RowMajor -o bicgstabell8_row &
	wait
elif [ "$1" = "diagonal" ]
then
	echo "Compiling benchmarks for Diagonal preconditioner"
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=1 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D MAJORITY=Eigen::ColMajor -o bicgstab_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=1 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D MAJORITY=Eigen::RowMajor -o bicgstab_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D ELL=1 -D MAJORITY=Eigen::ColMajor -o bicgstabell1_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D ELL=1 -D MAJORITY=Eigen::RowMajor -o bicgstabell1_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D ELL=2 -D MAJORITY=Eigen::ColMajor -o bicgstabell2_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D ELL=2 -D MAJORITY=Eigen::RowMajor -o bicgstabell2_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D ELL=4 -D MAJORITY=Eigen::ColMajor -o bicgstabell4_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D ELL=4 -D MAJORITY=Eigen::RowMajor -o bicgstabell4_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D ELL=8 -D MAJORITY=Eigen::ColMajor -o bicgstabell8_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::DiagonalPreconditioner<T>" -D ELL=8 -D MAJORITY=Eigen::RowMajor -o bicgstabell8_row &
	wait
elif [ "$1" = "ilut" ]
then
	echo "Compiling benchmarks for ILUT preconditioner"
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=1 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D MAJORITY=Eigen::ColMajor -o bicgstab_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=1 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D MAJORITY=Eigen::RowMajor -o bicgstab_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D ELL=1 -D MAJORITY=Eigen::ColMajor -o bicgstabell1_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D ELL=1 -D MAJORITY=Eigen::RowMajor -o bicgstabell1_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D ELL=2 -D MAJORITY=Eigen::ColMajor -o bicgstabell2_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D ELL=2 -D MAJORITY=Eigen::RowMajor -o bicgstabell2_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D ELL=4 -D MAJORITY=Eigen::ColMajor -o bicgstabell4_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D ELL=4 -D MAJORITY=Eigen::RowMajor -o bicgstabell4_row &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D ELL=8 -D MAJORITY=Eigen::ColMajor -o bicgstabell8_col &
	g++ eigen_bicgstab.cpp -I$(pwd)/eigen-eigen-323c052e1731 -std=c++1z -O3 -fopenmp -march=native -D ALGORITHM=2 -D PRECONDITIONER="Eigen::IncompleteLUT<T>" -D ELL=8 -D MAJORITY=Eigen::RowMajor -o bicgstabell8_row &
	wait
else
	echo "Something went from in compile.sh"
fi

