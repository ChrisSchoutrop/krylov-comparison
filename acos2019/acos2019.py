'''
Plotscript for the acos_2019 results
00 result.algorithm << "\t" <<
01 result.true_residual << "\t" <<
02 result.estimated_residual << "\t" <<
03 result.iterations << "\t" <<
04 result.return_flag << "\t" <<
05 result.u << "\t" <<
06 result.beta << "\t" <<
07 result.e << "\t" <<
08 result.time << "\t" <<
09 result.N << "\t" <<
10 result.max_iterations << "\t" <<
11 result.tolerance << "\t" <<
12 result.const_source << "\t" <<
13 result.boundary_value << "\t" <<
14 result.length << "\t" <<
15 result.S << "\t" <<
16 result.L << "\t" <<
17 result.timestamp << "\t" <<
'''

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import math
import scipy.interpolate


def read_file(filename):
    with open(filename) as f:
        reader = csv.reader(f, delimiter='\t')
        next(reader)  # skip header
        data = []
        #gooi elke sense van efficient uit het raam en gwn appenden ipv preallocate
        for row in reader:
            data.append(row)
    return data


plt.close()
font = {'family': 'normal',
        'weight': 'normal',
        'size': 15}
matplotlib.rc('font', **font)

suffices = ['_bicgstab']

for suffix in suffices:

	data = read_file("acos2020"+suffix+".txt")
	#Convert to a useful format
	data = np.array(data, dtype='object')

	#Convert the numerical data to numbers
	data[:, 1:-1] = data[:, 1:-1].astype(float)

	def mv_per_iter(S, L):
		#Table 1 from Kensuke
		return L*(S+1)+L+1

	def axpy_per_iter(S, L):
		#Table 1 from Kensuke
		return 0.5*L*S*(L+1)*(S+1)+1.5*L+S+0.5

	def filter_bicgstab(data):
		if str(data[0]) == "BiCGSTAB":
			return True
		else:
			return False

	def filter_idrstab(data, S, L):
		if (str(data[0]) == "IDRStab" and int(data[15]) == S and int(data[16]) == L):
			return True
		else:
			return False

	plot_idrstab_S1_L1 = np.empty([9, 0])
	plot_idrstab_S1_L2 = np.empty([9, 0])
	plot_idrstab_S4_L1 = np.empty([9, 0])
	plot_idrstab_S4_L2 = np.empty([9, 0])
	plot_bicgstab = np.empty([9, 0])
	for x in range(0, data.shape[0]):
		if data[x, 6] < 0:
			continue
		if data[x, 9] != 161:
			continue
		length = data[x, 14]
		h = data[x, 14]/(data[x, 9]-1)
		Pe = data[x, 5]*length/data[x, 7]
		Dad = data[x, 6]/(data[x, 7]*length)
		Dac = Dad/Pe
		Pe_grid = data[x, 5]*h/data[x, 7]
		Da_grid = data[x, 6]/(data[x, 7]*h)
		true_rel_res = data[x, 1]
		est_res_per_iter = data[x, 2]/(data[x, 3]+1)
		time = data[x,8]
		iterations = data[x, 3]+1
		matvecs=iterations
		new_data = np.array([[Pe], [Dad], [Pe_grid], [Da_grid], [true_rel_res], [
                    matvecs], [time*true_rel_res], [time], [iterations*true_rel_res]])
		if filter_idrstab(data[x, :], 1, 1):
			new_data[-1] = new_data[-1]*mv_per_iter(1, 1)
			new_data[5] = new_data[5]*mv_per_iter(1, 1)
			plot_idrstab_S1_L1 = np.concatenate(
                            (plot_idrstab_S1_L1, new_data), axis=1)
		if filter_idrstab(data[x, :], 1, 2):
			new_data[-1] = new_data[-1]*mv_per_iter(1, 2)
			new_data[5] = new_data[5]*mv_per_iter(1, 2)
			plot_idrstab_S1_L2 = np.concatenate(
                            (plot_idrstab_S1_L2, new_data), axis=1)
		if filter_idrstab(data[x, :], 4, 1):
			new_data[-1] = new_data[-1]*mv_per_iter(4, 1)
			new_data[5] = new_data[5]*mv_per_iter(4, 1)
			plot_idrstab_S4_L1 = np.concatenate(
                            (plot_idrstab_S4_L1, new_data), axis=1)
		if filter_idrstab(data[x, :], 4, 2):
			new_data[-1] = new_data[-1]*mv_per_iter(4, 2)
			new_data[5] = new_data[5]*mv_per_iter(4, 2)
			plot_idrstab_S4_L2 = np.concatenate(
                            (plot_idrstab_S4_L2, new_data), axis=1)
		if filter_bicgstab(data[x, :]):
			new_data[-1] = new_data[-1]*2  # 2MV per iter
			new_data[5] = new_data[5]*2
			plot_bicgstab = np.concatenate(
                            (plot_bicgstab, new_data), axis=1)

	def remove_bad(data, pv):
		#Remove nan
		data[pv, data[pv, :]
                    != data[pv, :]] = 1e16
		return data

	pv = -1
	#pv = -2
	#print(plot_idrstab_L2_S4)
	plot_idrstab_S1_L1 = remove_bad(plot_idrstab_S1_L1, pv)
	plot_idrstab_S1_L2 = remove_bad(plot_idrstab_S1_L2, pv)
	plot_idrstab_S4_L1 = remove_bad(plot_idrstab_S4_L1, pv)
	plot_idrstab_S4_L2 = remove_bad(plot_idrstab_S4_L2, pv)
	plot_bicgstab = remove_bad(plot_bicgstab, pv)
	#print(plot_idrstab_L2_S4)
	#np.set_printoptions(threshold=np.inf)
	print(data[pv,:])
	def plot_tricontour_restime(data, name, pv):
		f = plt.figure()
		ax = plt.gca()
		try:
			if False:
				pcm = plt.tricontourf(data[0, :], data[1, :], np.log10(
					data[pv, :]), cmap='jet', vmin=-16, vmax=3)
			else:
				x=data[0, :]
				y=data[1, :]
				N_interpolate=len(set(x))
				#z=np.log10(data[pv, :])
				z=data[pv, :]
				xmin=min(x)
				xmax=max(y)
				ymin=min(y[y[:] > 0])
				ymax=max(y)
				zmin=min(z)
				zmax=max(z)
				print('zmin: '+str(zmin)+' s zmax: '+str(zmax)+' s')
				xi=np.logspace(np.log10(xmin),np.log10(xmax),N_interpolate)
				yi=np.logspace(np.log10(ymin),np.log10(ymax),N_interpolate)
				xi,yi = np.meshgrid(xi,yi)
				zi = scipy.interpolate.griddata((x,y),z,(xi,yi),method='nearest',rescale=True)
				#pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=2e-3, vmax=5),edgecolors='none')
				#pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=6e-3, vmax=9),edgecolors='none')
				pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=1e-12, vmax=1),edgecolors='none')

			# pcm = plt.contourf(data[0, :], data[1, :], z, cmap='jet', vmin=-16, vmax=3)
			cbar = plt.colorbar(extend='both')
			cbar.ax.set_ylabel('$Wall time\\times\\|\\bf{b}-\\bf{A}\\bf{x}\\|/\\|\\bf{b}\\|$',usetex=True)
			plt.xlabel('$u/\\mathcal{E}$',usetex=True)
			plt.ylabel('$\\beta/\\mathcal{E}$',usetex=True)
			plt.xscale('log')
			plt.yscale('log')
			plt.xlim(min(data[0, :]), max(data[0, :]))
			plt.ylim(min(data[1, data[1, :] > 0]), max(
				data[1, :]))
			f.savefig(name+suffix+"_restime.pdf", bbox_inches='tight')
			f.savefig(name+suffix+"_restime.png", bbox_inches='tight')
		except Exception as e:
			print(e)
		return

	def plot_tricontour_time(data, name, pv):
		f = plt.figure()
		ax = plt.gca()
		try:
			if False:
				pcm = plt.tricontourf(data[0, :], data[1, :], np.log10(
					data[pv, :]), cmap='jet', vmin=-16, vmax=3)
			else:
				x=data[0, :]
				y=data[1, :]
				N_interpolate=len(set(x))
				#z=np.log10(data[pv, :])
				z=data[pv, :]
				xmin=min(x)
				xmax=max(y)
				ymin=min(y[y[:] > 0])
				ymax=max(y)
				zmin=min(z)
				zmax=max(z)
				print('zmin: '+str(zmin)+' s zmax: '+str(zmax)+' s')
				xi=np.logspace(np.log10(xmin),np.log10(xmax),N_interpolate)
				yi=np.logspace(np.log10(ymin),np.log10(ymax),N_interpolate)
				xi,yi = np.meshgrid(xi,yi)
				zi = scipy.interpolate.griddata((x,y),z,(xi,yi),method='nearest',rescale=True)
				#pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=2e-3, vmax=5),edgecolors='none')
				pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=6e-3, vmax=9),edgecolors='none')
				#pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=1e-12, vmax=1),edgecolors='none')

			# pcm = plt.contourf(data[0, :], data[1, :], z, cmap='jet', vmin=-16, vmax=3)
			cbar = plt.colorbar()
			cbar.ax.set_ylabel('Wall time (s)',usetex=True)
			#cbar.ax.set_ylabel('Matrix-Vector products',usetex=True)
			plt.xlabel('$u/\\mathcal{E}$',usetex=True)
			plt.ylabel('$\\beta/\\mathcal{E}$',usetex=True)
			plt.xscale('log')
			plt.yscale('log')
			plt.xlim(min(data[0, :]), max(data[0, :]))
			plt.ylim(min(data[1, data[1, :] > 0]), max(
				data[1, :]))
			f.savefig(name+suffix+"_time.pdf", bbox_inches='tight')
			f.savefig(name+suffix+"_time.png", bbox_inches='tight')
		except Exception as e:
			print(e)
		return

	def plot_tricontour_matvecs(data, name, pv):
		f = plt.figure()
		ax = plt.gca()
		try:
			if False:
				pcm = plt.tricontourf(data[0, :], data[1, :], np.log10(
					data[pv, :]), cmap='jet', vmin=-16, vmax=3)
			else:
				x=data[0, :]
				y=data[1, :]
				N_interpolate=len(set(x))
				#z=np.log10(data[pv, :])
				z=data[pv, :]
				xmin=min(x)
				xmax=max(y)
				ymin=min(y[y[:] > 0])
				ymax=max(y)
				zmin=min(z)
				zmax=max(z)
				print('zmin: '+str(zmin)+' s zmax: '+str(zmax)+' s')
				xi=np.logspace(np.log10(xmin),np.log10(xmax),N_interpolate)
				yi=np.logspace(np.log10(ymin),np.log10(ymax),N_interpolate)
				xi,yi = np.meshgrid(xi,yi)
				zi = scipy.interpolate.griddata((x,y),z,(xi,yi),method='nearest',rescale=True)
				#pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=2e-3, vmax=5),edgecolors='none')
				pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=1, vmax=1.3e4),edgecolors='none')
				#pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=1e-12, vmax=1),edgecolors='none')

			# pcm = plt.contourf(data[0, :], data[1, :], z, cmap='jet', vmin=-16, vmax=3)
			cbar = plt.colorbar()
			cbar.ax.set_ylabel('Matrix-Vector products',usetex=True)
			plt.xlabel('$u/\\mathcal{E}$',usetex=True)
			plt.ylabel('$\\beta/\\mathcal{E}$',usetex=True)
			plt.xscale('log')
			plt.yscale('log')
			plt.xlim(min(data[0, :]), max(data[0, :]))
			plt.ylim(min(data[1, data[1, :] > 0]), max(
				data[1, :]))
			f.savefig(name+suffix+"_matvecs.pdf", bbox_inches='tight')
			f.savefig(name+suffix+"_matvecs.png", bbox_inches='tight')
		except Exception as e:
			print(e)
		return

	def plot_tricontour_mvres(data, name, pv):
		f = plt.figure()
		ax = plt.gca()
		try:
			if False:
				pcm = plt.tricontourf(data[0, :], data[1, :], np.log10(
					data[pv, :]), cmap='jet', vmin=-16, vmax=3)
			else:
				x=data[0, :]
				y=data[1, :]
				N_interpolate=len(set(x))
				#z=np.log10(data[pv, :])
				z=data[pv, :]
				xmin=min(x)
				xmax=max(y)
				ymin=min(y[y[:] > 0])
				ymax=max(y)
				zmin=min(z)
				zmax=max(z)
				print('zmin: '+str(zmin)+' zmax: '+str(zmax))
				xi=np.logspace(np.log10(xmin),np.log10(xmax),N_interpolate)
				yi=np.logspace(np.log10(ymin),np.log10(ymax),N_interpolate)
				xi,yi = np.meshgrid(xi,yi)
				zi = scipy.interpolate.griddata((x,y),z,(xi,yi),method='nearest',rescale=True)
				pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=1e-16, vmax=1.3e4),edgecolors='none')

			# pcm = plt.contourf(data[0, :], data[1, :], z, cmap='jet', vmin=-16, vmax=3)
			cbar = plt.colorbar(extend='max')
			cbar.ax.set_ylabel('$MV*\\|\\bf{b}-\\bf{A}\\bf{x}\\|/\\|\\bf{b}\\|$',usetex=True)
			plt.xlabel('$u/\\mathcal{E}$',usetex=True)
			plt.ylabel('$\\beta/\\mathcal{E}$',usetex=True)
			plt.xscale('log')
			plt.yscale('log')
			plt.xlim(min(data[0, :]), max(data[0, :]))
			plt.ylim(min(data[1, data[1, :] > 0]), max(
				data[1, :]))
			f.savefig(name+suffix+"_mvres.pdf", bbox_inches='tight')
			f.savefig(name+suffix+"_mvres.png", bbox_inches='tight')
		except Exception as e:
			print(e)
		return

	def plot_tricontour_relres(data, name, pv):
		f = plt.figure()
		ax = plt.gca()
		try:
			if False:
				pcm = plt.tricontourf(data[0, :], data[1, :], np.log10(
					data[pv, :]), cmap='jet', vmin=-16, vmax=3)
			else:
				x=data[0, :]
				y=data[1, :]
				N_interpolate=len(set(x))
				#z=np.log10(data[pv, :])
				z=data[pv, :]
				xmin=min(x)
				xmax=max(y)
				ymin=min(y[y[:] > 0])
				ymax=max(y)
				zmin=min(z)
				zmax=max(z)
				print('zmin: '+str(zmin)+' zmax: '+str(zmax))
				xi=np.logspace(np.log10(xmin),np.log10(xmax),N_interpolate)
				yi=np.logspace(np.log10(ymin),np.log10(ymax),N_interpolate)
				xi,yi = np.meshgrid(xi,yi)
				zi = scipy.interpolate.griddata((x,y),z,(xi,yi),method='nearest',rescale=True)
				pcm=plt.pcolormesh(xi,yi,zi, cmap='jet',norm=matplotlib.colors.LogNorm(vmin=1e-12, vmax=1),edgecolors='none')

			# pcm = plt.contourf(data[0, :], data[1, :], z, cmap='jet', vmin=-16, vmax=3)
			cbar = plt.colorbar(extend='both')
			cbar.ax.set_ylabel('$\\|\\bf{b}-\\bf{A}\\bf{x}\\|/\\|\\bf{b}\\|$',usetex=True)
			plt.xlabel('$u/\\mathcal{E}$',usetex=True)
			plt.ylabel('$\\beta/\\mathcal{E}$',usetex=True)
			plt.xscale('log')
			plt.yscale('log')
			plt.xlim(min(data[0, :]), max(data[0, :]))
			plt.ylim(min(data[1, data[1, :] > 0]), max(
				data[1, :]))
			f.savefig(name+suffix+"_relres.pdf", bbox_inches='tight')
			f.savefig(name+suffix+"_relres.png", bbox_inches='tight')
		except Exception as e:
			print(e)
		return

	plot_tricontour_mvres(plot_idrstab_S1_L1, "idrstab_S1_L1", -1)
	plot_tricontour_mvres(plot_idrstab_S1_L2, "idrstab_S1_L2", -1)
	plot_tricontour_mvres(plot_idrstab_S4_L1, "idrstab_S4_L1", -1)
	plot_tricontour_mvres(plot_idrstab_S4_L2, "idrstab_S4_L2", -1)
	plot_tricontour_mvres(plot_bicgstab, "bicgstab", -1)

	plot_tricontour_time(plot_idrstab_S1_L1, "idrstab_S1_L1", -2)
	plot_tricontour_time(plot_idrstab_S1_L2, "idrstab_S1_L2", -2)
	plot_tricontour_time(plot_idrstab_S4_L1, "idrstab_S4_L1", -2)
	plot_tricontour_time(plot_idrstab_S4_L2, "idrstab_S4_L2", -2)
	plot_tricontour_time(plot_bicgstab, "bicgstab", -2)

	plot_tricontour_restime(plot_idrstab_S1_L1, "idrstab_S1_L1", -3)
	plot_tricontour_restime(plot_idrstab_S1_L2, "idrstab_S1_L2", -3)
	plot_tricontour_restime(plot_idrstab_S4_L1, "idrstab_S4_L1", -3)
	plot_tricontour_restime(plot_idrstab_S4_L2, "idrstab_S4_L2", -3)
	plot_tricontour_restime(plot_bicgstab, "bicgstab", -3)

	plot_tricontour_relres(plot_idrstab_S1_L1, "idrstab_S1_L1", 4)
	plot_tricontour_relres(plot_idrstab_S1_L2, "idrstab_S1_L2", 4)
	plot_tricontour_relres(plot_idrstab_S4_L1, "idrstab_S4_L1", 4)
	plot_tricontour_relres(plot_idrstab_S4_L2, "idrstab_S4_L2", 4)
	plot_tricontour_relres(plot_bicgstab, "bicgstab", 4)

	plot_tricontour_matvecs(plot_idrstab_S1_L1, "idrstab_S1_L1", 5)
	plot_tricontour_matvecs(plot_idrstab_S1_L2, "idrstab_S1_L2", 5)
	plot_tricontour_matvecs(plot_idrstab_S4_L1, "idrstab_S4_L1", 5)
	plot_tricontour_matvecs(plot_idrstab_S4_L2, "idrstab_S4_L2", 5)
	plot_tricontour_matvecs(plot_bicgstab, "bicgstab", 5)