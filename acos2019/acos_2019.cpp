#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseLU>
//#include "unsupported/Eigen/src/SparseExtra/MarketIO.h"
#include "idrstab.h"
#include <vector>
#include <string>
#include <iostream>
#include <chrono>
#include <type_traits>
#include <cmath>

//#define PRECONDITIONER Eigen::IncompleteLUT<double>
#define PRECONDITIONER Eigen::DiagonalPreconditioner<double>

typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SparseMatrixType;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorType;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> DenseMatrixTypeCol;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DenseMatrixTypeRow;

// const std::vector<double> u_vec   {1024.0, 512.0, 256.0, 128.0, 64.0, 32.0, 16.0, 8.0, 4.0, 2.0, 1.0, 1.0 / 2, 1.0 / 4, 1.0 / 8, 1.0 / 16, 1.0 / 32, 1.0 / 64, 1.0 / 128, 1.0 / 256, 1.0 / 512, 1.0 / 1024, 1.0 / 2048};
// const std::vector<double> beta_vec{-8192, -4096, -2048, -1024, -512, -256, -128, -64, -32, -16, -8, -4, -2, 0, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192};
// const std::vector<double> e_vec   {1024.0, 512.0, 256.0, 128.0, 64.0, 32.0, 16.0, 8.0, 4.0, 2.0, 1.0, 1.0 / 2, 1.0 / 4, 1.0 / 8, 1.0 / 16, 1.0 / 32, 1.0 / 64, 1.0 / 128, 1.0 / 256, 1.0 / 512, 1.0 / 1024, 1.0 / 2048, 1.0 / 4096, 1.0 / 8192};

// Logspaced numbers between 1e-6 and 1e+6
// 63x63
//const std::vector<double> u_vec {1e-06, 1.5615e-06, 2.4384e-06, 3.8075e-06, 5.9456e-06, 9.2841e-06, 1.4497e-05, 2.2638e-05, 3.535e-05, 5.52e-05, 8.6195e-05, 0.0001346, 0.00021017, 0.00032819, 0.00051248, 0.00080025, 0.0012496, 0.0019513, 0.003047, 0.0047579, 0.0074296, 0.011602, 0.018116, 0.028289, 0.044173, 0.068978, 0.10771, 0.16819, 0.26264, 0.41011, 0.6404, 1, 1.5615, 2.4384, 3.8075, 5.9456, 9.2841, 14.497, 22.638, 35.35, 55.2, 86.195, 134.6, 210.17, 328.19, 512.48, 800.25, 1249.6, 1951.3, 3047, 4757.9, 7429.6, 11602, 18116, 28289, 44173, 68978, 1.0771e+05, 1.6819e+05, 2.6264e+05, 4.1011e+05, 6.404e+05, 1e+06};
//const std::vector<double> beta_vec {1e-06, 1.5615e-06, 2.4384e-06, 3.8075e-06, 5.9456e-06, 9.2841e-06, 1.4497e-05, 2.2638e-05, 3.535e-05, 5.52e-05, 8.6195e-05, 0.0001346, 0.00021017, 0.00032819, 0.00051248, 0.00080025, 0.0012496, 0.0019513, 0.003047, 0.0047579, 0.0074296, 0.011602, 0.018116, 0.028289, 0.044173, 0.068978, 0.10771, 0.16819, 0.26264, 0.41011, 0.6404, 1, 1.5615, 2.4384, 3.8075, 5.9456, 9.2841, 14.497, 22.638, 35.35, 55.2, 86.195, 134.6, 210.17, 328.19, 512.48, 800.25, 1249.6, 1951.3, 3047, 4757.9, 7429.6, 11602, 18116, 28289, 44173, 68978, 1.0771e+05, 1.6819e+05, 2.6264e+05, 4.1011e+05, 6.404e+05, 1e+06};
//const std::vector<double> e_vec = {1};
// 31x31
// const std::vector<double> u_vec {1e-06, 2.5119e-06, 6.3096e-06, 1.5849e-05, 3.9811e-05, 0.0001, 0.00025119, 0.00063096, 0.0015849, 0.0039811, 0.01, 0.025119, 0.063096, 0.15849, 0.39811, 1, 2.5119, 6.3096, 15.849, 39.811, 100, 251.19, 630.96, 1584.9, 3981.1, 10000, 25119, 63096, 1.5849e+05, 3.9811e+05, 1e+06};
// const std::vector<double> beta_vec {1e-06, 2.5119e-06, 6.3096e-06, 1.5849e-05, 3.9811e-05, 0.0001, 0.00025119, 0.00063096, 0.0015849, 0.0039811, 0.01, 0.025119, 0.063096, 0.15849, 0.39811, 1, 2.5119, 6.3096, 15.849, 39.811, 100, 251.19, 630.96, 1584.9, 3981.1, 10000, 25119, 63096, 1.5849e+05, 3.9811e+05, 1e+06};
// const std::vector<double> e_vec = {1};
// 15x15
const std::vector<double> u_vec {1e-06, 7.1969e-06, 5.1795e-05, 0.00037276, 0.0026827, 0.019307, 0.13895, 1, 7.1969, 51.795, 372.76, 2682.7, 19307, 1.3895e+05, 1e+06};
const std::vector<double> beta_vec {1e-06, 7.1969e-06, 5.1795e-05, 0.00037276, 0.0026827, 0.019307, 0.13895, 1, 7.1969, 51.795, 372.76, 2682.7, 19307, 1.3895e+05, 1e+06};
const std::vector<double> e_vec = {1};

//const std::vector<double> u_vec {1e2};
//const std::vector<double> beta_vec {1e5};
//const std::vector<double> e_vec = {1};

const bool save_x = false;

const std::vector<double> N_vec{161};
const std::vector<double> L_vec{2};
const std::vector<double> S_vec{4};

const std::vector<double> tol_vec{1e-12};
const std::vector<int> maxiterations_vec{2000};

const double length = 1;
const double const_source = 1;
const double boundary_value =
	0; //This should be 0 because it is assumed in the derivation that the value is 0 at the edges.


struct Result
{
	std::string algorithm;
	double true_residual;
	double estimated_residual;
	double iterations;
	double return_flag;
	double u;
	double beta;
	double e;
	double time;
	int N;
	int max_iterations;
	double tolerance;
	double const_source;
	double boundary_value;
	double length;
	int S;
	int L;
	int64_t timestamp;
};

void print_result(Result result)
{
	std::cout <<
		result.algorithm << "\t" <<
		result.true_residual << "\t" <<
		result.estimated_residual << "\t" <<
		result.iterations << "\t" <<
		result.return_flag << "\t" <<
		result.u << "\t" <<
		result.beta << "\t" <<
		result.e << "\t" <<
		result.time << "\t" <<
		result.N << "\t" <<
		result.max_iterations << "\t" <<
		result.tolerance << "\t" <<
		result.const_source << "\t" <<
		result.boundary_value << "\t" <<
		result.length << "\t" <<
		result.S << "\t" <<
		result.L << "\t" <<
		result.timestamp << "\t" <<
		std::endl;
}

void print_results(std::vector<Result> results)
{
	for (auto result : results)
	{
		print_result(result);
	}
}

template<typename T>
Result solve_system(T& solver, const SparseMatrixType& A, const VectorType& b,
	double max_iterations, double tolerance, int S = -1, int L = -1)
{
	using namespace std::chrono;
	int64_t timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

	if (save_x)
	{
		Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int> > solver;
		// fill A and b;
		// Compute the ordering permutation vector from the structural pattern of A
		solver.analyzePattern(A);
		// Compute the numerical factorization
		solver.factorize(A);
		//Use the factors to solve the linear system
		VectorType x = solver.solve(b);
		//Eigen::saveMarket(A, "A_" + std::to_string(timestamp));
		//Eigen::saveMarketVector(b, "b_" + std::to_string(timestamp));
		//Eigen::saveMarketVector(x, "x_LU_" + std::to_string(timestamp));
	}

	Result r;
	r.timestamp = timestamp;
	high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

	if constexpr(std::is_same_v<T, Eigen::IDRStab<SparseMatrixType>>)
	{
		//if S and L are not given, the default of S=4,L=2 is used inside idrstab
		solver.setS(S);
		solver.setL(L);

	}

	solver.compute(A);
	solver.setTolerance(tolerance);
	solver.setMaxIterations(max_iterations);
	VectorType x = solver.solve(b);

	if (save_x)
	{
		if constexpr(std::is_same_v<T, Eigen::IDRStab<SparseMatrixType>>)
		{
			//Eigen::saveMarketVector(x, "x_IDRStab_" + std::to_string(timestamp));
		}

		if constexpr(std::is_same_v<T, Eigen::BiCGSTAB<SparseMatrixType>>)
		{
			//Eigen::saveMarketVector(x, "x_BiCGSTAB_" + std::to_string(timestamp));
		}
	}

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	r.time = time_span.count();
	r.estimated_residual = solver.error();
	r.true_residual = (b - A * x).norm() / b.norm();
	r.iterations = solver.iterations();
	r.return_flag = solver.info();
	r.tolerance = tolerance;
	r.max_iterations = max_iterations;

	return r;
}
double B(double z)
{
	return z / (std::expm1(z));
}

void construct_3D_indexing(SparseMatrixType& A, VectorType& b, int N, double u, double e,
	double beta)
{
	A.reserve(Eigen::VectorXi::Constant(A.cols(), 7)); //About 2x as fast as triplet construction
	const double h = length / (N - 1);
	const double P = u * h / e;
	const double e_HF = e * (P / expm1(P) - P / expm1(-P)) / 2;
	const double A_coeff = (-e_HF / h + u / 2);
	const double B_coeff = (-e_HF / h - u / 2);
	const double C_coeff = 6 * e_HF / h - h * beta;

	//Loop over grid in the x-direction(east-west)
	for (int i = 0; i < N; ++i)
	{
		//Loop over grid in the y-direction(north-south)
		for (int j = 0; j < N; ++j)
		{
			//Loop over grid in the z-direction(up-down)
			for (int k = 0; k < N; ++k)
			{
				double s_C = h * const_source;

				int index_center = i + j * N + k * N * N;
				int index_east  = (i + 1) + j * N + k * N * N;
				int index_west  = (i - 1) + j * N + k * N * N;
				int index_north = i + (j + 1) * N + k * N * N;
				int index_south = i + (j - 1) * N + k * N * N;
				int index_up    = i + j * N + (k + 1) * N * N;
				int index_down  = i + j * N + (k - 1) * N * N;

				//East
				if ((i + 1) + 1 <= N)
				{
					A.insert(index_center, index_east) = A_coeff;
				}

				//West
				if ((i - 1) >= 0)
				{
					A.insert(index_center, index_west) = B_coeff;
				}

				//North
				if ((j + 1) + 1 <= N)
				{
					A.insert(index_center, index_north) = A_coeff;
				}

				//South
				if ((j - 1) >= 0)
				{
					A.insert(index_center, index_south) = B_coeff;
				}

				//Up
				if ((k + 1) + 1 <= N)
				{
					A.insert(index_center, index_up) = A_coeff;
				}

				//Down
				if ((k - 1) >= 0)
				{
					A.insert(index_center, index_down) = B_coeff;
				}

				//Center
				A.insert(index_center, index_center) = C_coeff;
				b(index_center) = s_C;
			}
		}

	}

	// A.reserve(Eigen::VectorXi::Constant(A.cols(), 7)); //About 2x as fast as triplet construction
	// //A.reserve(7 * N); //This is very horrible
	// const double h = length / (N - 1);
	// const double P = u * h / e;
	// const double e_HF = e * (P / expm1(P) - P / expm1(-P)) / 2;
	// const double A_coeff = (-e_HF / h + u / 2);
	// const double B_coeff = (-e_HF / h - u / 2);
	// const double C_coeff = 6 * e_HF / h - h * beta;

	// //Loop over grid in the x-direction(east-west)
	// for (int i = 0; i < N; ++i)
	// {
	// 	//Loop over grid in the y-direction(north-south)
	// 	for (int j = 0; j < N; ++j)
	// 	{
	// 		//Loop over grid in the z-direction(up-down)
	// 		for (int k = 0; k < N; ++k)
	// 		{
	// 			double s_C = h * const_source;

	// 			int index_center = i + j * N + k * N * N;
	// 			int index_east  = (i + 1) + j * N + k * N * N;
	// 			int index_west  = (i - 1) + j * N + k * N * N;
	// 			int index_north = i + (j + 1) * N + k * N * N;
	// 			int index_south = i + (j - 1) * N + k * N * N;
	// 			int index_up    = i + j * N + (k + 1) * N * N;
	// 			int index_down  = i + j * N + (k - 1) * N * N;

	// 			//East
	// 			if (i + 1 < N)
	// 			{
	// 				A.insert(index_center, index_east) = A_coeff;
	// 			}
	// 			//West
	// 			if (i - 1 > 0)
	// 			{
	// 				A.insert(index_center, index_west) = B_coeff;
	// 			}

	// 			//North
	// 			if (j + 1 < N)
	// 			{
	// 				A.insert(index_center, index_north) = A_coeff;
	// 			}
	// 			//South
	// 			if (j - 1 > 0)
	// 			{
	// 				A.insert(index_center, index_south) = B_coeff;
	// 			}

	// 			//Up
	// 			if (k + 1 < N)
	// 			{
	// 				A.insert(index_center, index_up) = A_coeff;
	// 			}
	// 			//Down
	// 			if (k - 1 > 0)
	// 			{
	// 				A.insert(index_center, index_down) = B_coeff;
	// 			}


	// 			//Center
	// 			A.insert(index_center, index_center) = C_coeff;

	// 			b(index_center) = s_C;

	// 		}

	// 	}

	// }
}
void construct_3D(SparseMatrixType& A, VectorType& b, int N, double u, double e, double beta)
{
	const double h = length / (N - 1);
	const double P = u * h / e;
	const double e_HF = e * (P / expm1(P) - P / expm1(-P)) / 2;
	const double A_coeff = (-e_HF / h + u / 2) / h;
	const double B_coeff = (-e_HF / h - u / 2) / h;
	const double C_coeff = 6 * e_HF / (h * h) - beta;
	typedef Eigen::Triplet<double, int> T;
	//https://stackoverflow.com/questions/44680704/eigen-fill-up-sparse-rowmajor-matrix-with-triplets
	std::vector<T> entries;

	//Loop over grid in the x-direction(east-west)
	for (int i = 0; i < N; ++i)
	{
		//Loop over grid in the y-direction(north-south)
		for (int j = 0; j < N; ++j)
		{

			//Loop over grid in the z-direction(up-down)
			for (int k = 0; k < N; ++k)
			{
				double s_C = const_source;

				int index_center = i + j * N + k * N * N;
				int index_east  = (i + 1) + j * N + k * N * N;
				int index_west  = (i - 1) + j * N + k * N * N;
				int index_north = i + (j + 1) * N + k * N * N;
				int index_south = i + (j - 1) * N + k * N * N;
				int index_up    = i + j * N + (k + 1) * N * N;
				int index_down  = i + j * N + (k - 1) * N * N;

				//East
				if (i + 1 < N)
				{
					entries.push_back(T(index_center, index_east, A_coeff));
				}
				else
				{
					s_C = s_C - A_coeff * boundary_value;
				}

				//West
				if (i - 1 > 0)
				{
					entries.push_back(T(index_center, index_west, B_coeff));
				}
				else
				{
					s_C = s_C - B_coeff * boundary_value;
				}

				//North
				if (j + 1 < N)
				{
					entries.push_back(T(index_center, index_north, A_coeff));
				}
				else
				{
					s_C = s_C - A_coeff * boundary_value;
				}

				//South
				if (j - 1 > 0)
				{
					entries.push_back(T(index_center, index_south, B_coeff));
				}
				else
				{
					s_C = s_C - B_coeff * boundary_value;
				}

				//Up
				if (k + 1 < N)
				{
					entries.push_back(T(index_center, index_up, A_coeff));
				}
				else
				{
					s_C = s_C - A_coeff * boundary_value;
				}

				//Down
				if (k - 1 > 0)
				{
					entries.push_back(T(index_center, index_down, B_coeff));
				}
				else
				{
					s_C = s_C - B_coeff * boundary_value;
				}

				//Center
				entries.push_back(T(index_center, index_center, C_coeff));

				b(index_center) = s_C;

			}

		}

	}

	A.setFromTriplets(entries.begin(), entries.end());
}
void construct_2D_indexing(SparseMatrixType& A, VectorType& b, int N, double u, double e,
	double beta)
{
	A.reserve(Eigen::VectorXi::Constant(A.cols(), 5)); //About 2x as fast as triplet construction
	const double h = length / (N - 1);
	const double P = u * h / e;
	const double e_HF = e * (P / expm1(P) - P / expm1(-P)) / 2;
	//const double e_HF = e;
	const double A_coeff = (-e_HF / h + u / 2);
	const double B_coeff = (-e_HF / h - u / 2);
	const double C_coeff = 4 * e_HF / h - h * beta;

	//Loop over grid in the x-direction(east-west)
	for (int i = 0; i < N; ++i)
	{
		//Loop over grid in the y-direction(north-south)
		for (int j = 0; j < N; ++j)
		{

			double s_C = h * const_source;

			int index_center = i + j * N;
			int index_east  = (i + 1) + j * N;
			int index_west  = (i - 1) + j * N;
			int index_north = i + (j + 1) * N;
			int index_south = i + (j - 1) * N;

			//East
			if ((i + 1) + 1 <= N)
			{
				A.insert(index_center, index_east) = A_coeff;
			}

			//West
			if ((i - 1) >= 0)
			{
				A.insert(index_center, index_west) = B_coeff;
			}

			//North
			if ((j + 1) + 1 <= N)
			{
				A.insert(index_center, index_north) = A_coeff;
			}

			//South
			if ((j - 1) >= 0)
			{
				A.insert(index_center, index_south) = B_coeff;
			}

			//Center
			A.insert(index_center, index_center) = C_coeff;
			b(index_center) = s_C;
		}

	}
}
void construct_2D(SparseMatrixType& A, VectorType& b, int N, double u, double e, double beta)
{
	const double h = length / (N - 1);
	const double P = u * h / e;
	const double e_HF = e * (P / expm1(P) - P / expm1(-P)) / 2;
	const double A_coeff = (-e_HF / h + u / 2) / h;
	const double B_coeff = (-e_HF / h - u / 2) / h;
	const double C_coeff = 4 * e_HF / (h * h) - beta;
	typedef Eigen::Triplet<double, int> T;
	//https://stackoverflow.com/questions/44680704/eigen-fill-up-sparse-rowmajor-matrix-with-triplets
	std::vector<T> entries;


	//Loop over grid in the x-direction(east-west)
	for (int i = 0; i < N; ++i)
	{
		//Loop over grid in the y-direction(north-south)
		for (int j = 0; j < N; ++j)
		{

			double s_C = const_source;

			int index_center = i + j * N;
			int index_east  = (i + 1) + j * N;
			int index_west  = (i - 1) + j * N;
			int index_north = i + (j + 1) * N;
			int index_south = i + (j - 1) * N;

			//East
			if (i + 1 < N)
			{
				entries.push_back(T(index_center, index_east, A_coeff));
			}
			else
			{
				s_C = s_C - A_coeff * boundary_value;
			}

			//West
			if (i - 1 > 0)
			{
				entries.push_back(T(index_center, index_west, B_coeff));
			}
			else
			{
				s_C = s_C - B_coeff * boundary_value;
			}

			//North
			if (j + 1 < N)
			{
				entries.push_back(T(index_center, index_north, A_coeff));
			}
			else
			{
				s_C = s_C - A_coeff * boundary_value;
			}

			//South
			if (j - 1 > 0)
			{
				entries.push_back(T(index_center, index_south, B_coeff));
			}
			else
			{
				s_C = s_C - B_coeff * boundary_value;
			}

			//Center
			entries.push_back(T(index_center, index_center, C_coeff));
			b(index_center) = s_C;
		}

	}

	A.setFromTriplets(entries.begin(), entries.end());
}

void run_solvers(std::vector<Result> results, SparseMatrixType& A, VectorType& b, int N, double u,
	double e, double beta)
{

	for (auto tolerance : tol_vec)
	{
		for (auto max_iterations : maxiterations_vec)
		{
			// for (auto L : L_vec)
			// {
			// 	for (auto S : S_vec)
			// 	{
			// 		Eigen::IDRStab<SparseMatrixType, PRECONDITIONER> solver_idrstab;
			// 		Result result_idrstab = solve_system(solver_idrstab, A, b, max_iterations, tolerance, S, L);
			// 		result_idrstab.algorithm = "IDRStab";
			// 		result_idrstab.u = u;
			// 		result_idrstab.beta = beta;
			// 		result_idrstab.e = e;
			// 		result_idrstab.const_source = const_source;
			// 		result_idrstab.boundary_value = boundary_value;
			// 		result_idrstab.length = length;
			// 		result_idrstab.N = N;
			// 		result_idrstab.L = L;
			// 		result_idrstab.S = S;
			// 		results.push_back(result_idrstab);
			// 		print_result(result_idrstab);
			// 	}
			// }

			Eigen::BiCGSTAB<SparseMatrixType, PRECONDITIONER> solver_bicgstab;
			Result result_bicgstab = solve_system(solver_bicgstab, A, b, max_iterations * 6.5, tolerance);
			/*
			        The number 6 comes from:
			        BiCGStab does 2 MV per cycle, IDR(4)Stab(2) (Kensuke's implementation) 13 per cycle.
				L*(S+1)+L+1 for the Kensuke variant
				L*(S+1) for the original variant
			*/
			result_bicgstab.algorithm = "BiCGSTAB";
			result_bicgstab.u = u;
			result_bicgstab.beta = beta;
			result_bicgstab.e = e;
			result_bicgstab.const_source = const_source;
			result_bicgstab.boundary_value = boundary_value;
			result_bicgstab.length = length;
			result_bicgstab.L = 1;
			result_bicgstab.S = 1;
			result_bicgstab.N = N;
			results.push_back(result_bicgstab);
			print_result(result_bicgstab);
		}
	}
}

void test_2D(std::vector<Result>results, int N, double u, double e, double beta)
{
	SparseMatrixType A(N * N, N * N);
	VectorType b(N * N);
	construct_2D_indexing(A, b, N, u, e, beta);
	run_solvers(results, A, b, N, u, e, beta);
}
void test_3D(std::vector<Result>results, int N, double u, double e, double beta)
{
	SparseMatrixType A(N * N * N, N * N * N);
	VectorType b(N * N * N);
	construct_3D_indexing(A, b, N, u, e, beta);
	run_solvers(results, A, b, N, u, e, beta);
}

int main()
{
	std::vector<Result> results;

	for (int N : N_vec)
	{
		for (double u : u_vec)
		{
			for (double e : e_vec)
			{
				for (double beta : beta_vec)
				{
					test_2D(results, N, u, e, beta);
					//test_3D(results, N, u, e, beta);
				}
			}
		}
	}

}
