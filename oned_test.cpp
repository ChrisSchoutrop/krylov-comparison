#include "flux.hpp"
#include "mf.hpp"
#include <cmath>
#include <eigen3/Eigen/Dense>
#include <iostream>
#include <algorithm>
#include <array>
#include <iomanip>
/*
        Test of the model BVP presented in ACFD4.pdf
        System is small enough, dense will be fine
*/
using namespace Eigen;
void compare_solutions(VectorXd value, VectorXd expected)
{
	// std::cout << std::setw(15) << "value" << std::setw(15) << "expected" << std::endl;

	// for (int i = 0; i < value.size(); i++)
	// {
	// 	std::cout << std::setw(15) << value[i] << std::setw(15) << expected[i]  << std::endl;
	// }

	double deviation = (value - expected).norm();
	//std::cout << "Deviation: " << deviation << std::endl;
	assert(deviation < 1e-12);

}
double exact_solution(const double u, const double e, const double x)
{
	//Exact solution to the differential equation
	return (1 - std::exp(u * x / e)) / (1 - exp(u / e));
}
double exact_solution_CD(const double u, const double e, const double dx, const int N, const int i)
{
	//Exact solution to the CD difference scheme
	double P = mf::P(u, e, dx);
	double C2 = 1 / (std::pow((2 + P) / (2 - P), N - 1) - 1);
	double C1 = -C2;
	return C1 + C2 * std::pow((2 + P) / (2 - P), i);
}

double exact_solution_UW(const double u, const double e, const double dx, const int N, const int i)
{
	//Exact solution to the UW difference scheme
	double P = mf::P(u, e, dx);
	double C2 = 1 / (std::pow(1 + P, N - 1) - 1);
	double C1 = -C2;
	return C1 + C2 * std::pow(1 + P, i);
}
double exact_solution_HF(const double u, const double e, const double dx, const int N, const int i)
{
	//Exact solution to the HF difference scheme
	double P = mf::P(u, e, dx);
	double C2 = 1 / (std::expm1((N - 1) * P));
	double C1 = -C2;
	return C1 + C2 * std::exp(i * P);
}
double exact_solution_CF(const double u, const double e, const double dx, const int N, const int i)
{
	//Exact solution to the CF difference scheme
	//In this test there is no source, so HF=CF.
	return exact_solution_HF(u, e, dx, N, i);
}

struct Input
{
	int dimension = 1;
	int N;

	//Flux approximation
	std::function<std::array<double, 4>(double, double, double)> flux;

	//X-component advection(x):
	//https://stackoverflow.com/questions/6601930/c11-lambda-as-member-variable
	//double (*u_x)(double);
	std::function<double(double)> u;

	//Diffusion (x)
	std::function<double(double)> e;

	//Source (x)
	std::function<double(double)> s;

	//Dirichlet boundary conditions
	double phi_w{0};
	double phi_e{1};

	double x_w{0};
	double x_e{1};
};

VectorXd simulate(const Input& input_struct)
{
	double dx = (input_struct.x_e - input_struct.x_w) / (input_struct.N - 1);
	int N = input_struct.N;
	//Discretization matrix
	MatrixXd A(N, N);
	A.setZero();

	//Right hand side
	VectorXd b(N, 1);
	b.setZero();

	//Solution vector
	VectorXd phi(N, 1);

	//Dirichlet boundary conditions:
	A(0, 0) = 1;
	A(N - 1, N - 1) = 1;
	b(0) = input_struct.phi_w; //phi(x=0)
	b(N - 1) = input_struct.phi_e; //phi(x=1)

	//Constant coefficients in this example, no need to recalculate for each cell
	enum coefficient {alpha, beta, gamma, delta};
	std::array<double, 4> F_right;
	std::array<double, 4> F_left;

	for (size_t i = 1; i < N - 1; ++i)
	{
		double x = input_struct.x_w + i * dx;
		F_right = input_struct.flux(input_struct.u(x + 0.5 * dx), input_struct.e(x + 0.5 * dx), dx);
		F_left = input_struct.flux(input_struct.u(x - 0.5 * dx), input_struct.e(x - 0.5 * dx), dx);
		//Loop over all non-boundary cells
		//Conservation law: F_right-F_left=dx*s
		//F_left
		A(i, i - 1) -= F_left[alpha];
		A(i, i) -= F_left[beta];
		A(i, i + 1) -= 0;

		//F_right
		A(i, i - 1) += 0;
		A(i, i) += F_right[alpha];
		A(i, i + 1) += F_right[beta];

		//F_left
		b(i) -= dx * (F_left[gamma] * input_struct.s(x - dx) + F_left[delta] * input_struct.s(x));
		//F_right
		b(i) -= dx * (F_right[gamma] * input_struct.s(x) + F_right[delta] * input_struct.s(x + dx));
		//Source
		b(i) += dx * input_struct.s(x);
	}

	phi = A.colPivHouseholderQr().solve(b);
	//std::cout << "Residual: " << (b - A * phi).norm() / b.norm() << std::endl;
	//std::cout << A << std::endl;
	return phi;
}

int main()
{
	int N = 5;

	Input input;
	input.N = N;
	double dx = (input.x_e - input.x_w) / (input.N - 1);
	input.u = [](double x)
	{
		return 1.0;
	};
	input.e = [](double x)
	{
		return 1.0e-2;
	};
	input.s = [](double x)
	{
		return 0.0;
	};
	input.flux = [](double u, double e, double dx)
	{
		return flux::UW(u, e, dx);
	};
	VectorXd phi_UW = simulate(input);
	input.flux = [](double u, double e, double dx)
	{
		return flux::CD(u, e, dx);
	};
	VectorXd phi_CD = simulate(input);
	input.flux = [](double u, double e, double dx)
	{
		return flux::HF(u, e, dx);
	};
	VectorXd phi_HF = simulate(input);
	input.flux = [](double u, double e, double dx)
	{
		return flux::CF(u, e, dx);
	};
	VectorXd phi_CF = simulate(input);

	VectorXd phi_UW_exact(N, 1);
	VectorXd phi_CD_exact(N, 1);
	VectorXd phi_HF_exact(N, 1);
	VectorXd phi_CF_exact(N, 1);
	VectorXd phi_exact(N, 1);

	for (size_t i = 0; i < N; i++)
	{
		phi_UW_exact(i) = exact_solution_UW(input.u(0), input.e(0), dx, N, i);
		phi_CD_exact(i) = exact_solution_CD(input.u(0), input.e(0), dx, N, i);
		phi_HF_exact(i) = exact_solution_HF(input.u(0), input.e(0), dx, N, i);
		phi_CF_exact(i) = exact_solution_CF(input.u(0), input.e(0), dx, N, i);
		phi_exact(i) = exact_solution(      input.u(0), input.e(0), dx * i);
	}

	std::cout << "UW test" << std::endl;
	compare_solutions(phi_UW, phi_UW_exact);
	std::cout << "CD test" << std::endl;
	compare_solutions(phi_CD, phi_CD_exact);
	std::cout << "HF test" << std::endl;
	compare_solutions(phi_HF, phi_HF_exact);
	std::cout << "CF test" << std::endl;
	compare_solutions(phi_CF, phi_CF_exact);
	std::cout << "HF test2" << std::endl;
	compare_solutions(phi_HF, phi_exact);
	std::cout << "CF test2" << std::endl;
	compare_solutions(phi_CF, phi_exact);
	/*
	        Discrete conservation law:
		F_{j+1/2}-F_{j-1/2}=dx*s_j
	        F_{j+1/2}=
		+\alpha_{j+1/2}\varphi_j
		+\beta_{j+1/2}\varphi_{j+1}
		 +\Delta x(
		+\gamma_{j+1/2}s_j
		+\delta_{j+1/2}s_{j+1}
		)
	        F_{j-1/2}=
		+\alpha_{j-1/2}\varphi_{j-1}
		+\beta_{j-1/2}\varphi_{j}
		 +\Delta x(
		+\gamma_{j-1/2}s_{j-1}
		+\delta_{j-1/2}s_{j}
		)
	*/
}