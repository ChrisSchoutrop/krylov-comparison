//#include <cfenv>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
//#include <unsupported/Eigen/IterativeSolvers>
//#include "unsupported/Eigen/src/SparseExtra/MarketIO.h"
#include <Eigen/SparseLU>
#include "IDRStab.h"
#include "BiCGSTABL.h"
#include <vector>
#include <string>
#include <iostream>
#include <chrono>
#include <type_traits>
#include <cmath>
#include <limits>
#include "discretizer.hpp"
#include "input_struct.hpp"
#include <fstream>
#include "mf.hpp"
#include "result.hpp"
#include "condestLU.hpp"

#include <amgcl/adapter/eigen.hpp>
#include <amgcl/backend/builtin.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/solver/bicgstabl.hpp>
#include <amgcl/solver/idrs.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/damped_jacobi.hpp>
#include <amgcl/relaxation/as_preconditioner.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/profiler.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/adapter/eigen.hpp>
#include <amgcl/backend/eigen.hpp>
#include "cxxopts.hpp"
#include <omp.h>

//#define PRECONDITIONER Eigen::IncompleteLUT<double>
#define PRECONDITIONER Eigen::DiagonalPreconditioner<double>
//#define PRECONDITIONER Eigen::IdentityPreconditioner
#define SAVE_SOLUTION false

typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SparseMatrixType;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorType;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> DenseMatrixTypeCol;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DenseMatrixTypeRow;

//Settings
const std::vector<std::string> schemes{"CD"};
const std::vector<std::string> solvers{"eigen_bicgstab_diagonal", "eigen_bicgstabl_diagonal"};
const std::vector<double> Pe_vec{mf::logspace(-6, 6, 16)};
const std::vector<double> Da_vec{mf::logspace(-6, 6, 16)};
const std::vector<double> e_vec{1};
const std::vector<double> N_vec{81};
const std::vector<double> L_vec{1, 2, 3, 4, 8, 16};
const std::vector<double> S_vec{1, 2, 3, 4};
const double tolerance = 1e-12;
const int max_iterations = 2000;

template<typename T>
void solve_system_eigen(Result& result, T& solver, const SparseMatrixType& A, const VectorType& b,
	double max_iterations, double tolerance, int S = -1, int L = -1)
{
	using namespace std::chrono;

	//Start the timer
	high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

	//Set up the solver
	solver.compute(A);
	solver.setTolerance(tolerance);
	solver.setMaxIterations(max_iterations);

	//Set IDRStab/BiCGSTABL parameters
	if constexpr(std::is_same_v<T, Eigen::IDRStab<SparseMatrixType>>)
	{
		solver.setS(S);
		solver.setL(L);
	}

	if constexpr(std::is_same_v<T, Eigen::BiCGSTABL<SparseMatrixType>>)
	{
		solver.setL(L);
	}

	//Solve the system
	VectorType x = VectorType::Ones(b.rows());
	x = solver.solveWithGuess(b, x);
	//VectorType x = solver.solve(b);

	//Stop the timer
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	//Store the solver performance in the struct
	result.set_post_info((b - A * x).norm() / b.norm(), solver.error(), solver.iterations(),
		solver.info(), time_span.count());

	#if SAVE_SOLUTION
	saveMarketVector(x, Result.m_algorithm + std::to_string(timestamp));
	#endif
}

template<typename T, typename B>
void solve_system_amgcl(Result& result, const SparseMatrixType& A,
	const VectorType& b,
	double max_iterations, double tolerance)
{
	//Definitions
	using namespace std::chrono;
	int iterations;
	double true_residual;
	double estimated_residual;

	//Set up the solver parameters
	typename T::params prm;
	prm.solver.tol = tolerance;
	prm.solver.maxiter = max_iterations;

	//Initial guess & result vector
	//Eigen::VectorXd x_eigen = Eigen::VectorXd::Zero(A.rows());
	Eigen::VectorXd x_eigen = Eigen::VectorXd::Ones(A.rows());
	std::vector<double> x_amg = std::vector<double>(x_eigen.data(), x_eigen.data() + x_eigen.size());

	//Start the timer
	high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

	try
	{
		//Is this amgcl with amgcl backend?
		if constexpr(std::is_same_v<B, amgcl::backend::builtin<double>>)
		{
			//Convert Eigen's matrix format to one compatible with amgcl's backend
			size_t n = A.rows();
			const int* ptr = A.outerIndexPtr();
			const int* col = A.innerIndexPtr();
			const double* val = A.valuePtr();
			amgcl::backend::crs<double> A_amgcl(std::make_tuple(n,
					amgcl::make_iterator_range(ptr, ptr + n + 1),
					amgcl::make_iterator_range(col, col + ptr[n]),
					amgcl::make_iterator_range(val, val + ptr[n])));

			//Set up the solver
			T solve(A_amgcl, prm);

			//Solve the system for the given RHS:
			std::tie(iterations, estimated_residual) = solve(A_amgcl, b, x_amg);

			//Convert the result back into Eigen's format to compute the true residual.
			Eigen::VectorXd x_result = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(x_amg.data(),
					x_amg.size());
			true_residual = (b - A * Eigen::VectorXd(x_result)).norm() / b.norm();
		}
		//Is this amgcl with Eigen as backend?
		else if constexpr(std::is_same_v<B, amgcl::backend::eigen<double>>)
		{
			//Solve the system
			T solve(A, prm);
			std::tie(iterations, estimated_residual) = solve(A, b, x_eigen);

			//Calculate the true residual.
			true_residual = (b - A * x_eigen).norm() / b.norm();
		}
	}
	catch (const std::exception& ex)
	{
		std::cerr << "Exception in amgcl solving: " << ex.what() << std::endl;
		true_residual = std::numeric_limits<double>::quiet_NaN();
		estimated_residual = std::numeric_limits<double>::quiet_NaN();
		iterations = max_iterations;
	}
	catch (...)
	{
		std::cerr << "Unknown exception in amgcl solving" << std::endl;
		true_residual = std::numeric_limits<double>::quiet_NaN();
		estimated_residual = std::numeric_limits<double>::quiet_NaN();
		iterations = max_iterations;
	}

	//Stop the timer
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	//Store the solver performance in the struct
	result.set_post_info(true_residual, estimated_residual, iterations, -1, time_span.count());
}

template<typename T, typename B = amgcl::backend::builtin<double>>
void solve_system(T& solver, Result& result, const SparseMatrixType& A, const VectorType& b,
	const double max_iterations, const double tolerance, const int S = -1, const int L = -1)
{
	if constexpr(std::is_same_v<T, Eigen::IDRStab<SparseMatrixType>>
		|| std::is_same_v<T, Eigen::BiCGSTABL<SparseMatrixType>>
		|| std::is_same_v<T, Eigen::BiCGSTAB<SparseMatrixType>>)
	{
		solve_system_eigen(solver, A, b, max_iterations, tolerance, S, L);
	}
	else
	{
		solve_system_amgcl<B>(result, A,
			b,
			max_iterations, tolerance);
	}

	std::cout << result << std::endl;
}

void run_solver(const SparseMatrixType& A, const VectorType& b,
	int N, double u,
	double e, double beta, std::string scheme, std::string solver)
{
	using namespace std::chrono;
	int64_t timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	double cond = CondestLU::condestSuperLU(A);

	if (solver == "amgcl_bicgstab_amg")
	{
		/*
		        Library: AMGCL
		        Solver : BiCGStab
		        Precond: AMG
		        Backend: AMGCL
		*/
		Result result;
		result.set_pre_info(solver, u, beta, e, 1, 1, timestamp, scheme, tolerance, N, max_iterations, cond);
		typedef amgcl::backend::builtin<double> backend;
		typedef amgcl::make_solver <
		amgcl::amg <
		backend,
		amgcl::coarsening::smoothed_aggregation,
		amgcl::relaxation::spai0 >,
		amgcl::solver::bicgstab<backend >>
		amgcl_solver;
		solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
		std::cout << result;
	}
	else if (solver == "amgcl_bicgstabl_amg")
	{
		/*
		        Library: AMGCL
		        Solver : BiCGStab(L=2)
		        Precond: AMG
		        Backend: AMGCL
		*/
		Result result;
		result.set_pre_info(solver, u, beta, e, 1, 2, timestamp, scheme, tolerance, N,
			max_iterations, cond);
		typedef amgcl::backend::builtin<double> backend;
		typedef amgcl::make_solver <
		amgcl::amg <
		backend,
		amgcl::coarsening::smoothed_aggregation,
		amgcl::relaxation::spai0 >,
		amgcl::solver::bicgstabl<backend >>
		amgcl_solver;
		solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
		std::cout << result;
	}
	else if (solver == "amgcl_idrs_amg")
	{
		/*
		        Library: AMGCL
		        Solver : IDR(S=4)
		        Precond: AMG
		        Backend: AMGCL
		*/
		Result result;
		result.set_pre_info(solver, u, beta, e, 1, 4, timestamp, scheme, tolerance, N,
			max_iterations, cond);
		typedef amgcl::backend::builtin<double> backend;
		typedef amgcl::make_solver <
		amgcl::amg <
		backend,
		amgcl::coarsening::smoothed_aggregation,
		amgcl::relaxation::spai0 >,
		amgcl::solver::idrs<backend >>
		amgcl_solver;
		solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
		std::cout << result;
	}
	else if (solver == "amgcl_eigen_bicgstab_amg")
	{
		/*
		        Library: AMGCL
		        Solver : BiCGStab
		        Precond: AMG
		        Backend: Eigen
		*/
		Result result;
		result.set_pre_info(solver, u, beta, e, 1, 1, timestamp, scheme, tolerance,
			N, max_iterations, cond);
		typedef amgcl::backend::eigen<double> backend;
		typedef amgcl::make_solver <
		amgcl::amg <
		backend,
		amgcl::coarsening::smoothed_aggregation,
		amgcl::relaxation::spai0 >,
		amgcl::solver::bicgstab<backend >>
		amgcl_solver;
		solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
		std::cout << result;
	}
	else if (solver == "eigen_idrstab_diagonal")
	{
		/*
		        Library: Eigen
		        Solver : IDR(S)Stab(L)
		        Precond: Diagonal
		        Backend: Eigen
		*/
		for (auto S : S_vec)
		{
			for (auto L : L_vec)
			{
				Result result;
				result.set_pre_info(solver, u, beta, e, S, L, timestamp, scheme, tolerance, N, max_iterations, cond);
				Eigen::IDRStab<SparseMatrixType, PRECONDITIONER> solver;
				solve_system_eigen(result, solver, A, b, max_iterations, tolerance, S, L);
				std::cout << result;
			}
		}

	}
	else if (solver == "eigen_bicgstabl_diagonal")
	{
		/*
		        Library: Eigen
		        Solver : BiCGStab(L)
		        Precond: Diagonal
		        Backend: Eigen
		*/
		for (auto L : L_vec)
		{
			Result result;


			result.set_pre_info(solver, u, beta, e, 1, L, timestamp, scheme, tolerance, N,
				max_iterations, cond);
			Eigen::BiCGSTABL<SparseMatrixType, PRECONDITIONER> solver;
			solve_system_eigen(result, solver, A, b, max_iterations, tolerance, 1, L);
			std::cout << result;
		}
	}
	else if (solver == "eigen_bicgstab_diagonal")
	{
		/*
		        Library: Eigen
		        Solver : BiCGStab
		        Precond: Diagonal
		        Backend: Eigen
		*/
		Result result;
		result.set_pre_info(solver, u, beta, e, 1, 1, timestamp, scheme, tolerance, N,
			max_iterations, cond);
		Eigen::BiCGSTAB<SparseMatrixType, PRECONDITIONER> solver_bicgstab;
		solve_system_eigen(result, solver_bicgstab, A, b, max_iterations, tolerance, 1, 1);
		std::cout << result;
	}
	else if (solver == "amgcl_bicgstab_diagonal")
	{
		/*
		        Library: AMGCL
		        Solver : BiCGStab
		        Precond: Diagonal
		        Backend: AMGCL
		*/
		Result result;
		result.set_pre_info(solver, u, beta, e, 1, 1, timestamp, scheme, tolerance, N,
			max_iterations, cond);
		typedef amgcl::backend::builtin<double> backend;
		typedef amgcl::make_solver <
		amgcl::relaxation::as_preconditioner <
		amgcl::backend::builtin<double>,
		      amgcl::relaxation::damped_jacobi
		      >,
		      amgcl::solver::bicgstab <
		      amgcl::backend::builtin<double>
		      >>
		      amgcl_solver;
		solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
		std::cout << result;
	}
	else if (solver == "amgcl_bicgstabl_diagonal")
	{
		/*
		        Library: AMGCL
		        Solver : BiCGStab(L=2)
		        Precond: Diagonal
		        Backend: AMGCL
		*/
		Result result;
		result.set_pre_info(solver, u, beta, e, 1, 2, timestamp, scheme, tolerance, N,
			max_iterations, cond);
		typedef amgcl::backend::builtin<double> backend;
		typedef amgcl::make_solver <
		amgcl::relaxation::as_preconditioner <
		amgcl::backend::builtin<double>,
		      amgcl::relaxation::damped_jacobi
		      >,
		      amgcl::solver::bicgstabl <
		      amgcl::backend::builtin<double>
		      >>
		      amgcl_solver;
		solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
		std::cout << result;
	}
	else if (solver == "amgcl_idrs_diagonal")
	{
		/*
		        Library: AMGCL
		        Solver : IDR(S=4)
		        Precond: Diagonal
		        Backend: AMGCL
		*/
		Result result;
		result.set_pre_info(solver, u, beta, e, 4, 1, timestamp, scheme, tolerance, N,
			max_iterations, cond);
		typedef amgcl::backend::builtin<double> backend;
		typedef amgcl::make_solver <
		amgcl::relaxation::as_preconditioner <
		amgcl::backend::builtin<double>,
		      amgcl::relaxation::damped_jacobi
		      >,
		      amgcl::solver::idrs <
		      amgcl::backend::builtin<double>
		      >>
		      amgcl_solver;
		solve_system_amgcl<amgcl_solver, backend>(result, A, b, max_iterations, tolerance);
		std::cout << result;
	}


}

void run_solvers(const SparseMatrixType& A, const VectorType& b,
	int N, double u,
	double e, double beta, std::string scheme)
{
	for (auto solver : solvers)
	{
		run_solver(A, b, N, u, e, beta, scheme, solver);
	}

}

void test_2D(int N, double Pe, double e, double Da,
	std::string scheme)
{
	SparseMatrixType A(N * N, N * N);
	VectorType b(N * N);
	Input2D input;

	input.x_w = 0;
	input.x_e = 1;
	input.y_s = 0;
	input.y_n = 1;
	input.N_x = N;
	input.N_y = N;
	input.phi_w = [](double y)
	{
		return 1.0;
	};
	input.phi_e = [](double y)
	{
		return 1.0;
	};
	input.phi_n = [](double x)
	{
		return 1.0;
	};
	input.phi_s = [](double x)
	{
		return 1.0;
	};
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);

	//Convert from Pe & Da to u & beta.
	double u = Pe * e / dx;
	double beta = Da * (u + e / dx) / dx;

	input.u_x = [u](double x, double y)
	{
		return u;
	};
	input.u_y = [u](double x, double y)
	{
		return u;
	};
	input.e = [e](double x, double y)
	{
		return e;
	};
	input.s = [beta](double x, double y)
	{
		return 0.0;
	};
	input.sl = [beta](double x, double y)
	{
		return beta;
	};
	discretizer::get_flux(input, scheme);

	if (scheme == "CFCF")
	{
		discretizer::discretizecf(input, A, b);
	}
	else
	{
		discretizer::discretize(input, A, b);
	}

	run_solvers(A, b, N, u, e, beta, scheme);
}
void test_3D(int N, double Pe, double e, double Da,
	std::string scheme)
{
	SparseMatrixType A(N * N * N, N * N * N);
	VectorType b(N * N * N);
	Input3D input;
	input.x_w = 0;
	input.x_e = 1;
	input.y_s = 0;
	input.y_n = 1;
	input.z_d = 0;
	input.z_u = 1;
	input.N_x = N;
	input.N_y = N;
	input.N_z = N;
	input.phi_w = [](double y, double z)
	{
		return 1.0;
	};
	input.phi_e = [](double y, double z)
	{
		return 1.0;
	};
	input.phi_n = [](double x, double z)
	{
		return 1.0;
	};
	input.phi_s = [](double x, double z)
	{
		return 1.0;
	};
	input.phi_d = [](double x, double y)
	{
		return 1.0;
	};
	input.phi_u = [](double x, double y)
	{
		return 1.0;
	};
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	double dz = (input.z_u - input.z_d) / (input.N_z - 1);

	//Convert from Pe & Da to u & beta.
	double u = Pe * e / dx;
	double beta = Da * (u + e / dx) / dx;

	input.u_x = [u](double x, double y, double z)
	{
		return u;
	};
	input.u_y = [u](double x, double y, double z)
	{
		return u;
	};
	input.u_z = [u](double x, double y, double z)
	{
		return u;
	};
	input.e = [e](double x, double y, double z)
	{
		return e;
	};
	input.s = [beta](double x, double y, double z)
	{
		return beta;
	};
	input.sl = [](double x, double y, double z)
	{
		return 0.0;
	};
	discretizer::get_flux(input, scheme);

	//construct_2D_indexing(A, b, N, u, e, beta);
	if (scheme == "CFCF")
	{
		discretizer::discretizecf(input, A, b);
	}
	else
	{
		discretizer::discretize(input, A, b);
	}

	run_solvers(A, b, N, u, e, beta, scheme);
}

int main()
{


	// see https://en.cppreference.com/w/cpp/numeric/fenv for making this portable. There are
	// also other possibilities, like translating the FPU exception into a C++ exception that
	// can then be handled gracefully by the program... so it seems.
	// NOTE that division by zero (of a non-zero value) and overflows are not necessarily
	// problematic, since IEE754 describes clear semantics for thee cases.
	// compileren met optimalisatie en debug, krijgt backtrace dan met gdb precies de regel waar het gebeurt
	//feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
	//std::cout << "2D test" << std::endl;

	for (int N : N_vec)
	{
		for (double Pe : Pe_vec)
		{
			for (double e : e_vec)
			{
				for (double Da : Da_vec)
				{
					for (std::string scheme : schemes)
					{
						test_2D(N, Pe, e, Da, scheme);
					}
				}
			}
		}
	}

	// std::cout << "3D test" << std::endl;

	// for (int N : N_vec)
	// {
	// 	for (double Pe : Pe_vec)
	// 	{
	// 		for (double e : e_vec)
	// 		{
	// 			for (double Da : Da_vec)
	// 			{
	// 				for (std::string scheme : schemes)
	// 				{
	// 					test_3D(N, Pe, e, Da, scheme);
	// 				}
	// 			}
	// 		}
	// 	}
	// }

}
