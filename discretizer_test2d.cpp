#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <unsupported/Eigen/IterativeSolvers>
#include <Eigen/SparseLU>
#include "IDRStab.h"
#include "BiCGSTABL.h"
#include <vector>
#include <string>
#include <iostream>
#include <chrono>
#include <type_traits>
#include <cmath>
#include <limits>
#include "discretizer.hpp"
#include "input_struct.hpp"
#include <fstream>
#include "mf.hpp"
#include "result.hpp"
#include <iomanip>
typedef std::chrono::high_resolution_clock Clock;
typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SparseMatrixType;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorType;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> DenseMatrixTypeCol;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DenseMatrixTypeRow;

//TODO: Set up a case with all schemes, compare convergence

double exact_solution1(double a, double b, double c, double x, double y)
{
	//TODO: Test synthesized solution
	//phi(x)=a*sin(b*x)+c
	//u=u
	//e=e
	//sl=sl
	//s constructed such that the solution fits
	return a * std::pow(x + y, b) + c;
}
double exact_solution1_s(double a, double b, double c, double u, double e, double sl, double x,
	double y)
{
	//TODO: Test synthesized solution
	//phi(x)=a*sin(b*x)+c
	//u=u
	//e=e
	//sl=sl
	//s constructed such that the solution fits
	return	-2 * a * (-1 + b) * b * e * std::pow(x + y,
			(-2 + b)) + 2 * a * b * u * std::pow(x + y,
			(-1 + b)) + sl * (c + a * std::pow(x + y, b));
}
double compare_solutions(VectorType value, VectorType expected)
{
	// std::cout << std::setw(15) << "value" << std::setw(15) << "expected" << std::setw(
	// 		15) << "deviation" << std::endl;

	// for (int i = 0; i < value.size(); i++)
	// {
	// 	std::cout << std::setw(15) << value[i] << std::setw(15) << expected[i] << std::setw(
	// 			15) << value[i] - expected[i]  << std::endl;
	// }

	double deviation = (value - expected).norm();
	//std::cout << "Deviation/N: " << deviation / value.size() << std::endl;
	//assert((std::abs(deviation / value.size()) < 5e-3));
	return deviation / value.size();
}
double solve_case(int N, std::string scheme)
{
	Input2D input;

	input.x_w = 0;
	input.x_e = 1;
	input.y_s = 0;
	input.y_n = 1;
	input.N_x = N;
	input.N_y = N;
	double a = 1;
	double b = 2;
	double c = 1;
	double u = 1;
	double e = 1;
	double sl = 0;

	input.phi_w = [a, b, c, input](double y)
	{
		return exact_solution1(a, b, c, input.x_w, y);
	};
	input.phi_e = [a, b, c, input](double y)
	{
		return exact_solution1(a, b, c, input.x_e, y);
	};
	input.phi_n = [a, b, c, input](double x)
	{
		return exact_solution1(a, b, c,  x, input.y_n);
	};
	input.phi_s = [a, b, c, input](double x)
	{
		return exact_solution1(a, b, c, x, input.y_s);
	};

	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	input.u_x = [u](double x, double y)
	{
		return u;
	};
	input.u_y = [u](double x, double y)
	{
		return u;
	};
	input.e = [e](double x, double y)
	{
		return e;
	};
	input.s = [a, b, c, u, e, sl](double x, double y)
	{
		return exact_solution1_s(a, b, c, u, e, sl, x, y);
	};
	input.sl = [sl](double x, double y)
	{
		return sl;
	};

	if (scheme == "CF" || scheme == "CFCF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CF(u, e, dx);
		};
	}
	else if (scheme == "HF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::HF(u, e, dx);
		};
	}
	else if (scheme == "CD")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CD(u, e, dx);
		};
	}
	else if (scheme == "UW")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::UW(u, e, dx);
		};
	}

	SparseMatrixType A;
	VectorType rhs;

	if (scheme == "CFCF")
	{
		discretizer::discretizecf(input, A, rhs);
	}
	else
	{
		discretizer::discretize(input, A, rhs);
	}

	Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
	solver.analyzePattern(A);
	solver.factorize(A);
	//Eigen::BiCGSTAB<SparseMatrixType> solver(A);
	//Eigen::IDRStab<SparseMatrixType> solver(A);
	VectorType phi = solver.solve(rhs);
	VectorType phi_exact(input.N_x * input.N_y);

	for (int i = 0; i < input.N_x; i++)
	{
		for (int j = 0; j < input.N_y; j++)
		{
			//Index of the cell
			int ix_c = i + input.N_x * j;
			//Position of the nodal point
			double x = input.x_w + i * dx;
			double y = input.y_s + j * dy;
			phi_exact(ix_c) = exact_solution1(a, b, c, x, y);
		}


	}

	return compare_solutions(phi, phi_exact);
}
int main()
{
	std::vector<int> N_vector(7);
	std::generate(N_vector.begin(), N_vector.end(), [n = 1]()mutable
	{
		n *= 2;
		return n + 1;});

	std::cout << std::setw(5) << "N" << std::setw(15) << "CFCF" << std::setw(15) << "CF" << std::setw(15) << "HF" << std::setw(15) << "CD" << std::setw(15) << "UW" << std::endl;

	for (auto N : N_vector)
	{
		std::cout << std::setw(5) << N
			<< std::setw(15) << solve_case(N, "CFCF")
			<< std::setw(15) << solve_case(N, "CF")
			<< std::setw(15) << solve_case(N, "HF")
			<< std::setw(15) << solve_case(N, "CD")
			<< std::setw(15) << solve_case(N, "UW")
			<< std::endl;
	}

	assert(solve_case(257, "CFCF") < 7e-7);
	assert(solve_case(257, "CF") < 8e-4);
	assert(solve_case(257, "HF") < 8e-4);
	assert(solve_case(257, "CD") < 8e-4);
	assert(solve_case(257, "UW") < 8e-4);
}