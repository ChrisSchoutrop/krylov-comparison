#include<string>
#include "result.hpp"
void Result::set_pre_info(std::string algorithm, double u, double beta, double e, int S, int L,
	int64_t timestamp, std::string scheme, double tolerance, int N, int max_iterations, double cond)
{
	/*
	        Info that can be obtained before running the solver
	*/
	m_algorithm = algorithm;
	m_u = u;
	m_beta = beta;
	m_e = e;
	m_S = S;
	m_L = L;
	m_timestamp = timestamp;
	m_scheme = scheme;
	m_tolerance = tolerance;
	m_N = N;
	m_max_iterations = max_iterations;
	m_cond = cond;
};
void Result::set_post_info(double true_residual, double estimated_residual, int iterations,
	int return_flag, double time)
{
	/*
	        Info that can only be obtained after running the solver
	*/

	m_true_residual = true_residual;
	m_estimated_residual = estimated_residual;
	m_iterations = iterations;
	m_return_flag = return_flag;
	m_time = time;
};