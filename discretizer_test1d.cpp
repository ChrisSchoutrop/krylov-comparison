#include "flux.hpp"
#include "mf.hpp"
#include "input_struct.hpp"
#include "discretizer.hpp"
#include <cmath>
#include <iostream>
#include <algorithm>
#include <array>
#include <iomanip>
#include "eigen3/Eigen/Sparse"
#include "eigen3/Eigen/Dense"
#include <chrono>
#include "string"
typedef std::chrono::high_resolution_clock Clock;
typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SparseMatrixType;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorType;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> DenseMatrixTypeCol;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DenseMatrixTypeRow;

//TODO: Set up a case with all schemes, compare convergence

double exact_solution1(double a, double b, double c, double x)
{
	//TODO: Test synthesized solution
	//phi(x)=a*sin(b*x)+c
	//u=u
	//e=e
	//sl=sl
	//s constructed such that the solution fits
	return a * std::pow(x, b) + c;
}
double exact_solution1_s(double a, double b, double c, double u, double e, double sl, double x)
{
	//TODO: Test synthesized solution
	//phi(x)=a*sin(b*x)+c
	//u=u
	//e=e
	//sl=sl
	//s constructed such that the solution fits
	return	-a * (-1 + b) * b * e * std::pow(x, (-2 + b)) + a * b * u * std::pow(x,
			(-1 + b)) + sl * (c + a * std::pow(x, b));
}
double compare_solutions(VectorType value, VectorType expected)
{
	// std::cout << std::setw(15) << "value" << std::setw(15) << "expected" << std::setw(
	// 		15) << "deviation" << std::endl;

	// for (int i = 0; i < value.size(); i++)
	// {
	// 	std::cout << std::setw(15) << value[i] << std::setw(15) << expected[i] << std::setw(
	// 			15) << value[i] - expected[i]  << std::endl;
	// }

	double deviation = (value - expected).norm();
	//std::cout << "Deviation/N: " << deviation / value.size() << std::endl;
	//assert((std::abs(deviation / value.size()) < 1e-3));
	return deviation / value.size();
}
double solve_case(int N_x, std::string scheme)
{
	Input1D input;
	input.N_x = N_x;

	input.x_w = 0.5;
	input.x_e = 1;
	double a = 2.23606;
	double b = 0.57721;
	double c = 3.14159265359;
	double u = 2.7182818284;
	double e = 1.41421;
	double sl = 1.73205;

	input.phi_w = exact_solution1(a, b, c, input.x_w);
	input.phi_e = exact_solution1(a, b, c, input.x_e);

	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	input.u = [u](double x)
	{
		return u;
	};
	input.e = [e](double x)
	{
		return e;
	};
	input.s = [a, b, c, u, e, sl](double x)
	{
		return exact_solution1_s(a, b, c, u, e, sl, x);
	};
	input.sl = [sl](double x)
	{
		return sl;
	};

	if (scheme == "CF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CF(u, e, dx);
		};
	}
	else if (scheme == "HF")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::HF(u, e, dx);
		};
	}
	else if (scheme == "CD")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::CD(u, e, dx);
		};
	}
	else if (scheme == "UW")
	{
		input.flux = [](double u, double e, double dx)
		{
			return flux::UW(u, e, dx);
		};
	}

	SparseMatrixType A;
	VectorType rhs;
	auto t1 = Clock::now();
	discretizer::discretize(input, A, rhs);
	auto t2 = Clock::now();
	t1 = Clock::now();
	Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
	solver.analyzePattern(A);
	solver.factorize(A);
	VectorType phi = solver.solve(rhs);

	VectorType phi_exact(input.N_x);

	for (size_t i = 0; i < input.N_x; i++)
	{
		phi_exact(i) = exact_solution1(a, b, c, input.x_w + dx * i);
	}

	return compare_solutions(phi, phi_exact);
}

int main()
{
	std::vector<int> N_vector(10);
	std::generate(N_vector.begin(), N_vector.end(), [n = 1]()mutable
	{
		n *= 2;
		return n + 1;});

	std::cout << std::setw(5) << "N" << std::setw(15) << "CF" << std::setw(15) << "HF" << std::setw(15) << "CD" << std::setw(15) << "UW" << std::endl;

	for (auto N_x : N_vector)
	{
		std::cout << std::setw(5) << N_x << std::setw(15) << solve_case(N_x, "CF") << std::setw(15) << solve_case(N_x, "HF") << std::setw(15) << solve_case(N_x, "CD") << std::setw(15) << solve_case(N_x,
				"UW") << std::endl;
	}

	assert(solve_case(2048, "CF") < 1e-9);
	assert(solve_case(2048, "HF") < 1e-9);
	assert(solve_case(2048, "CD") < 1e-9);
	assert(solve_case(2048, "UW") < 1e-6);

}