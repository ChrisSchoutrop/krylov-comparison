'''
Plotscript for the acos_2019 results
00 result.algorithm << "\t" <<
01 result.true_residual << "\t" <<
02 result.estimated_residual << "\t" <<
03 result.iterations << "\t" <<
04 result.return_flag << "\t" <<
05 result.u << "\t" <<
06 result.beta << "\t" <<
07 result.e << "\t" <<
08 result.time << "\t" <<
09 result.N << "\t" <<
10 result.max_iterations << "\t" <<
11 result.tolerance << "\t" <<
12 result.const_source << "\t" <<
13 result.boundary_value << "\t" <<
14 result.length << "\t" <<
15 result.S << "\t" <<
16 result.L << "\t" <<
17 result.timestamp << "\t" <<
'''
#TODO: Clean this in some nice way

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import math
import scipy.interpolate
import plotter
from multiprocessing import Pool, cpu_count

plt.close()
font = {'family': 'normal',
        'weight': 'normal',
        'size': 15}
matplotlib.rc('font', **font)
'''
Available options:
	Algorithms (string):
		-amgcl_bicgstab_amg
		-amgcl_bicgstabl_amg
		-amgcl_idrs_amg
		-amgcl_eigen_bicgstab_amg
		-eigen_idrstab_diagonal
		-eigen_bicgstabl_diagonal
		-eigen_bicgstab_diagonal
		-amgcl_bicgstab_diagonal
		-amgcl_bicgstabl_diagonal
	N (int):
		-5
		-11
		-21
		-41
		-81
		-161
	x,y (string)
		-u
		-e
		-beta
		-Advective Damkohler
		-Diffusive Damkohler
		-Harmonic Damkohler
		-Peclet
		-Grid Advective Damkohler
		-Grid Diffusive Damkohler
		-Grid Harmonic Damkohler
		-Grid Peclet
	z (string)
		-true_residual
		-estimated_residual
		-iterations
		-time
		-cond
	scheme (string)
		-UW
		-CD
		-HF
		-CF
		-CFCF
'''
plot_jobs = []

job = {
	"algorithm": "eigen_bicgstab_diagonal",
	"scheme": "CD",
	"x": "Grid Peclet",
	"y": "Grid Harmonic Damkohler",
	"z":"cond",
	"N": 161,
	"e": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "eigen_bicgstab_diagonal",
	"scheme": "CD",
	"x": "Grid Peclet",
	"y": "Grid Harmonic Damkohler",
	"z":"true_residual",
	"N": 161,
	"e": 1,
}
plot_jobs.append(job)
job = {
	"algorithm": "eigen_bicgstab_diagonal",
	"scheme": "CD",
	"x": "Grid Peclet",
	"y": "Grid Harmonic Damkohler",
	"z":"iterations",
	"N": 161,
	"e": 1,
}
plot_jobs.append(job)

#Load in data
data = np.loadtxt('acos_2020.log', delimiter='\t', dtype=object)
print(data)
data[:, 1:-3] = data[:, 1:-3].astype(float)
data[:, -1] = data[:, -1].astype(float)
print(data)

#Execute plot jobs
for job in plot_jobs:
	print(job)
	filename=""
	#Convert to a useful format
	if "N" in job:
		N = job["N"]
	else:
		N = None
	if isinstance(job["algorithm"],list):
		filename = filename+'_'+str('-'.join(job["algorithm"]))
	else:
		filename = filename+job["algorithm"]
	if isinstance(job["scheme"],list):
		filename = filename+'_'+str('-'.join(job["scheme"]))
	else:
		filename=filename+'_'+str(job["scheme"])
	filename = filename+'_'+str(N)
	filename = filename+'_x'+str(job["x"])
	filename = filename+'_y'+str(job["y"])
	if "z" in job:
		filename = filename+'_z'+str(job["z"])
		z_type = job["z"]
	else:
		z_type = None
	if "u" in job:
		u = job["u"]
	else:
		u = None
	if "e" in job:
		e = job["e"]
	else:
		u = None
	if "beta" in job:
		beta = job["beta"]
	else:
		beta = None
	if "bicgstabl" in job["algorithm"].lower():
		L = job["L"]
		S = -1
	elif "bicgstab" in job["algorithm"]:
		L = 1
		S = 1
	elif "idrstab" in job["algorithm"]:
		L = job["L"]
		S = job["S"]
	elif "idrs" in job["algorithm"]:
		L=-1
		S=job["S"]
	else:
		L = -1
		S = -1
	filename = filename+'_S'+str(S)
	filename = filename+'_L'+str(L)
	plotter.plot(filename, data, z_type, job["scheme"],
	             job["algorithm"], job["x"], job["y"], L, S, N, u, e, beta)
