#ifndef DISCRETIZER_H
#define DISCRETIZER_H
#include "eigen3/Eigen/Dense"
#include "eigen3/Eigen/Sparse"
#include "input_struct.hpp"
#include <string>
#include "flux.hpp"

class discretizer
{
		typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SparseMatrixType;
		typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorType;
		typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> DenseMatrixTypeCol;
		typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DenseMatrixTypeRow;

	public:
		static void discretize(const Input1D& input, SparseMatrixType& A, VectorType& b);
		static void discretize(const Input2D& input, SparseMatrixType& A, VectorType& b);
		static void discretize(const Input3D& input, SparseMatrixType& A, VectorType& b);
		static void discretizecf(const Input2D& input, SparseMatrixType& A, VectorType& b);
		static void discretizecf(const Input3D& input, SparseMatrixType& A, VectorType& b);
		template <typename T>
		static void get_flux(T& input, const std::string& scheme)
		{
			if (scheme == "CF")
			{
				input.flux = [](double u, double e, double d)
				{
					return flux::CF(u, e, d);
				};
			}
			else if (scheme == "HF")
			{
				input.flux = [](double u, double e, double d)
				{
					return flux::HF(u, e, d);
				};
			}
			else if (scheme == "CD")
			{
				input.flux = [](double u, double e, double d)
				{
					return flux::CD(u, e, d);
				};
			}
			else if (scheme == "UW")
			{
				input.flux = [](double u, double e, double d)
				{
					return flux::UW(u, e, d);
				};
			}
			else if (scheme == "CFCF")
			{
				input.flux = [](double u, double e, double d)
				{
					return flux::CF(u, e, d);
				};
			}
		};
};
#endif //DISCRETIZER_H